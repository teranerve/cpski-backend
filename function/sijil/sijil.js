const knex = require('../../connection.js')

async function getImplementerCiptaSijil(implementerId) {
  let result = null

  result = await knex.connect.raw(
    `SELECT 
      implementerId, 
      implementerFullname, 
      implementerKawasan, 
      implementerNegara
    FROM implementer      
    WHERE implementerId = ` + implementerId,
  )

  return result
}

async function getSenaraiPesertaSijil(implementerId) {
  let result = null

  result = await knex.connect.raw(
    `SELECT DISTINCT liveStokType, liveStokNo, liveStokAddress, aktivitiLookupTajuk, wakilPesertaName, 
    implementerNegara, implementerKawasan, userFullname AS wakilName, tempahanNo, tempahanDetailsLiveStok.tempahanId
    FROM tempahanDetailsLiveStok
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN liveStokBahagian ON tempahanDetailsLiveStok.liveStokBahagianId = liveStokBahagian.liveStokBahagianId
    JOIN aktivitiDate ON tempahanDetailsLiveStok.dateAktivitiId = aktivitiDate.aktivitiDateId
    JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
    JOIN wakilPeserta ON tempahanDetailsLiveStok.wakilPesertaId = wakilPeserta.wakilPesertaId
    JOIN implementer ON tempahanDetailsLiveStok.implementerId = implementer.implementerId
    JOIN tempahan ON tempahanDetailsLiveStok.tempahanId = tempahan.tempahanId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user ON userWakil.userId = user.userId
    WHERE ISNULL(sijilStatus) 
    AND tempahanDetailsLiveStok.implementerId = ` + implementerId,
  )

  return result
}

async function getSenaraiPesertaSijilActive(implementerId) {
  let result = null

  result = await knex.connect.raw(
    `SELECT DISTINCT liveStokType, liveStokNo, aktivitiLookupTajuk, wakilPesertaName, 
    implementerNegara, implementerKawasan, userFullname AS wakilName, tempahanNo, 
    tempahanDetailsLiveStok.tempahanId, tempahanDetailsLiveStok.sijilPath
    FROM tempahanDetailsLiveStok
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN liveStokBahagian ON tempahanDetailsLiveStok.liveStokBahagianId = liveStokBahagian.liveStokBahagianId
    JOIN aktivitiDate ON tempahanDetailsLiveStok.dateAktivitiId = aktivitiDate.aktivitiDateId
    JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
    JOIN wakilPeserta ON tempahanDetailsLiveStok.wakilPesertaId = wakilPeserta.wakilPesertaId
    JOIN implementer ON tempahanDetailsLiveStok.implementerId = implementer.implementerId
    JOIN tempahan ON tempahanDetailsLiveStok.tempahanId = tempahan.tempahanId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user ON userWakil.userId = user.userId
    WHERE sijilStatus IS NOT NULL
    AND tempahanDetailsLiveStok.implementerId = ` + implementerId,
  )

  return result
}

async function getPesertaSijilByTempahan(tempahan, name, implementerId) {
  let result = null

  result = await knex.connect.raw(
    `SELECT DISTINCT liveStokType, liveStokNo, aktivitiLookupTajuk, wakilPesertaName, 
    implementerNegara, implementerKawasan, userFullname AS wakilName, tempahanNo, sijilStatus, 
    tempahanDetailsLiveStokId, tempahanDetailsLiveStok.tempahanId, liveStokBahagian.liveStokBahagianNo,
    tempahanDetails.tempahanDetailsBhgnType
    FROM tempahanDetailsLiveStok
    JOIN tempahanDetails ON tempahanDetailsLiveStok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN liveStokBahagian ON tempahanDetailsLiveStok.liveStokBahagianId = liveStokBahagian.liveStokBahagianId
    JOIN aktivitiDate ON tempahanDetailsLiveStok.dateAktivitiId = aktivitiDate.aktivitiDateId
    JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
    JOIN wakilPeserta ON tempahanDetailsLiveStok.wakilPesertaId = wakilPeserta.wakilPesertaId
    JOIN implementer ON tempahanDetailsLiveStok.implementerId = implementer.implementerId
    JOIN tempahan ON tempahanDetailsLiveStok.tempahanId = tempahan.tempahanId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user ON userWakil.userId = user.userId
    WHERE tempahanDetailsLiveStok.implementerId = ` + implementerId +`
    AND wakilPesertaName = "` + name + `"
    AND tempahan.tempahanNo = '` + tempahan + `'`
  )

  return result[0]
}

async function updateTempahanDetailsLiveStok(Id, filePath, liveStokNo) {
  let result = null
  let sql = null

  try {
    sql = await knex
      .connect(`tempahanDetailsLiveStok`)
      .where('tempahanDetailsLiveStokId', '=', Id)
      .update({
        sijilStatus: 'ACTIVE',
        sijilPath: `https://yayasanikhlasski.s3.ap-southeast-1.amazonaws.com/sijil/` + process.env.ENVIRONMENT + `/` + liveStokNo + `/` + filePath
      })

    console.log('updateTempahanDetailsLiveStok: ', sql)

    if (!sql || sql.length == 0) {
      result = false
    } else {
      result = sql[0]
    }
  } catch (error) {
    console.log(error)
  }

  return result
}

async function getPesertaInOneAnimal(tempahanNo, tempahanId, implementerId, noHaiwan) {
  let result = null

  result = await knex.connect.raw(
    `SELECT liveStokType, liveStokNo, aktivitiLookupTajuk, wakilPesertaName, 
    implementerNegara, implementerKawasan, userFullname AS wakilName, tempahanNo, tempahanDetailsLiveStok.tempahanId, liveStokBahagian.liveStokBahagianNo
    FROM tempahanDetailsLiveStok
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN liveStokBahagian ON tempahanDetailsLiveStok.liveStokBahagianId = liveStokBahagian.liveStokBahagianId
    JOIN aktivitiDate ON tempahanDetailsLiveStok.dateAktivitiId = aktivitiDate.aktivitiDateId
    JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
    JOIN wakilPeserta ON tempahanDetailsLiveStok.wakilPesertaId = wakilPeserta.wakilPesertaId
    JOIN implementer ON tempahanDetailsLiveStok.implementerId = implementer.implementerId
    JOIN tempahan ON tempahanDetailsLiveStok.tempahanId = tempahan.tempahanId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user ON userWakil.userId = user.userId
    WHERE ISNULL(sijilStatus) 
    AND tempahanDetailsLiveStok.implementerId = ` + implementerId +`
    AND liveStok.liveStokNo = '` + noHaiwan + `'`
  )

  return result[0]
}

module.exports = {
  getImplementerCiptaSijil,
  getSenaraiPesertaSijil,
  getSenaraiPesertaSijilActive,
  getPesertaSijilByTempahan,
  updateTempahanDetailsLiveStok,
  getPesertaInOneAnimal
}
