const knex = require("../../connection.js");
const moment = require("moment");

function check_param(body) {
  if (!body.stok || body.stok.length < 1)
    return { status: false, message: "Stok is required" };

  if (!body.lokasi) return { status: false, message: "Lokasi is required" };

  return { status: true };
}

async function update_stok_location(body) {
  // GET INDEX 0 FROM body.stok
  const stok = body.stok.length;

  // LOOP EVERY STOK
  for (let i = 0; i < body.stok.length; i++) {
    // UPDATE STOK STATUS
    var update = await knex
      .connect("liveStok")
      .update({
        liveStokAddress: body.lokasi,
      })
      .whereLike("liveStokNo", body.stok[i].liveStokNo);
    console.log("update livestok lokasi: ", update);
  }

  return true;
}

module.exports = {
  check_param,
  update_stok_location,
};
