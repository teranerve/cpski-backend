const knex = require("../connection.js");
const moment = require("moment");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function insertUser(username, fullname, password,type) {
  let result = null;

  try {
    let datetime = getDateTime();

    result = await knex.connect(`user`).insert({
      userUsername: username,
      userPassword: password,
      userFullname: fullname,
      userType: type,
      userRegistrationDate: datetime,
      userStatus: "ACTIVE",
      userStatusCode: 1,
      setupKorbanId: 2,
    });

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getUser(userType) {
  let result = null;

  console.log(userType);

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex.connect(`user`).where(`userType`, userType)

  console.log(result);

  return result;
}

async function updateUser(username, fullname) {
  let result = null;

  try {

    result = await knex
      .connect(`user`)
      .update({
        userFullname: fullname,
      })
      .where(`userUsername`, username);

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function deleteUser(username) {
  let result = null;

  try {
    result = await knex.connect(`user`).where({ userUsername: username }).del();

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

module.exports = {
  insertUser,
  getUser,
  updateUser,
  deleteUser,
};