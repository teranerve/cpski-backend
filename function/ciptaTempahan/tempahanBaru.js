const knex = require("../../connection.js");
const moment = require("moment");
const axios = require("axios");

async function getSenaraiNegara() {
  let result = null;

  result = await knex
    .connect("implementer")
    .join(
      "setupKorbanImplementerLokasi",
      "setupKorbanImplementerLokasi.implementerId",
      "=",
      "implementer.implementerId"
    )
    .join(
      "setupKorban",
      "setupKorban.setupKorbanId",
      "=",
      "setupKorbanImplementerLokasi.setupKorbanId"
    )
    .where({
      "setupKorban.setupKorbanStatus": "Active",
      "implementer.implementerStatus": "Active",
    })
    .whereIn("implementer.implementerKawasan", ["Malaysia", "Luar Negara"])
    // .andWhere(function () {
    //   this.where("setupKorban.setupKorbanTarikhMula", ">", 2022 - 03 - 20);
    // })
    // .andWhere(function () {
    //   this.where("setupKorban.setupKorbanTarikhTamat", ">", 2022 - 06 - 20);
    // })
    .select(
      "implementer.implementerNegara",
      "implementer.implementerKawasan",
      "implementer.implementerStatus"
    )
    .count("implementer.implementerKawasan as implementerKawasanCount")
    .groupBy(
      "implementer.implementerNegara",
      "implementer.implementerKawasan",
      "implementer.implementerStatus"
    )
    .orderBy("implementer.implementerKawasan", "desc");

  return result;
}

async function getSenaraiHaiwan() {
  let result = null;

  try {
    result = await knex
      .connect("confLookup as cl")
      .join(
        "configuration as cg",
        "cl.confLookupId",
        "=",
        "cg.configurationValue1"
      )
      .join(
        "configuration as cg2",
        "cg2.configurationParentId",
        "=",
        "cg.configurationId"
      )
      .join(
        "confLookup as cl2",
        "cl2.confLookupId",
        "=",
        "cg2.configurationValue1"
      )
      .where({
        "cg2.configurationStatus": "Active",
      })
      .whereIn("cg.configurationParentId", [2, 7])
      .select(
        "cg2.configurationId",
        "cg.configurationParentId",
        "cl.confLookupCode as NEGARA",
        "cl2.confLookupCode as HAIWAN",
        "cg2.configurationValue2 as HARGA"
      );
  } catch (error) {
    console.log(error);
  }

  return result;
}

// GENERATE RANDOM NUMBER
function generateRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// GET DATE TIME NOW
function getDateTime() {
  return moment().format("YYYYMMDDhhmmss");
}

// GET DATE TIME NOW FORMAT
function getDateTimeFormat() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

// GENERATE TEMPAHAN NUMBER
async function generateTempahanNo() {
  let random = generateRandomNumber(1000, 9999);
  let random2 = generateRandomNumber(1000, 9999);
  let datetime = getDateTime();
  return "YI" + random + datetime + random2;
}

// GENERATE TEMPAHAN NUMBER
async function generateRefundNo() {
  let random = generateRandomNumber(1000, 9999);
  let random2 = generateRandomNumber(1000, 9999);
  let datetime = getDateTime();
  return "RE" + random + datetime + random2;
}

// GET ORGANIZATION'S ID
async function getOrganizationId(organizationDomain, organizationStatus) {
  let result = null;

  result = await knex
    .connect("organization")
    .where({
      organizationDomain: organizationDomain,
      organizationStatus: organizationStatus,
    })
    .select("organizationId");

  return result;
}

// GET SETUP KORBAN ID
async function getSetupKorbanId(
  setupKorbanTahun,
  setupKorbanStatus,
  organizationId
) {
  let result = null;

  result = await knex
    .connect("setupKorban")
    .where({
      setupKorbanTahun: setupKorbanTahun,
      setupKorbanStatus: setupKorbanStatus,
      organizationId: organizationId,
    })
    .select("setupKorbanId");

  return result;
}

// GET EJEN DETAILS
async function getEjenDetails(userEjenPermalink) {
  let result = null;

  result = await knex
    .connect("userEjen")
    .where({
      userEjenPermalink: userEjenPermalink,
    })
    .select("*");

  return result;
}

async function checkEmail(userEmail) {
  let result = null;

  result = await knex
    .connect("user")
    .where({
      userEmail: userEmail,
    })
    .select("userEmail");

  return result;
}

async function checkPhone(userPhoneNo) {
  let result = null;

  result = await knex
    .connect("user")
    .where({
      userPhoneNo: userPhoneNo,
    })
    .select("userPhoneNo");

  return result;
}

async function getUserWakilId(userFullname, userPhoneNo, userEmail) {
  let result = null;

  result = await knex
    .connect("user")
    .join("userWakil", "user.userId", "=", "userWakil.userId")
    .where({
      // 'user.userFullname': userFullname, --check Nama yang sama
      "user.userPhoneNo": userPhoneNo, // --check Phone yang sama
      // 'user.userEmail': userEmail, --check Email yang sama
    })
    .select("userWakil.userWakilId", "user.userId");

  return result;
}

async function insertUser(userFullname, userPhoneNo, userEmail) {
  let result = null;
  let datetime = getDateTimeFormat();

  result = await knex.connect("user").insert({
    userFullname: userFullname,
    userEmail: userEmail,
    userPhoneNo: userPhoneNo,
    userType: "Wakil",
    userTypeCode: 30,
    userRegistrationDate: datetime,
    userStatus: "Active",
    userStatusCode: 20,
  });

  return result;
}

async function insertUserWakil(insertUserWakil) {
  let result = null;

  result = await knex.connect("userWakil").insert({
    userId: insertUserWakil,
    userWakilStatus: "Active",
    userWakilStatusCode: 20,
  });

  return result;
}

// GET LIVE STOK BIL BAHAGIAN BAKI
async function checkTempahanId(tempahanNo) {
  let result = null;

  result = await knex
    .connect("tempahan")
    .join(
      "tempahanDetails",
      "tempahanDetails.tempahanId",
      "=",
      "tempahan.tempahanId"
    )
    .select("tempahan.tempahanId", "tempahanDetails.tempahanDetailsId")
    .where({
      "tempahan.tempahanNo": tempahanNo,
    });

  return result;
}

// CHECK IF WAKIL PESERTA NAME IS EXIST
async function getWakilPeserta(wakilPesertaName, wakilPesertaGender) {
  let result = null;

  result = await knex
    .connect("wakilPeserta")
    .where({
      wakilPesertaName: wakilPesertaName,
      wakilPesertaGender: wakilPesertaGender,
    })
    // .orWhere({
    //     'wakilPesertaGender': wakilPesertaGender
    // })
    .select("wakilPesertaId", "wakilPesertaName");

  return result;
}

// INSERT INTO WAKIL PESERTA TABLES
async function insertWakilPeserta(
  userWakilId,
  wakilPesertaName,
  wakilPesertaGender,
  wakilPesertaStatus,
  wakilPesertaStatusCode
) {
  let result = null;
  // CAPITALIZE ALL WORKD FIRST LETTER FOR USERWAKIL NAME
  let arr = wakilPesertaName.split(" ");
  for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }

  result = await knex.connect("wakilPeserta").insert({
    userWakilId: userWakilId,
    wakilPesertaName: arr.join(" "),
    wakilPesertaGender: wakilPesertaGender,
    wakilPesertaStatus: wakilPesertaStatus,
    wakilPesertaStatusCode: wakilPesertaStatusCode,
  });

  return result;
}

// INSERT INTO TEMPAHAN TABLES
async function insertTempahan(
  tempahanNo,
  setupKorbanId,
  userWakilId,
  userEjenId,
  organizationId,
  tempahanIbadahTypeCode,
  tempahanIbadahType,
  tempahanTotalPrice,
  tempahanQuantity,
  tempahanStatus,
  tempahanStatusCode
) {
  let result = null;
  let datetime = getDateTimeFormat();

  result = await knex.connect("tempahan").insert({
    tempahanNo: tempahanNo,
    setupKorbanId: setupKorbanId,
    userWakilId: userWakilId,
    userEjenId: userEjenId,
    organizationId: organizationId,
    tempahanIbadahTypeCode: tempahanIbadahTypeCode,
    tempahanIbadahType: tempahanIbadahType,
    tempahanTotalPrice: tempahanTotalPrice,
    tempahanQuantity: tempahanQuantity,
    tempahanStatus: tempahanStatus,
    tempahanStatusCode: tempahanStatusCode,
    tempahanCreatedDate: datetime,
    tempahanUpdatedDate: datetime,
  });

  console.log("CREATE TEMPAHAN BARU: ", result);

  return result;
}

// GET AVAIALBLE STOK FOR ANIMAL
async function getStockSelection(
  implementerNegara,
  implementerIbadah,
  tempahanQuantity,
  tempahanIbadahType,
  jenisHaiwan,
  limit
) {
  let result = null;

  result = await knex
    .connect("liveStok")
    .join("implementer", "implementer.implementerId", "liveStok.implementerId")
    .join(
      "liveStokBahagian",
      "liveStokBahagian.liveStokId",
      "liveStok.liveStokId"
    )
    .select(
      "liveStokBahagian.liveStokBahagianId",
      "liveStokBahagian.liveStokBahagianNo",
      "liveStok.liveStokId",
      "liveStok.liveStokBilBahagianBaki",
      "liveStok.liveStokType",
      "liveStok.implementerId",
      "liveStok.implementerLokasiId"
    )
    .where({
      "implementer.implementerNegara": implementerNegara,
      // 'implementer.implementerIbadah': implementerIbadah,
      "liveStok.liveStokIbadah": tempahanIbadahType,
      "liveStok.liveStokType": jenisHaiwan,
    })
    .whereIn("implementer.implementerIbadah", [implementerIbadah, 37])
    .whereIn("liveStok.liveStokStatus", [
      "Active",
      "Partial",
      "Pending Reserved",
    ])
    .where("liveStokBahagian.liveStokBahagianStatus", "=", "Active")
    .where("liveStok.liveStokBilBahagianBaki", ">=", tempahanQuantity)
    .orderBy("liveStok.liveStokId")
    .limit(limit);

  return result;
}

// INSERT INTO TEMPAHAN DETAILS TABLES
async function insertTempahanDetails(
  tempahanId,
  implementerId,
  implementerLokasiId,
  wakilPesertaId,
  tempahanDetailsHaiwanType,
  tempahanDetailsHaiwanTypeCode,
  tempahanDetailsBhgnCount,
  tempahanDetailsBhgnType,
  tempahanDetailsPrice,
  tempahanDetailsStatus,
  tempahanDetailsStatusCode
) {
  let result = null;

  let datetime = getDateTimeFormat();

  result = await knex.connect("tempahanDetails").insert({
    tempahanId: tempahanId,
    implementerId: implementerId,
    implementerLokasiId: implementerLokasiId,
    wakilPesertaId: wakilPesertaId,
    tempahanDetailsHaiwanType: tempahanDetailsHaiwanType,
    tempahanDetailsHaiwanTypeCode: tempahanDetailsHaiwanTypeCode,
    tempahanDetailsBhgnCount: tempahanDetailsBhgnCount,
    tempahanDetailsBhgnType: tempahanDetailsBhgnType,
    tempahanDetailsPrice: tempahanDetailsPrice,
    tempahanDetailsStatus: tempahanDetailsStatus,
    tempahanDetailsStatusCode: tempahanDetailsStatusCode,
    tempahanDetailsDateCreated: datetime,
  });

  return result;
}

// INSERT INTO TEMPAHAN DETAILS LIVE STOK TABLES
async function checkIfAlreadyInsertIntoTempahanDetailsLiveStock(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahanDetailsLiveStok")
    .count("* AS count")
    .where({
      tempahanId: tempahanId,
    });

  return result;
}

// UPDATE TEMPAHAN TABLE UPON PAYMENT
async function updateTempahan(
  tempahanStatus,
  tempahanStatusCode,
  tempahanId,
  tempahanTarikhBayaran,
  tempahanTarikhUploadResit,
  tempahanTarikhPengesahan
) {
  let result = null;

  try {
    result = await knex
      .connect("tempahan")
      .update({
        tempahanStatus: tempahanStatus,
        tempahanStatusCode: tempahanStatusCode,
        tempahanTarikhBayaran: tempahanTarikhBayaran,
        tempahanTarikhUploadResit: tempahanTarikhUploadResit,
        tempahanTarikhPengesahan: tempahanTarikhPengesahan,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE TEMPAHAN TOTAL PRICE
async function updateTempahanTotalPrice(tempahanId, tempahanTotalPrice) {
  let result = null;

  try {
    result = await knex
      .connect("tempahan")
      .update({
        tempahanTotalPrice: tempahanTotalPrice,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE TEMPAHAN TABLE UPON PAYMENT
async function updateCancelTempahan(
  tempahanStatus,
  tempahanStatusCode,
  tempahanId,
  tempahanRefundValue
) {
  let result = null;

  try {
    result = await knex
      .connect("tempahan")
      .update({
        tempahanStatus: tempahanStatus,
        tempahanStatusCode: tempahanStatusCode,
        tempahanRefundValue: tempahanRefundValue,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// GET TEMPAHAN DETAILS
async function getTempahanDetails(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahanDetails")
    .join(
      "wakilPeserta",
      "tempahanDetails.wakilPesertaId",
      "=",
      "wakilPeserta.wakilPesertaId"
    )
    .join(
      "implementer",
      "tempahanDetails.implementerId",
      "=",
      "implementer.implementerId"
    )
    .join("tempahan", "tempahanDetails.tempahanId", "=", "tempahan.tempahanId")
    .where({
      "tempahanDetails.tempahanId": tempahanId,
    })
    .select(
      "tempahanDetails.*",
      "wakilPeserta.wakilPesertaName",
      "implementer.implementerKawasan",
      "implementer.implementerNegara",
      "tempahan.tempahanIbadahType",
      "tempahan.tempahanIbadahTypeCode"
    );

  return result;
}

// GET TEMPAHAN DETAILS
async function getTempahan(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahan")
    .where({
      tempahanId: tempahanId,
    })
    .select("*");

  return result;
}

// UPDATE TEMPAHAN TABLE UPON CLICK PAYMENT BUTTON
async function updateTempahanDetailsBasedOnTempahanDetailsId(
  tempahanDetailsStatus,
  tempahanDetailsStatusCode,
  tempahanDetailsId
) {
  let result = null;

  try {
    result = await knex
      .connect("tempahanDetails")
      .update({
        tempahanDetailsStatus: tempahanDetailsStatus,
        tempahanDetailsStatusCode: tempahanDetailsStatusCode,
      })
      .where("tempahanDetailsId", tempahanDetailsId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// GET COMMISSION BY PARTS
async function getCommissionByParts(negara, haiwan) {
  let result = null;

  try {
    result = await knex
      .connect("configuration as cf")
      .join(
        "confLookup as cl",
        "cl.confLookupId",
        "=",
        "cf.configurationValue1"
      )
      .join(
        "configuration as cf2",
        "cf2.configurationId",
        "=",
        "cf.configurationParentId"
      )
      .join(
        "confLookup as cl2",
        "cl2.confLookupId",
        "=",
        "cf2.configurationValue1"
      )
      .join(
        "configuration as cf3",
        "cf3.configurationId",
        "=",
        "cf2.configurationParentId"
      )
      .join(
        "confLookup as cl3",
        "cl3.confLookupId",
        "=",
        "cf3.configurationValue1"
      )
      .where({
        "cl2.confLookupCode": negara,
        "cl.confLookupCode": haiwan,
      })
      .select(
        "cf.configurationDescription as CURRENCY",
        "cf.configurationValue2 as KOMISYEN"
      );
  } catch (error) {
    console.log(error);
  }

  return result;
}

// INSERT INTO TEMPAHAN DETAILS LIVE STOK TABLES
async function insertTempahanDetailsLiveStok(
  tempahanDetailsId,
  tempahanId,
  implementerId,
  implementerLokasiId,
  wakilPesertaId,
  liveStokId,
  liveStokBahagianId,
  aktivitiId,
  tempahanDetailsLiveStokIdStatus
) {
  let result = null;

  result = await knex.connect("tempahanDetailsLiveStok").insert({
    tempahanDetailsId: tempahanDetailsId,
    tempahanId: tempahanId,
    implementerId: implementerId,
    implementerLokasiId: implementerLokasiId,
    wakilPesertaId: wakilPesertaId,
    liveStokId: liveStokId,
    liveStokBahagianId: liveStokBahagianId,
    // setupKorbanAktivitiId:setupKorbanAktivitiId,
    // aktivitiId: aktivitiId,
    // aktivitiDateId:aktivitiDateId,
  });

  return result;
}

// UPDATE LIVESTOCK BAHAGIAN TABLE
async function updateLiveStockBahagianStatus(
  liveStokBahagianStatus,
  liveStokBahagianStatusCode,
  liveStokId,
  liveStokBahagianId
) {
  let result = null;

  try {
    result = await knex
      .connect("liveStokBahagian")
      .update({
        liveStokBahagianStatus: liveStokBahagianStatus,
        liveStokBahagianStatusCode: liveStokBahagianStatusCode,
      })
      .where({
        liveStokBahagianStatus: "Active",
        liveStokId: liveStokId,
        liveStokBahagianId: liveStokBahagianId,
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE LIVESTOCK TABLE
async function updateLiveStockStatus(
  liveStokStatus,
  liveStokStatusCode,
  liveStokId
) {
  let result = null;

  try {
    result = await knex
      .connect("liveStok")
      .update({
        liveStokStatus: liveStokStatus,
        liveStokStatusCode: liveStokStatusCode,
      })
      .where("liveStokId", liveStokId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE LIVESTOCK BAHAGIAN TABLE
async function updateLiveStockBahagianStatusPendingReserved(
  liveStokBahagianStatus,
  liveStokBahagianStatusCode,
  liveStokId,
  liveStokBahagianId
) {
  let result = null;

  try {
    result = await knex
      .connect("liveStokBahagian")
      .update({
        liveStokBahagianStatus: liveStokBahagianStatus,
        liveStokBahagianStatusCode: liveStokBahagianStatusCode,
      })
      .where({
        liveStokBahagianStatus: "Pending Reserved",
        liveStokId: liveStokId,
        liveStokBahagianId: liveStokBahagianId,
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE LIVESTOCK BAHAGIAN TABLE
async function updateLiveStockBahagianStatusReserved(
  liveStokBahagianStatus,
  liveStokBahagianStatusCode,
  liveStokId,
  liveStokBahagianId
) {
  let result = null;

  try {
    result = await knex
      .connect("liveStokBahagian")
      .update({
        liveStokBahagianStatus: liveStokBahagianStatus,
        liveStokBahagianStatusCode: liveStokBahagianStatusCode,
      })
      .where({
        liveStokBahagianStatus: "Reserved",
        liveStokId: liveStokId,
        liveStokBahagianId: liveStokBahagianId,
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

// GENERATE RESIT NUMBER
async function generateResitNo() {
  let random = generateRandomNumber(1000, 9999);
  let random2 = generateRandomNumber(1000, 9999);
  let datetime = moment().format("YYYYMMDDHHmmss");
  return random + datetime + random2;
}

// INSERT TABLE RESIT
async function insertResit(
  tempahanId,
  userWakilId,
  resitAmountPaid,
  resitAmountNet,
  resitPaymentMethod,
  resitRefNo,
  resitPaymentStatusCode,
  resitImagePath
) {
  let result = null;
  let datetime = getDateTimeFormat();
  let resitNo = await generateResitNo();

  result = await knex.connect("resit").insert({
    resitNo: resitNo, //setel
    tempahanId: tempahanId, //get from tempahanNumber
    userWakilId: userWakilId, // get from maklumatPendaftaran:Array
    resitAmountPaid: resitAmountPaid, //get from tempahanNumber
    resitAmountNet: resitAmountNet,
    // resitChargeFee: resitChargeFee,
    // resitRefNo: resitRefNo,
    // resitFPXCCNo: resitFPXCCNo,
    resitPaymentMethod: resitPaymentMethod, //fpx/cc/transfer
    resitRefNo: resitRefNo,
    resitDateCreated: datetime, //setel
    resitPaymentStatus: "Pending",
    resitPaymentStatusCode: resitPaymentStatusCode,
    resitImagePath: resitImagePath,
  });

  return result;
}

// GET EJEN DETAILS 2
async function getEjenDetails2(tempahanId) {
  let result = null;

  result = await knex
    .connect("resit")
    .join("tempahan", "resit.tempahanId", "=", "tempahan.tempahanId")
    .join("userEjen", "tempahan.userEjenId", "=", "userEjen.userEjenId")
    .where({
      "resit.tempahanId": tempahanId,
    })
    .select("tempahan.userEjenId", "tempahan.tempahanTotalPrice", "userEjen.*");

  return result;
}

// INSERT INTO EJEN COMMISSION TABLE
async function insertEjenCommission(
  userEjenId,
  tempahanId,
  ejenCommissionType,
  ejenCommissionValue,
  ejenCommissionStatus,
  ejenCommissionStatusCode
) {
  let result = null;

  result = await knex.connect("ejenCommission").insert({
    userEjenId: userEjenId,
    tempahanId: tempahanId,
    ejenCommissionType: ejenCommissionType,
    ejenCommissionValue: ejenCommissionValue,
    ejenCommissionStatus: ejenCommissionStatus,
    ejenCommissionStatusCode: ejenCommissionStatusCode,
  });

  return result;
}

// GET TEMPAHAN DETAILS LIVE STOK
async function getTempahanDetailsLiveStok(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahanDetailsLiveStok")
    .join(
      "liveStok",
      "tempahanDetailsLiveStok.liveStokId",
      "=",
      "liveStok.liveStokId"
    )
    .where({
      tempahanId: tempahanId,
    })
    .select("tempahanDetailsLiveStok.*", "liveStok.liveStokType");

  return result;
}

// GET LIVE STOK BIL BAHAGIAN BAKI
async function getLiveStokBahagianBaki(liveStokId) {
  let result = null;

  result = await knex
    .connect("liveStok")
    .where({
      liveStokId: liveStokId,
    })
    .select("liveStokBilBahagian", "liveStokBilBahagianBaki");

  return result;
}

// UPDATE LIVESTOCK TABLE
async function updateLiveStock(
  liveStokBilBahagianBaki,
  liveStokStatus,
  liveStokStatusCode,
  liveStokId
) {
  let result = null;

  try {
    result = await knex
      .connect("liveStok")
      .update({
        liveStokBilBahagianBaki: liveStokBilBahagianBaki,
        liveStokStatus: liveStokStatus,
        liveStokStatusCode: liveStokStatusCode,
      })
      .where("liveStokId", liveStokId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE RESIT TABLE UPON PAYMENT
async function updateResit(tempahanId, resitPaymentStatus) {
  let result = null;
  let datetime = getDateTimeFormat();

  // let resitAmountNetNew = parseFloat(resitAmountNet) - 0.50

  try {
    result = await knex
      .connect("resit")
      .update({
        resitPaymentStatus: resitPaymentStatus,
        resitDateUpdated: datetime,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE EJEN COMMISSION TABLE UPON PAYMENT
async function updateEjenCommission(
  ejenCommissionStatus,
  ejenCommissionStatusCode,
  tempahanId
) {
  let result = null;

  try {
    result = await knex
      .connect("ejenCommission")
      .update({
        ejenCommissionStatus: ejenCommissionStatus,
        ejenCommissionStatusCode: ejenCommissionStatusCode,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// GET IMPLEMENTER LOKASI HAIWAN JENIS
async function getImplementerLokasiHaiwan(
  implementerId,
  implementerLokasiHaiwanJenis
) {
  let result = null;

  try {
    result = await knex
      .connect("implementerLokasiHaiwan")
      .where({
        implementerId: implementerId,
        implementerLokasiHaiwanJenis: implementerLokasiHaiwanJenis,
      })
      .select(
        "implementerId",
        "jumlahKeseluruhan",
        "jumlahBaki",
        "jumlahKeseluruhanBhgn",
        "jumlahBakiBhgn"
      );
  } catch (error) {
    console.log(error);
  }

  return result;
}

// UPDATE IMPLEMENTER LOKASI HAIWAN TABLE
async function updateImplementerLokasiHaiwan(
  jumlahBaki,
  jumlahBakiBhgn,
  implementerId,
  implementerLokasiHaiwanJenis
) {
  let result = null;

  try {
    console.log(result);

    result = await knex
      .connect("implementerLokasiHaiwan")
      .update({
        jumlahBaki: jumlahBaki,
        jumlahBakiBhgn: jumlahBakiBhgn,
      })
      .where({
        implementerId: implementerId,
        implementerLokasiHaiwanJenis: implementerLokasiHaiwanJenis,
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

// SEND SMS
async function sendSMS(body, phones) {
  let result = null;

  try {
    await axios
      .post(
        "https://manage.smsniaga.com/api/send",
        {
          body: body,
          phones: [phones],
          sender_id: "YAYASAN IKHLAS",
        },
        {
          headers: {
            Authorization: `Bearer ` + process.env.TOKEN,
          },
        }
      )
      .then(function (response) {
        result = response.statusText;
      })
      .catch(function (error) {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function sendWhatsapp(
  modalNamaWakil,
  modalTelefonWakil,
  modalStatusPembayaran,
  modalNoTempahan,
  getResitDetails,
  getRingkasanTempahan
) {
  let result = null;
  let resultSMS = null;
  let resultWhatsapp = null;
  let userNumber = "+6" + modalTelefonWakil;
  let userMessage = null;
  let userWhatsapp = null;
  let ringkasanMessage = "";
  let totalKuantiti = 0;

  for (let i = 0; i < getRingkasanTempahan.length; i++) {
    ringkasanMessage +=
      `\r\n*Haiwan Korban :* ` +
      getRingkasanTempahan[i].tempahanDetailsHaiwanType +
      `\r\n*Bahagian Korban :* ` +
      getRingkasanTempahan[i].kuantiti +
      ` ` +
      getRingkasanTempahan[i].tempahanDetailsBhgnType +
      `\r\n`;
  }
  //TODO: manual tambah quantity
  for (let i = 0; i < getRingkasanTempahan.length; i++) {
    if (getRingkasanTempahan[i].tempahanDetailsBhgnType === 'Ekor'){
      totalKuantiti = totalKuantiti + (getRingkasanTempahan[i].kuantiti * 7)
    }
    else{
      totalKuantiti = totalKuantiti + getRingkasanTempahan[i].kuantiti
    }
  }

  console.log("ringkasanMessage");
  console.log(ringkasanMessage);

  if (modalStatusPembayaran == "Success") {
    userMessage =
      "Tahniah, tempahan anda telah berjaya dibuat. Nombor Rujukan :" +
      modalNoTempahan;

    userWhatsapp =
      `Assalamualaikum Tuan/Puan\r\n` +
      modalNamaWakil +
      `\r\n\r\nAlhamdulillah Pendaftaran Korban Tuan/Puan telah berjaya. Terima kasih kerana telah membuat tempahan ibadah Korban bersama kami di Korban Ikhlas
    \r\n\r\nKami minta jasa baik pihak Tuan/Puan untuk menyimpan nombor ini dalam 'phone contact' sebagai nombor Korban Ikhlas.
    \r\n\r\nPerkembangan terkini Korban Ikhlas akan dikongsikan di channel telegram rasmi Korban Ikhlas sebagai makluman dan rujukan Tuan/Puan:
    \r\n\r\nTelegram Korban Ikhlas - ezy.la/KorbanIkhlasUpdate
    \r\n\r\n=========================
    \r\nBerikut adalah ringkasan tempahan korban anda:
    \r\n\r\nNo. Resit : ` +
      getResitDetails[0].resitNo +
      `\r\nNo. Rujukan : ` +
      modalNoTempahan +
//TODO: tahun 2025 ni kena tuka jadi dynamic sbb mgkn ada more than one jenis haiwan
    `\r\n\r\nHaiwan Korban : Lembu` +
      `\r\nBahagian Korban : ` +
      totalKuantiti +

      `\r\n\r\nJumlah : RM ` +
      getResitDetails[0].resitAmountPaid.toFixed(2) +
      `\r\n\r\n=========================
      \r\n\r\nAkad Peserta
      \r\n\r\nSaya, `+ modalNamaWakil + ` selaku wakil kepada peserta, bersetuju untuk membeli haiwan Korban dan menyerahkan pelaksanaanya kepada urus setia program Korban Ikhlas pada tahun ini kerana Allah Ta'ala.
      \r\n\r\nAkad Penerima
      \r\n\r\nKami pihak Korban Ikhlas menerima tanggungjawab pelaksanaan korban sebagai wakil peserta, seperti yang telah dinyatakan, pada Tahun ini kerana Allah.
      \r\n\r\nDoa Timbalan Mufti Perak Kepada Peserta Korban Ikhlas 2024 : https://bit.ly/AkadKorbanIkhlas`;

  }
  if (modalStatusPembayaran == "Failed") {
    userMessage =
      "Harap maaf, tempahan anda tidak berjaya. Nombor Rujukan: " +
      modalNoTempahan;
    userWhatsapp =
      "Harap maaf, tempahan anda tidak berjaya. Nombor Rujukan: " +
      modalNoTempahan;
  }
  // console.log(userNumber)

  if (modalStatusPembayaran == "Success" || modalStatusPembayaran == "Failed") {
    // HANTAR NOTIFIKASI MELALUI SMS
    await axios
      .post(
        "https://manage.smsniaga.com/api/send",
        {
          body: userMessage,
          phones: [userNumber],
          sender_id: "YAYASAN IKHLAS",
        },
        {
          headers: {
            Authorization: `Bearer ` + process.env.TOKEN,
          },
        }
      )
      .then(function (response) {
        resultSMS = response.statusText;
      })
      .catch(function (error) {
        console.log(error);
      });

    // HANTAR NOTIFIKASI MELALUI WHATSAPP
    await axios
      .post(
        `https://onsend.io/api/v1/send`,
        {
          message: userWhatsapp,
          phone_number: userNumber,
        },
        {
          headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${process.env.TOKEN_ONSEND}`,
            'Content-Type': 'application/json',
          },
        }
      )
      // .post(
      //   `https://api.maytapi.com/api/` +
      //     process.env.PRODUCT_ID +
      //     `/` +
      //     process.env.PHONE_ID +
      //     `/sendMessage`,
      //   {
      //     message: userWhatsapp,
      //     to_number: userNumber,
      //     type: "text",
      //   },
      //   {
      //     headers: {
      //       "Content-Type": "application/json",
      //       "x-maytapi-key": process.env.TOKEN_WHATSAPP,
      //     },
      //   }
      // )
      .then(function (response) {
        resultWhatsapp = response.statusText;
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  result = {
    resultSMS: resultSMS,
    resultWhatsapp: resultWhatsapp,
  };

  return result;
}

// GET RINGKASAN TEMPAHAN DETAILS
async function getResitDetails(tempahanId) {
  let result = null;

  result = await knex
    .connect("resit")
    .where({
      tempahanId: tempahanId,
    })
    .select("resitNo", "resitAmountPaid");
  return result;
}

// GET RINGKASAN TEMPAHAN DETAILS
async function getRingkasanTempahan(tempahanId) {
  let result = null;

  result = await knex
    .connect("tempahan")
    .join(
      "tempahanDetails",
      "tempahan.tempahanId",
      "=",
      "tempahanDetails.tempahanId"
    )
    .where({
      "tempahan.tempahanId": tempahanId,
    })
    .count("tempahanDetails.tempahanDetailsHaiwanType AS kuantiti")
    .select(
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnType"
    )
    .groupBy(
      "tempahanDetails.tempahanDetailsHaiwanType",
      "tempahanDetails.tempahanDetailsBhgnType"
    );

  return result;
}

// CANCEL TEMPAHAN TABLE
async function cancelTempahan(tempahanStatus, tempahanStatusCode, tempahanId) {
  let result = null;

  try {
    result = await knex
      .connect("tempahan")
      .update({
        tempahanStatus: tempahanStatus,
        tempahanStatusCode: tempahanStatusCode,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// CANCEL TEMPAHAN DETAILS TABLE
async function cancelTempahanDetails(
  tempahanStatus,
  tempahanStatusCode,
  tempahanId
) {
  let result = null;

  try {
    result = await knex
      .connect("tempahan")
      .update({
        tempahanStatus: tempahanStatus,
        tempahanStatusCode: tempahanStatusCode,
      })
      .where("tempahanId", tempahanId);
  } catch (error) {
    console.log(error);
  }

  return result;
}

// INSERT INTO REFUND TABLES
async function insertRefund(
  tempahanId,
  refundTotalAmount,
  refundSettlementDate,
  refundSettlementNo,
  refundStatus
) {
  let result = null;
  let datetime = await getDateTimeFormat();
  let refundNo = await generateRefundNo();

  result = await knex.connect("refund").insert({
    refundNo: refundNo,
    tempahanId: tempahanId,
    refundTotalAmount: refundTotalAmount,
    refundCreatedDate: datetime,
    refundSettlementDate: refundSettlementDate,
    refundSettlementNo: refundSettlementNo,
    refundStatus: refundStatus,
  });

  return result;
}

// INSERT INTO REFUND DETAILS TABLES
async function insertRefundDetails(
  refundId,
  tempahanDetailsId,
  refundDetailsAmount,
  refundDetailsStatus
) {
  let result = null;

  result = await knex.connect("refundDetails").insert({
    refundId: refundId,
    tempahanDetailsId: tempahanDetailsId,
    refundDetailsAmount: refundDetailsAmount,
    refundDetailsStatus: refundDetailsStatus,
  });

  return result;
}

async function updatePenamaList(penamaOld, penamaNew) {
  try {
    const userWakil = await knex
      .connect("tempahanDetailsLiveStok")
      .select("wakilPeserta.userWakilId as id")
      .join(
        "wakilPeserta",
        "wakilPeserta.wakilPesertaId",
        "tempahanDetailsLiveStok.wakilPesertaId"
      )
      .where("liveStokBahagianId", penamaOld[0].LIVE_STOK)
      .first();

    for (let i = 0; i < penamaNew.length; i++) {

      console.log(penamaNew[i].WAKIL, penamaOld[i].WAKIL);
      if (penamaNew[i].WAKIL !== penamaOld[i].WAKIL) {
        console.log("different");

        const newWakilPeserta = await knex.connect("wakilPeserta").insert({
          wakilPesertaName: penamaNew[i].WAKIL.toUpperCase(),
          userWakilId: userWakil.id,
        });

        await knex
          .connect("tempahanDetailsLiveStok")
          .update({ wakilPesertaId: newWakilPeserta[0] })
          .where("liveStokBahagianId", penamaOld[i].LIVE_STOK);
      }
    }

    return { status: true };
  } catch (error) {
    console.log(error);
    return { status: false, error };
  }
}

module.exports = {
  getSenaraiNegara,
  getSenaraiHaiwan,
  getDateTime,
  getDateTimeFormat,
  generateRandomNumber,
  generateTempahanNo,
  generateRefundNo,
  getOrganizationId,
  getSetupKorbanId,
  getEjenDetails,
  getUserWakilId,
  getWakilPeserta,
  getStockSelection,
  getTempahanDetails,
  getTempahan,
  getCommissionByParts,
  getEjenDetails2,
  getTempahanDetailsLiveStok,
  getLiveStokBahagianBaki,
  getImplementerLokasiHaiwan,
  getResitDetails,
  getRingkasanTempahan,

  generateResitNo,

  checkEmail,
  checkPhone,
  checkTempahanId,
  checkIfAlreadyInsertIntoTempahanDetailsLiveStock,

  cancelTempahan,
  cancelTempahanDetails,

  insertUser,
  insertUserWakil,
  insertWakilPeserta,
  insertTempahan,
  insertTempahanDetails,
  insertTempahanDetailsLiveStok,
  insertResit,
  insertEjenCommission,
  insertRefund,
  insertRefundDetails,

  updateTempahan,
  updateTempahanTotalPrice,
  updateCancelTempahan,
  updateTempahanDetailsBasedOnTempahanDetailsId,
  updateLiveStockBahagianStatus,
  updateLiveStockBahagianStatusPendingReserved,
  updateLiveStockBahagianStatusReserved,
  updateLiveStockStatus,
  updateLiveStock,
  updateResit,
  updateEjenCommission,
  updateImplementerLokasiHaiwan,

  sendSMS,
  sendWhatsapp,

  updatePenamaList,
};
