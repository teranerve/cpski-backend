const knex = require("../../connection.js");
const moment = require("moment");
const sha = require("sha256");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function getCurrentSetupKorbanId() {
  let year = moment().format("YYYY");

  let setupKorban = await knex
    .connect("setupKorban")
    .where("setupKorbanTahun", year)
    .first();

  if (!setupKorban) return false;

  return setupKorban.setupKorbanId;
}

async function insertUser(
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql = await knex.connect(`user`).insert({
      organizationId: "1",
      userFullname: userFullname,
      userUsername: userUsername,
      userEmail: userEmail,
      userPassword: sha(userPassword),
      userPhoneNo: userPhoneNo,
      userIc: userIc,
      userType: "Ejen",
      userTypeCode: "29",
      userAlamat: userAlamat,
      userAlamatPostCode: userAlamatPostCode,
      userAlamatBandar: userAlamatBandar,
      userAlamatNegeri: userAlamatNegeri,
      userAlamatNegara: userAlamatNegara,
      userBankAccNo: userBankAccNo,
      userBankHolderName: userBankHolderName,
      userBank: userBank,
      userRegistrationDate: datetime,
      userStatus: "Active",
      userStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkUsername(userUsername) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userUsername`, userUsername)
      .where(`userType`, "Ejen");

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkEmail(userEmail) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userEmail`, userEmail)
      .where(`userType`, "Ejen");

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPhoneNo(userPhoneNo) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userPhoneNo`, userPhoneNo)
      .where(`userType`, "Ejen");

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkIc(userIc) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userIc`, userIc)
      .where(`userType`, "Ejen");

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPermalink(userEjenPermalink) {
  let result = null;

  try {
    let sql = await knex
      .connect(`userEjen`)
      .where(`userEjenPermalink`, userEjenPermalink);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertUserEjen(
  insertUser,
  userEjenPermalink,
  userEjenCategory,
  userEjenCommissionCategory,
  userEjenCommissionType,
  userEjenCommissionValue,
  userEjenIdReferal,
  setupKorbanId
) {
  let result = null;

  try {
    let sql = await knex.connect(`userEjen`).insert({
      userId: insertUser,
      userEjenPermalink: userEjenPermalink,
      userEjenCategory: userEjenCategory,
      userEjenCommissionCategory: userEjenCommissionCategory,
      userEjenCommissionType: userEjenCommissionType,
      userEjenCommissionValue: userEjenCommissionValue,
      userEjenIdReferal: userEjenIdReferal,
      userEjenStatus: "Active",
      userEjenStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertUserEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getEjen(userType) {
  let result = null;
  let count = null;

  result = await knex.connect.raw(`
  SELECT a.userEjenId, 

  (SELECT userFullname from user 
  where userId = a.userId) USERFULLNAME, 

  (SELECT user.userFullname 
  from user 
  join userEjen on user.userId = userEjen.userId
  where userEjen.userEjenId = a.userEjenIdReferal) REFERENCENAME,

  (SELECT userStatus from user 
  where userId = a.userId) USERSTATUS, 

  (SELECT userId from user 
  where userId = a.userId) USERID, 

  (SELECT DISTINCT SUM(ejenCommissionValue) from 
  ejenCommission where userEjenId = a.userEjenId 
  and ejenCommissionStatus = "Success") TOTALCOMMISSION, 

  (SELECT COUNT(tempahanId) from 
  tempahan where userEjenId = a.userEjenId 
  and tempahanStatus = "Success") TOTALTRANSACTIONS, 

  (SELECT SUM(tempahanTotalPrice) from 
  tempahan where userEjenId = a.userEjenId 
  and tempahanStatus = "Success") RM, 

  (SELECT COUNT(b.tempahanDetailsLiveStokId) from tempahanDetailsLiveStok b 
  left join tempahan c on b.tempahanId = c.tempahanId 
  WHERE c.userEjenId = a.userEjenId  
  and c.tempahanStatus = "Success") JUMLAHBAHAGIAN 

  FROM userEjen a 
  group by a.userEjenId 
  order by (SELECT SUM(ejenCommissionValue) from 
  ejenCommission where userEjenId = a.userEjenId 
  and ejenCommissionStatus = "Success") DESC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenTransaksi(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, SUM(t.tempahanTotalPrice) as jumlahTransaksi, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  WHERE u.userType = 'Ejen'
  AND t.tempahanStatus = 'Success'
  GROUP BY u.userId
  
  UNION
  
    SELECT u.userId, u.userFullname, SUM(t.tempahanTotalPrice) as jumlahTransaksi, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  WHERE u.userType = 'Ejen'
  AND ISNULL(t.tempahanStatus)
  GROUP BY u.userId 
  ORDER BY jumlahTransaksi DESC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenBahagian(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, COUNT(tdl.tempahanDetailsLiveStokId) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  LEFT JOIN tempahanDetails td ON t.tempahanId = td.tempahanId
  LEFT JOIN tempahanDetailsLiveStok tdl ON td.tempahanDetailsId = tdl.tempahanDetailsId 
  WHERE u.userType = 'Ejen'
  AND t.tempahanStatus = 'Success'
  GROUP BY u.userId
  
  UNION
  
    SELECT u.userId, u.userFullname, COUNT(tdl.tempahanDetailsLiveStokId) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  LEFT JOIN tempahanDetails td ON t.tempahanId = td.tempahanId
  LEFT JOIN tempahanDetailsLiveStok tdl ON td.tempahanDetailsId = tdl.tempahanDetailsId 
  WHERE u.userType = 'Ejen'
  AND ISNULL(t.tempahanStatus)
  GROUP BY u.userId 
  ORDER BY jumlahBahagian DESC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByDateAsc(userType) {
  let result = null;
  let count = null;

  result = await knex.connect.raw(`
  SELECT a.userEjenId, 

(SELECT userFullname from user 
where userId = a.userId) USERFULLNAME, 

(SELECT user.userFullname 
from user 
join userEjen on user.userId = userEjen.userId
where userEjen.userEjenId = a.userEjenIdReferal) REFERENCENAME,

(SELECT userStatus from user 
where userId = a.userId) USERSTATUS, 

(SELECT userId from user 
where userId = a.userId) USERID, 

(SELECT DISTINCT SUM(ejenCommissionValue) from 
ejenCommission where userEjenId = a.userEjenId 
and ejenCommissionStatus = "Success") TOTALCOMMISSION, 

(SELECT COUNT(tempahanId) from 
tempahan where userEjenId = a.userEjenId 
and tempahanStatus = "Success") TOTALTRANSACTIONS, 

(SELECT SUM(tempahanTotalPrice) from 
tempahan where userEjenId = a.userEjenId 
and tempahanStatus = "Success") RM, 

(SELECT COUNT(b.tempahanDetailsLiveStokId) from tempahanDetailsLiveStok b 
left join tempahan c on b.tempahanId = c.tempahanId 
WHERE c.userEjenId = a.userEjenId  
and c.tempahanStatus = "Success") JUMLAHBAHAGIAN 

FROM userEjen a 
group by a.userEjenId 
order by (SELECT userRegistrationDate from user
where userId = a.userId) ASC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByDateAscTransaksi(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, SUM(t.tempahanTotalPrice) as jumlahTransaksi,SUM(t.tempahanQuantity) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  WHERE u.userType = 'Ejen'
  AND t.tempahanStatus = 'Success'
  GROUP BY u.userId
  
  UNION
  
    SELECT u.userId, u.userFullname, SUM(t.tempahanTotalPrice) as jumlahTransaksi,SUM(t.tempahanQuantity) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  WHERE u.userType = 'Ejen'
  AND ISNULL(t.tempahanStatus)
  GROUP BY u.userId 
  ORDER BY userRegistrationDate ASC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByDateAscBahagian(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, COUNT(tdl.tempahanDetailsLiveStokId) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  LEFT JOIN tempahanDetails td ON t.tempahanId = td.tempahanId
  LEFT JOIN tempahanDetailsLiveStok tdl ON td.tempahanDetailsId = tdl.tempahanDetailsId 
  WHERE u.userType = 'Ejen'
  AND t.tempahanStatus = 'Success'
  GROUP BY u.userId
  
  UNION
  
    SELECT u.userId, u.userFullname, COUNT(tdl.tempahanDetailsLiveStokId) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  LEFT JOIN tempahanDetails td ON t.tempahanId = td.tempahanId
  LEFT JOIN tempahanDetailsLiveStok tdl ON td.tempahanDetailsId = tdl.tempahanDetailsId 
  WHERE u.userType = 'Ejen'
  AND ISNULL(t.tempahanStatus)
  GROUP BY u.userId 
  ORDER BY userRegistrationDate ASC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByDateDesc(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  // result = await knex.connect.raw(`
  // SELECT u.userId, u.userFullname, SUM(ec.ejenCommissionValue) as jumlahKomisyen, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  // FROM user u LEFT JOIN userEjen ue
  // ON u.userId = ue.userId
  // LEFT JOIN ejenCommission ec
  // ON ue.userEjenId = ec.userEjenId
  // WHERE u.userType = 'Ejen'
  // AND ec.ejenCommissionStatus = 'Success'
  // GROUP BY u.userId

  // UNION

  // SELECT u.userId, u.userFullname, SUM(ec.ejenCommissionValue) as jumlahKomisyen, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  // FROM user u LEFT JOIN userEjen ue
  // ON u.userId = ue.userId
  // LEFT JOIN ejenCommission ec
  // ON ue.userEjenId = ec.userEjenId
  // WHERE u.userType = 'Ejen'
  // AND ISNULL(ec.ejenCommissionStatus)
  // GROUP BY u.userId
  // ORDER BY userRegistrationDate DESC`)

  result = await knex.connect.raw(`
  SELECT a.userEjenId, 

(SELECT userFullname from user 
where userId = a.userId) USERFULLNAME, 

(SELECT user.userFullname 
from user 
join userEjen on user.userId = userEjen.userId
where userEjen.userEjenId = a.userEjenIdReferal) REFERENCENAME,

(SELECT userStatus from user 
where userId = a.userId) USERSTATUS, 

(SELECT userId from user 
where userId = a.userId) USERID, 

(SELECT DISTINCT SUM(ejenCommissionValue) from 
ejenCommission where userEjenId = a.userEjenId 
and ejenCommissionStatus = "Success") TOTALCOMMISSION, 

(SELECT COUNT(tempahanId) from 
tempahan where userEjenId = a.userEjenId 
and tempahanStatus = "Success") TOTALTRANSACTIONS, 

(SELECT SUM(tempahanTotalPrice) from 
tempahan where userEjenId = a.userEjenId 
and tempahanStatus = "Success") RM, 

(SELECT COUNT(b.tempahanDetailsLiveStokId) from tempahanDetailsLiveStok b 
left join tempahan c on b.tempahanId = c.tempahanId 
WHERE c.userEjenId = a.userEjenId  
and c.tempahanStatus = "Success") JUMLAHBAHAGIAN 

FROM userEjen a 
group by a.userEjenId 
order by (SELECT userRegistrationDate from user
where userId = a.userId) DESC;`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByDateDescTransaksi(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, SUM(t.tempahanTotalPrice) as jumlahTransaksi,SUM(t.tempahanQuantity) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  WHERE u.userType = 'Ejen'
  AND t.tempahanStatus = 'Success'
  GROUP BY u.userId
  
  UNION
  
    SELECT u.userId, u.userFullname, SUM(t.tempahanTotalPrice) as jumlahTransaksi,SUM(t.tempahanQuantity) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  WHERE u.userType = 'Ejen'
  AND ISNULL(t.tempahanStatus)
  GROUP BY u.userId 
  ORDER BY userRegistrationDate DESC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByDateDescBahagian(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, COUNT(tdl.tempahanDetailsLiveStokId) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  LEFT JOIN tempahanDetails td ON t.tempahanId = td.tempahanId
  LEFT JOIN tempahanDetailsLiveStok tdl ON td.tempahanDetailsId = tdl.tempahanDetailsId 
  WHERE u.userType = 'Ejen'
  AND t.tempahanStatus = 'Success'
  GROUP BY u.userId
  
  UNION
  
    SELECT u.userId, u.userFullname, COUNT(tdl.tempahanDetailsLiveStokId) as jumlahBahagian, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN tempahan t ON t.userEjenId = ue.userEjenId
  LEFT JOIN tempahanDetails td ON t.tempahanId = td.tempahanId
  LEFT JOIN tempahanDetailsLiveStok tdl ON td.tempahanDetailsId = tdl.tempahanDetailsId 
  WHERE u.userType = 'Ejen'
  AND ISNULL(t.tempahanStatus)
  GROUP BY u.userId 
  ORDER BY userRegistrationDate DESC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenSortByCommisionDesc(userType) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  // result = await knex.connect(`user`)
  // .select('user.userId','user.userFullname', 'user.userAlamatNegara', 'user.userRegistrationDate', 'user.userStatus')
  // .sum(`ejenCommission.ejenCommissionValue`, { as: 'jumlahKomisyen' })
  // .leftJoin(`userEjen`, `user.userId`, `userEjen.userId`)
  // .leftJoin(`ejenCommission`, `userEjen.userEjenId`, `ejenCommission.userEjenId`)
  // .where(`user.userType`, userType)
  // .where(`ejenCommission.ejenCommissionStatus`, 'Success')
  // .groupBy(`user.userId`)

  result = await knex.connect.raw(`
  SELECT u.userId, u.userFullname, SUM(ec.ejenCommissionValue) as jumlahKomisyen, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN ejenCommission ec
  ON ue.userEjenId = ec.userEjenId
  WHERE u.userType = 'Ejen'
  AND ec.ejenCommissionStatus = 'Success'
  GROUP BY u.userId
  
  UNION 
  
  SELECT u.userId, u.userFullname, SUM(ec.ejenCommissionValue) as jumlahKomisyen, u.userAlamatNegara, u.userRegistrationDate, u.userStatus
  FROM user u LEFT JOIN userEjen ue
  ON u.userId = ue.userId
  LEFT JOIN ejenCommission ec
  ON ue.userEjenId = ec.userEjenId
  WHERE u.userType = 'Ejen'
  AND ISNULL(ec.ejenCommissionStatus)
  GROUP BY u.userId 
  ORDER BY jumlahKomisyen DESC`);

  console.log(result);

  count = await knex.connect(`user`).count().where(`userType`, userType);

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getEjenCommission(userEjenId) {
  let result = null;

  result = await knex
    .connect(`ejenCommission`)
    .sum(`ejenCommission.ejenCommissionValue`, { as: "jumlahKomisyen" })
    .where(`ejenCommission.ejenCommissionStatus`, "Success")
    .where(`ejenCommission.userEjenId`, userEjenId);
}

async function checkUsername2(userUsername, userId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userUsername`, userUsername)
      .where(`userType`, "Ejen")
      .whereNot(`userId`, userId);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkEmail2(userEmail, userId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userEmail`, userEmail)
      .where(`userType`, "Ejen")
      .whereNot(`userId`, userId);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPhoneNo2(userPhoneNo, userId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userPhoneNo`, userPhoneNo)
      .where(`userType`, "Ejen")
      .whereNot(`userId`, userId);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkIc2(userIc, userId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userIc`, userIc)
      .where(`userType`, "Ejen")
      .whereNot(`userId`, userId);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPermalink2(userEjenPermalink, userId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`userEjen`)
      .where(`userEjenPermalink`, userEjenPermalink)
      .whereNot(`userId`, userId);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPassword2(userPassword, userId) {
  let result = null;
  let userPassword2 = sha(userPassword);
  console.log(`password:---`, userPassword);
  console.log(`hashhhpassword:---`, userPassword2);
  console.log(`userIDDDD:---`, userId);

  try {
    //compare with hashed pass
    let sql = await knex
      .connect(`user`)
      .where(`userPassword`, userPassword2)
      .where(`userId`, userId);

    //compare with normal pass
    let sql2 = await knex
      .connect(`user`)
      .where(`userPassword`, userPassword)
      .where(`userId`, userId);

    console.log("getUser: ", sql);
    if (!sql || (sql.length == 0 && !sql2) || sql2.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateEjen(
  userId,
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userStatus,
  userBankAccNo,
  userBankHolderName,
  userBank
) {
  console.log(`lalu biasa matt`);
  let result = null;

  try {
    if (userStatus == "Active") {
      let sql = await knex
        .connect(`user`)
        .update({
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: sha(userPassword),
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userStatus: userStatus,
          userStatusCode: 20,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
        })
        .where(`userId`, userId);
    } else {
      let sql = await knex
        .connect(`user`)
        .update({
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: sha(userPassword),
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userStatus: userStatus,
          userStatusCode: 21,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
        })
        .where(`userId`, userId);
    }

    console.log("updateEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateEjenNoPassword(
  userId,
  userFullname,
  userUsername,
  userEmail,
  userPhoneNo,
  userIc,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userStatus,
  userBankAccNo,
  userBankHolderName,
  userBank
) {
  console.log(`lalu no password`);
  let result = null;

  try {
    if (userStatus == "Active") {
      let sql = await knex
        .connect(`user`)
        .update({
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userStatus: userStatus,
          userStatusCode: 20,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
        })
        .where(`userId`, userId);
    } else {
      let sql = await knex
        .connect(`user`)
        .update({
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userStatus: userStatus,
          userStatusCode: 21,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
        })
        .where(`userId`, userId);
    }

    console.log("updateEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateEjen2(
  userId,
  userEjenPermalink,
  userEjenCategory,
  userEjenCommissionCategory,
  userEjenCommissionType,
  userEjenCommissionValue,
  userEjenIdReferal
) {
  console.log(`lalu biasa matt`);
  let result = null;

  try {
    let sql = await knex
      .connect(`userEjen`)
      .update({
        userId: userId,
        userEjenPermalink: userEjenPermalink,
        userEjenCategory: userEjenCategory,
        userEjenCommissionCategory: userEjenCommissionCategory,
        userEjenCommissionType: userEjenCommissionType,
        userEjenCommissionValue: userEjenCommissionValue,
        userEjenIdReferal: userEjenIdReferal,
        userEjenStatus: "Active",
        userEjenStatusCode: "20",
      })
      .where(`userId`, userId);

    console.log("updateEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUser(username, fullname) {
  let result = null;

  try {
    result = await knex
      .connect(`user`)
      .update({
        userFullname: fullname,
      })
      .where(`userUsername`, username);

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function deleteUser(username) {
  let result = null;

  try {
    result = await knex.connect(`user`).where({ userUsername: username }).del();

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getButiranEjen(userId) {
  let result = null;

  result = await knex
    .connect(`user`)
    .join(`userEjen`, `user.userId`, `userEjen.userId`)
    .where(`user.userId`, userId);

  return result;
}

async function getEjenSettlement() {
  let result = null;

  /*  
    SELECT
      SUM(ejenCommission.ejenCommissionValue),
      settlementLookup.settlementLookupId,
      settlementLookup.settlementLookupDate,
      settlementLookup.settlementLookupCreatedDate,
      settlementLookup.settlementLookupUpdatedDate,
      settlementLookup.settlementLookupStatus
    FROM
        settlementLookup
        JOIN ejenCommission ON settlementLookup.settlementLookupId = ejenCommission.settlementLookupId
    GROUP BY
        settlementLookup.settlementLookupId,
        settlementLookup.settlementLookupDate,
        settlementLookup.settlementLookupStatus	
  */

  result = await knex.connect.raw(`SELECT
    IFNULL(SUM(ejenCommission.ejenCommissionValue),0) as jumlahKomisyen,
    settlementLookup.settlementLookupId,
    settlementLookup.settlementLookupDate,
    settlementLookup.settlementLookupCreatedDate,
    settlementLookup.settlementLookupUpdatedDate,
    settlementLookup.settlementLookupStatus
FROM
    settlementLookup
    LEFT JOIN ejenCommission ON settlementLookup.settlementLookupId = ejenCommission.settlementLookupId
GROUP BY
    settlementLookup.settlementLookupId,
    settlementLookup.settlementLookupDate,
    settlementLookup.settlementLookupStatus`);

  // result = await knex.connect(`settlementLookup`)
  //   .join('ejenCommission', 'settlementLookup.settlementLookupId', '=', 'ejenCommission.settlementLookupId')
  //   .sum('ejenCommission.ejenCommissionValue as jumlahKomisyen')
  //   .select(
  //     'settlementLookup.settlementLookupId',
  //     'settlementLookup.settlementLookupDate',
  //     'settlementLookup.settlementLookupCreatedDate',
  //     'settlementLookup.settlementLookupUpdatedDate',
  //     'settlementLookup.settlementLookupStatus',
  //   )
  //   .groupBy(
  //     'settlementLookup.settlementLookupId',
  //     'settlementLookup.settlementLookupDate',
  //     'settlementLookup.settlementLookupStatus',
  //   )

  return result[0];
}

async function getEjenSettlementDetails(settlementLookupId) {
  let result = null;

  // GET SETTLEMENT SUMMARY
  result = await knex.connect.raw(
    `SELECT
    (
        SELECT
            IFNULL(SUM(ejenCommission.ejenCommissionValue), 0)
        FROM
            ejenCommission
        WHERE
            ejenCommission.settlementLookupId = ` +
      settlementLookupId +
      `
    ) jumlahKomisyen,
    (
        SELECT
            COUNT(DISTINCT(ejenCommission.userEjenId))
        FROM
            ejenCommission
        WHERE
            ejenCommission.settlementLookupId = ` +
      settlementLookupId +
      `
    ) jumlahEjen,
    settlementLookup.settlementLookupId,
    settlementLookup.settlementLookupDate,
    settlementLookup.settlementLookupCreatedDate,
    settlementLookup.settlementLookupUpdatedDate,
    settlementLookup.settlementLookupStatus
FROM
    settlementLookup
WHERE
    settlementLookup.settlementLookupId = ` +
      settlementLookupId
  );

  // GET LIST OF EJEN UNDER THE CURRENT SPECIFIC SETTLEMENT DATE
  list = await knex.connect.raw(
    `SELECT
    ejenCommission.userEjenId,
    (
        SELECT
            IFNULL(SUM(ejenCommission.ejenCommissionValue), 0)
        FROM
            ejenCommission
        WHERE
            ejenCommission.settlementLookupId = ` +
      settlementLookupId +
      `
    ) ejenCommissionValue,
    user.userFullname,
    settlementLookup.settlementLookupId,
    ejenCommission.ejenCommissionStatus,
    ejenCommission.ejenCommissionReferenceNo
FROM
    settlementLookup
    LEFT JOIN ejenCommission ON settlementLookup.settlementLookupId = ejenCommission.settlementLookupId
    JOIN userEjen ON ejenCommission.userEjenId = userEjen.userEjenId
    JOIN user ON userEjen.userId = user.userId
    JOIN tempahan ON ejenCommission.userEjenId = tempahan.userEjenId
    JOIN tempahanDetails ON tempahan.tempahanId = tempahanDetails.tempahanId
    JOIN implementer ON tempahanDetails.implementerId = implementer.implementerId
WHERE
    ejenCommission.settlementLookupId = ` +
      settlementLookupId +
      `
    AND tempahanDetails.tempahanDetailsStatus = 'Success'
GROUP BY
    ejenCommission.userEjenId,
    user.userFullname,
    settlementLookup.settlementLookupId,
    ejenCommission.ejenCommissionStatus,
    ejenCommission.ejenCommissionReferenceNo`
  );

  return [result[0][0], list[0]];
}

// INSERT NEW SETTLEMENT INTO SETTLEMENTLOOKUP TABLE
async function insertSettlementDate(settlementLookupDate) {
  let result = null;
  let datetime = getDateTime();

  try {
    result = await knex.connect(`settlementLookup`).insert({
      settlementLookupDate: settlementLookupDate,
      settlementLookupCreatedDate: datetime,
      settlementLookupUpdatedDate: datetime,
      settlementLookupStatus: "Active",
    });
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getEjenSettlementTempahan(settlementLookupId, userEjenId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT
    tempahan.*
FROM
    settlementLookup
    JOIN ejenCommission ON settlementLookup.settlementLookupId = ejenCommission.settlementLookupId
    JOIN tempahan ON ejenCommission.tempahanId = tempahan.tempahanId
WHERE
    settlementLookup.settlementLookupId = ` +
      settlementLookupId +
      `
    AND ejenCommission.userEjenId = ` +
      userEjenId
  );

  return result[0];
}

async function updateEjenCommission(
  settlementLookupId,
  userEjenId,
  transactionNo
) {
  let result = null;
  let result2 = null;
  let datetime = getDateTime();

  try {
    result = await knex
      .connect(`ejenCommission`)
      .update({
        ejenCommissionReferenceNo: transactionNo,
        ejenCommissionStatus: "Settlement Done",
        ejenCommissionUpdatedDate: datetime,
        // ejenCommissionStatusCode: 22
      })
      .where({
        settlementLookupId: settlementLookupId,
        userEjenId: userEjenId,
        ejenCommissionStatus: "Success",
      });

    count = await knex
      .connect(`ejenCommission`)
      .join(
        `settlementLookup`,
        `ejenCommission.settlementLookupId`,
        `=`,
        `settlementLookup.settlementLookupId`
      )
      .count("* as count")
      .where("ejenCommission.ejenCommissionStatus", "=", "Success")
      .where("settlementLookup.settlementLookupId", "=", settlementLookupId);

    // console.log(`---count-----`)
    // console.log(count[0].count)

    if (count[0].count == 0) {
      result2 = await knex
        .connect(`settlementLookup`)
        .update({
          settlementLookupStatus: "Settlement Done",
          settlementLookupUpdatedDate: datetime,
        })
        .where({
          settlementLookupId: settlementLookupId,
          settlementLookupStatus: "Active",
        });
    }
    /**
     * select
    count(*)
from
    ejenCommission
    join settlementLookup on ejenCommission.settlementLookupId = settlementLookup.settlementLookupId
where
    ejenCommission.ejenCommissionStatus = 'Success'
    and settlementLookup.settlementLookupId = 1
     */
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateAllEjenCommission(settlementLookupId, transactionNo) {
  let result = null;
  let result2 = null;
  let datetime = getDateTime();

  try {
    result = await knex
      .connect(`ejenCommission`)
      .update({
        ejenCommissionReferenceNo: transactionNo,
        ejenCommissionStatus: "Settlement Done",
        ejenCommissionUpdatedDate: datetime,
        // ejenCommissionStatusCode: 22
      })
      .where({
        settlementLookupId: settlementLookupId,
        ejenCommissionStatus: "Success",
      });

    result2 = await knex
      .connect(`settlementLookup`)
      .update({
        settlementLookupStatus: "Settlement Done",
        settlementLookupUpdatedDate: datetime,
      })
      .where({
        settlementLookupId: settlementLookupId,
        settlementLookupStatus: "Active",
      });
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function countActiveSettlement(settlementLookupId) {
  let result = null;

  try {
    result = await knex
      .connect(`ejenCommission`)
      .join(
        `settlementLookup`,
        `ejenCommission.settlementLookupId`,
        `=`,
        `settlementLookup.settlementLookupId`
      )
      .count("* as count")
      .where("ejenCommission.ejenCommissionStatus", "=", "Success")
      .where("settlementLookup.settlementLookupId", "=", settlementLookupId);
  } catch (error) {
    console.log(error);
  }

  return result[0];
}

async function getEjenDuta(ejenDuta) {
  return await knex
    .connect("user")
    .select("userUsername")
    .where("userId", ejenDuta)
    .first();
}

async function getEjenCawangan(ejenDuta, tahun) {
  // Get duta userId
  let ejenDutaId = await knex
    .connect("userEjen")
    .select("userEjenId as id")
    .where("userId", ejenDuta)
    .first();
  console.log("ejenDutaId: ", ejenDutaId);

  if (!ejenDutaId) return false;

  // Get cawangans with ejenDutaId and count of tempahan with tempahanStatus = "Success"

  let cawangans = null;
  if (!tahun || tahun === "all") {
    cawangans = await knex
      .connect("userEjen")
      .select(
        "userUsername",
        "userFullname",
        "userEjen.setupKorbanId",
        "userEjenStatus"
      )
      .count("tempahan.tempahanId as tempahanCount")
      .sum("tempahan.tempahanTotalPrice as tempahanTotalPrice")
      .leftJoin("user", "user.userId", "userEjen.userId")
      .leftJoin("tempahan", "tempahan.userEjenId", "userEjen.userEjenId")
      .where("userEjenIdReferal", ejenDutaId.id)
      .andWhere("tempahan.tempahanStatus", "Success")
      .groupBy(
        "userUsername",
        "userFullname",
        "userEjen.setupKorbanId",
        "userEjenStatus"
      );
  } else {
    cawangans = await knex
      .connect("userEjen")
      .select(
        "userUsername",
        "userFullname",
        "userEjen.setupKorbanId",
        "userEjenStatus"
      )
      .count("tempahan.tempahanId as tempahanCount")
      .sum("tempahan.tempahanTotalPrice as tempahanTotalPrice")
      .leftJoin("user", "user.userId", "userEjen.userId")
      .leftJoin("tempahan", "tempahan.userEjenId", "userEjen.userEjenId")
      .where("userEjenIdReferal", ejenDutaId.id)
      .andWhere("tempahan.tempahanStatus", "Success")
      .andWhere("userEjen.setupKorbanId", tahun)
      .groupBy(
        "userUsername",
        "userFullname",
        "userEjen.setupKorbanId",
        "userEjenStatus"
      );
  }

  return cawangans;
}

module.exports = {
  getCurrentSetupKorbanId,
  insertUser,
  insertUserEjen,
  insertSettlementDate,

  getEjen,
  getEjenTransaksi,
  getEjenBahagian,
  getEjenSortByDateAsc,
  getEjenSortByDateAscTransaksi,
  getEjenSortByDateAscBahagian,
  getEjenSortByDateDesc,
  getEjenSortByDateDescTransaksi,
  getEjenSortByDateDescBahagian,
  getEjenSortByCommisionDesc,
  getButiranEjen,
  getEjenCommission,
  getEjenSettlement,
  getEjenSettlementDetails,
  getEjenSettlementTempahan,

  updateUser,

  deleteUser,

  checkUsername,
  checkEmail,
  checkPhoneNo,
  checkIc,
  checkPermalink,
  checkUsername2,
  checkEmail2,
  checkPhoneNo2,
  checkIc2,
  checkPermalink2,
  checkPassword2,

  countActiveSettlement,

  updateEjen,
  updateEjen2,
  updateEjenNoPassword,
  updateEjenCommission,
  updateAllEjenCommission,

  getEjenDuta,
  getEjenCawangan,
};
