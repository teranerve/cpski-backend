const knex = require("../../connection.js");
const moment = require("moment");

async function getConfig(confLookupGroupCode) {
  let result = null;

  result = await knex
    .connect(`confLookup`)
    .select(`confLookupId`, `confLookupCode`)
    .where(`confLookupGroupCode`, confLookupGroupCode)
    .where(`confLookupStatus`, "Active");

  // result = await knex.connect.raw(`select confLookupCode from confLookup where confLookupId between 7 and 11 or
  // confLookupId = 5`)

  return result;
}

async function getNegara() {
  let result = null;

  result = await knex.connect.raw(
    `select confLookupCode from confLookup where confLookupId BETWEEN 8 and 11 or confLookupId = 5 or confLookupId = 62`
  );

  console.log(result);

  return result;
}

async function getNegaraImplementer() {
  let result = null;

  result = await knex
    .connect(`confLookup`)
    .select(`confLookupId`, `confLookupCode`)
    .whereIn(`confLookupGroupCode`, ["pilihan_negara", "pilihan_negara2"])
    .where(`confLookupStatus`, "Active")
    .whereNot(`confLookupCode`, "Kemboja");

  console.log(result);

  return result;
}

async function getListPengguna() {
  let result = null;

  result = await knex.connect
    .raw(`select confLookupCode from confLookup where confLookupId between 25 and 27 or 
    confLookupId between 29 and 30`);

  console.log(result);

  return result;
}

async function getListJenisEjen() {
  let result = null;

  result = await knex.connect.raw(
    `select confLookupCode from confLookup where confLookupGroupCode = "Ejen" and confLookupStatus = "Active"`
  );

  console.log(result);

  return result;
}

async function getListDutaRef() {
  let result = null;

  result = await knex.connect.raw(
    `SELECT user.userFullname,userEjen.userEjenId FROM user JOIN userEjen ON user.userId = userEjen.userId WHERE user.userType = 'Ejen' AND userEjen.userEjenCategory = 'duta'`
  );

  console.log(result);

  return result;
}

async function getTahunKorban() {
  let tahunkorban = null;

  tahunkorban = await knex
    .connect(`setupKorban`)
    .select(`setupKorbanId`, `setupKorbanTahun`, `setupKorbanStatus`);

  // console.log(tahunkorban);

  return { tahunkorban };
}

async function insertTahunKorban({ tahunKorban, tahunStatus }) {
  let insert = null;

  try {
    insert = await knex.connect(`setupKorban`).insert({
      organizationId: 1,
      setupKorbanTahun: tahunKorban,
      setupKorbanStatus: tahunStatus,
      setupKorbanStatusCode: 20,
    });
    console.log(insert);
  } catch (error) {
    console.log(error);
  }

  return insert;
}

async function getTempatKorban() {
  const tempatkorban = await knex.connect
    .select(
      "cl.confLookupGroupCode",
      "cl.confLookupCode",
      "cl.confLookupStatus",
      "cl.setupKorbanId",
      "sk.setupKorbanTahun",
      "cl.confLookupParentCode as kawasanCode",
      knex.connect.raw(`
      (SELECT confLookupCode FROM confLookup WHERE confLookupId = kawasanCode) AS jenisKawasan
    `)
    )
    .from("confLookup as cl")
    .leftJoin("setupKorban as sk", "cl.setupKorbanId", "sk.setupKorbanId")
    .where("confLookupGroupCode", "pilihan_negara");

  // let overview = await knex
  //   .connect("confLookup as cl")
  //   .count("confLookupCode as overview")
  //   .where("confLookupGroupCode", "pilihan_negara")
  //   .first();

  return { tempatkorban };
}

async function getListKawasan() {
  let kawasan = null;
  let latestTahunKorban = null;

  kawasan = await knex
    .connect("confLookup")
    .select("confLookupId", "confLookupCode")
    .where("confLookupGroupCode", "pilihan_kawasan");

  latestTahunKorban = await knex
    .connect("setupKorban")
    .select("setupKorbanId", "setupKorbanTahun")
    .where("setupKorbanStatus", "Active")
    .orderBy("setupKorbanId", "desc")
    .limit(1);

  return { kawasan, latestTahunKorban };
}

async function insertTempatKorban({
  setupTahunId,
  namaNegara,
  kawasan,
  shortformNegara,
}) {
  //START QUERY INSERT UNTUK NAMA NEGARA DALAM TABLE confLookup
  let insertTempat = await knex.connect("confLookup").insert({
    setupKorbanId: setupTahunId,
    confLookupGroupCode: "pilihan_negara",
    confLookupCode: namaNegara,
    confLookupParentCode: kawasan,
    confLookupStatus: "Active",
  });

  console.log("insertTempat", insertTempat);

  if (!insertTempat) {
    throw "ERROR Insert into table confLookup!";
  }

  confLookupId = insertTempat[0];
  //END QUERY INSERT UNTUK NAMA NEGARA DALAM TABLE confLookup

  //START QUERY INSERT UNTUK SHORTFORM NAMA NEGARA DALAM TABLE configuration
  let setupShortformNegara = await knex.connect("configuration").insert({
    setupKorbanId: setupTahunId,
    configurationLevel: 1,
    configurationName: "ringkasan_nama_negara",
    configurationStatus: "Active",
    configurationParentId: 87,
    configurationValue1: confLookupId,
    configurationValue2: shortformNegara,
  });

  console.log("setupShortformNegara", setupShortformNegara);

  if (!setupShortformNegara) {
    throw "ERROR Insert into table configuration!";
  }
  //END QUERY INSERT UNTUK SHORTFORM NAMA NEGARA DALAM TABLE configuration

  //START QUERY SELECT UNTUK DAPATKAN ID HAIWAN 15,16,17 DALAM TABLE confLookup
  let configCodeHaiwan = await knex
    .connect("confLookup")
    .select("confLookupId")
    .where("confLookupGroupCode", "haiwan");

  let configHaiwanId = configCodeHaiwan.map((row) => row.confLookupId);

  console.log("configCodeHaiwan", configCodeHaiwan);
  //END QUERY SELECT UNTUK DAPATKAN ID HAIWAN 15,16,17 DALAM TABLE confLookup

  //START QUERY INSERT UNTUK HARGA BAHAGIAN LEVEL 1 DALAM TABLE configuration
  let setupHargaBahagianlvl1 = await knex.connect("configuration").insert({
    setupKorbanId: setupTahunId,
    configurationLevel: 1,
    configurationName: "pilihan_negara",
    configurationStatus: "Active",
    configurationParentId: 2,
    configurationValue1: confLookupId,
  });

  console.log("setupHargaBahagianlvl1", setupHargaBahagianlvl1);

  if (!setupHargaBahagianlvl1) {
    throw "ERROR Insert setupHargaBahagianlvl1!";
  }

  configurationBahagianId = setupHargaBahagianlvl1[0];
  //END QUERY INSERT UNTUK HARGA BAHAGIAN LEVEL 1 DALAM TABLE configuration

  //START QUERY INSERT UNTUK HARGA BAHAGIAN LEVEL 2 DALAM TABLE configuration
  for (let i = 0; i < configHaiwanId.length; i++) {
    let setupHargaBahagianlvl2 = await knex.connect("configuration").insert({
      setupKorbanId: setupTahunId,
      configurationLevel: 2,
      configurationStatus: "Active",
      configurationParentId: configurationBahagianId,
      configurationValue1: configHaiwanId[i],
      configurationValue2: "",
    });

    console.log("setupHargaBahagianlvl2", setupHargaBahagianlvl2);

    if (!setupHargaBahagianlvl2) {
      throw "ERROR Insert setupHargaBahagianlvl2!";
    }
  }
  //END QUERY INSERT UNTUK HARGA BAHAGIAN LEVEL 2 DALAM TABLE configuration

  //START QUERY INSERT UNTUK HARGA SEEKOR LEVEL 1 DALAM TABLE configuration
  let setupHargaSeekorlvl1 = await knex.connect("configuration").insert({
    setupKorbanId: setupTahunId,
    configurationLevel: 1,
    configurationName: "pilihan_negara",
    configurationStatus: "Active",
    configurationParentId: 7,
    configurationValue1: confLookupId,
  });

  console.log("setupHargaSeekorlvl1", setupHargaSeekorlvl1);

  if (!setupHargaSeekorlvl1) {
    throw "ERROR Insert setupHargaSeekorlvl1!";
  }

  configurationSeekorId = setupHargaSeekorlvl1[0];
  //END QUERY INSERT UNTUK HARGA SEEKOR LEVEL 1 DALAM TABLE configuration

  //START QUERY INSERT UNTUK HARGA SEEKOR LEVEL 2 DALAM TABLE configuration
  for (let i = 0; i < configHaiwanId.length; i++) {
    let setupHargaSeekorlvl2 = await knex.connect("configuration").insert({
      setupKorbanId: setupTahunId,
      configurationLevel: 2,
      configurationStatus: "Active",
      configurationParentId: configurationSeekorId,
      configurationValue1: configHaiwanId[i],
      configurationValue2: "",
    });

    console.log("setupHargaSeekorlvl2", setupHargaSeekorlvl2);

    if (!setupHargaSeekorlvl2) {
      throw "ERROR Insert setupHargaSeekorlvl2!";
    }
  }
  //END QUERY INSERT UNTUK HARGA SEEKOR LEVEL 2 DALAM TABLE configuration

  //START QUERY INSERT UNTUK KOMISYEN EJEN LEVEL 1 DALAM TABLE configuration
  let setupKomisyenEjenlvl1 = await knex.connect("configuration").insert({
    setupKorbanId: setupTahunId,
    configurationLevel: 2,
    configurationName: "kawasan jualan ejen",
    configurationStatus: "Active",
    configurationParentId: 119,
    configurationValue1: confLookupId,
  });

  console.log("setupKomisyenEjenlvl1", setupKomisyenEjenlvl1);

  if (!setupKomisyenEjenlvl1) {
    throw "ERROR Insert setupKomisyenEjenlvl1!";
  }

  configurationKomisyenId = setupKomisyenEjenlvl1[0];
  //END QUERY INSERT UNTUK KOMISYEN EJEN LEVEL 1 DALAM TABLE configuration

  //START QUERY INSERT UNTUK KOMISYEN EJEN LEVEL 2 DALAM TABLE configuration
  for (let i = 0; i < configHaiwanId.length; i++) {
    let setupKomisyenEjenlvl2 = await knex.connect("configuration").insert({
      setupKorbanId: setupTahunId,
      configurationLevel: 3,
      configurationName: "jenis haiwan dan komisyen",
      configurationDescription: "RM",
      configurationStatus: "Active",
      configurationParentId: configurationKomisyenId,
      configurationValue1: configHaiwanId[i],
      configurationValue2: "NULL",
    });

    console.log("setupKomisyenEjenlvl2", setupKomisyenEjenlvl2);

    if (!setupKomisyenEjenlvl2) {
      throw "ERROR Insert setupKomisyenEjenlvl2!";
    }
  }
  //END QUERY INSERT UNTUK KOMISYEN EJEN LEVEL 2 DALAM TABLE configuration
}

async function getHargaHaiwan() {
  const hargahaiwan = await knex
    .connect("configuration as A")
    .select(
      "A.configurationId as configurationId_A",
      "A.configurationValue1 as negaraId",
      "B.confLookupCode AS negara",
      "C.configurationValue1 AS haiwanId",
      "D.confLookupCode as haiwan",
      "C.configurationValue2 as harga",
      "A.configurationParentId AS hargaHaiwanId",
      knex.connect.raw(`
      (SELECT configurationName FROM configuration WHERE configurationId = hargaHaiwanId) AS jenisHargaHaiwan
    `)
    )
    .join("confLookup as B", "A.configurationValue1", "B.confLookupId")
    .join("configuration as C", "A.configurationId", "C.configurationParentId")
    .join("confLookup as D", "C.configurationValue1", "D.confLookupId")
    // .where("A.configurationName", "pilihan negara")
    .where("A.configurationName", "IN", ["pilihan negara", "pilihan_negara"])
    .orderBy("B.confLookupCode", "asc");

  // const overview = await knex.connect
  //   .select(
  //     "A.configurationId as configurationId_A",
  //     "A.configurationValue1 as negaraId",
  //     "B.confLookupCode AS negara",
  //     "C.configurationValue1 AS haiwanId",
  //     "D.confLookupCode as haiwan"
  //   )
  //   .count("D.confLookupCode as totalHaiwan")
  //   .from("configuration as A")
  //   .join("confLookup as B", "A.configurationValue1", "B.confLookupId")
  //   .join("configuration as C", "A.configurationId", "C.configurationParentId")
  //   .join("confLookup as D", "C.configurationValue1", "D.confLookupId")
  //   .where("D.confLookupCode", "Lembu")
  //   .where("A.configurationName", "pilihan negara")
  //   .groupBy("A.configurationId", "C.configurationValue1", "D.confLookupCode")
  //   .first();

  return { hargahaiwan };
}

async function getKomisyenEjen() {
  const komisyenejen = await knex
    .connect("configuration as A")
    .select(
      "A.configurationParentId AS configParentId",
      "C.configurationValue1 AS negaraId",
      "D.confLookupCode AS negara",
      "A.configurationValue1 AS haiwanId",
      "B.confLookupCode AS haiwan",
      "A.configurationValue2 AS komisyen"
    )
    .join("confLookup AS B", "A.configurationValue1", "B.confLookupId")
    .join("configuration AS C", "A.configurationParentId", "C.configurationId")
    .join("confLookup AS D", "C.configurationValue1", "D.confLookupId")
    .where("A.configurationName", "jenis haiwan dan komisyen")
    .andWhere("C.configurationParentId", 119);

  return { komisyenejen };
}

module.exports = {
  getConfig,
  getNegara,
  getNegaraImplementer,
  getListPengguna,
  getListJenisEjen,
  getListDutaRef,
  getTahunKorban,
  insertTahunKorban,
  getTempatKorban,
  getListKawasan,
  insertTempatKorban,
  getHargaHaiwan,
  getKomisyenEjen,
};
