const knex = require("../../connection.js");
const moment = require("moment");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function insertImplementerLokasiHaiwan(jenis, hargaekor, hargabhg) {
  let result = null;

  try {
    let datetime = getDateTime();

    result = await knex.connect(`implementerLokasiHaiwan`).insert({
      implementerLokasiHaiwanJenis: jenis,
      implementerLokasiHaiwanHargaEkor: hargaekor,
      implementerLokasiHaiwanHargaBhgn: hargabhg,
      implementerLokasiHaiwanStatus: "ACTIVE",
      implementerLokasiHaiwanStatusCode: 1,
    });

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getImplementerLokasiHaiwan(implementerId, tahunId) {
  let result = null;
  let name = null;

  // console.log("APA INI", implementerId, tahunId);

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex
    .connect(`implementerLokasiHaiwan`)
    .where(`implementerId`, implementerId)
    .andWhere(`setupKorbanId`, tahunId);

  name = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);
    
  console.log(result);

  return [result, name];
}

async function updateUser(username, fullname) {
  let result = null;

  try {
    result = await knex
      .connect(`user`)
      .update({
        userFullname: fullname,
      })
      .where(`userUsername`, username);

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function deleteUser(username) {
  let result = null;

  try {
    result = await knex.connect(`user`).where({ userUsername: username }).del();

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

module.exports = {
  insertImplementerLokasiHaiwan,
  getImplementerLokasiHaiwan,
  updateUser,
  deleteUser,
};
