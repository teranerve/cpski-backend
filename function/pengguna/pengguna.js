const sha = require("sha256");
const knex = require("../../connection.js");
const moment = require("moment");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function getCurrentSetupKorbanId() {
  let year = moment().format("YYYY");

  let setupKorban = await knex
    .connect("setupKorban")
    .where("setupKorbanTahun", year)
    .first();

  if (!setupKorban) return false;

  return setupKorban.setupKorbanId;
}

async function insertUser(
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql = await knex.connect(`user`).insert({
      userFullname: userFullname,
      userUsername: userUsername,
      userEmail: userEmail,
      userPassword: sha(userPassword),
      userPhoneNo: userPhoneNo,
      userIc: userIc,
      userType: userType,
      userTypeCode: "30",
      userAlamat: userAlamat,
      userAlamatPostCode: userAlamatPostCode,
      userAlamatBandar: userAlamatBandar,
      userAlamatNegeri: userAlamatNegeri,
      userAlamatNegara: userAlamatNegara,
      userBankAccNo: userBankAccNo,
      userBankHolderName: userBankHolderName,
      userBank: userBank,
      userRegistrationDate: datetime,
      userStatus: "Active",
      userStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertUserWakil(insertUser) {
  let result = null;

  try {

    let sql = await knex.connect(`userWakil`).insert({
      userId: insertUser,
      userWakilStatus: "Active",
      userWakilStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertUserAdmin(
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql = await knex.connect(`user`).insert({
      userFullname: userFullname,
      userUsername: userUsername,
      userEmail: userEmail,
      userPassword: sha(userPassword),
      userPhoneNo: userPhoneNo,
      userIc: userIc,
      userType: userType,
      userTypeCode: "26",
      userAlamat: userAlamat,
      userAlamatPostCode: userAlamatPostCode,
      userAlamatBandar: userAlamatBandar,
      userAlamatNegeri: userAlamatNegeri,
      userAlamatNegara: userAlamatNegara,
      userBankAccNo: userBankAccNo,
      userBankHolderName: userBankHolderName,
      userBank: userBank,
      userRegistrationDate: datetime,
      userStatus: "Active",
      userStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertUserStaff(
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql = await knex.connect(`user`).insert({
      userFullname: userFullname,
      userUsername: userUsername,
      userEmail: userEmail,
      userPassword: sha(userPassword),
      userPhoneNo: userPhoneNo,
      userIc: userIc,
      userType: userType,
      userTypeCode: "27",
      userAlamat: userAlamat,
      userAlamatPostCode: userAlamatPostCode,
      userAlamatBandar: userAlamatBandar,
      userAlamatNegeri: userAlamatNegeri,
      userAlamatNegara: userAlamatNegara,
      userBankAccNo: userBankAccNo,
      userBankHolderName: userBankHolderName,
      userBank: userBank,
      userRegistrationDate: datetime,
      userStatus: "Active",
      userStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertUserEjen(
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql = await knex.connect(`user`).insert({
      userFullname: userFullname,
      userUsername: userUsername,
      userEmail: userEmail,
      userPassword: sha(userPassword),
      userPhoneNo: userPhoneNo,
      userIc: userIc,
      userType: userType,
      userTypeCode: "29",
      userAlamat: userAlamat,
      userAlamatPostCode: userAlamatPostCode,
      userAlamatBandar: userAlamatBandar,
      userAlamatNegeri: userAlamatNegeri,
      userAlamatNegara: userAlamatNegara,
      userBankAccNo: userBankAccNo,
      userBankHolderName: userBankHolderName,
      userBank: userBank,
      userRegistrationDate: datetime,
      userStatus: "Active",
      userStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertUserEjen2(
  insertUserEjen,
  userEjenPermalink,
  userEjenCategory,
  userEjenCommissionCategory,
  userEjenCommissionType,
  userEjenCommissionValue,
  userEjenIdReferal,
  setupKorbanId
) {
  let result = null;
  console.log("Lalu pengguna.js");
  console.log(
    insertUserEjen,
    userEjenPermalink,
    userEjenCategory,
    userEjenCommissionCategory,
    userEjenCommissionType,
    userEjenCommissionValue,
    setupKorbanId
  );
  try {
    let sql = await knex.connect(`userEjen`).insert({
      userId: insertUserEjen,
      userEjenPermalink: userEjenPermalink,
      userEjenIdReferal: userEjenIdReferal,
      userEjenCategory: userEjenCategory,
      userEjenCommissionCategory: userEjenCommissionCategory,
      userEjenCommissionType: userEjenCommissionType,
      userEjenCommissionValue: userEjenCommissionValue,
      userEjenStatus: "Active",
      userEjenStatusCode: "20",
      setupKorbanId,
    });

    console.log("insertUserEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkUsernameEmail(username, email, phoneno) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userUsername`, username)
      .orWhere(`userEmail`, email)
      .orWhere("userPhoneNo", phoneno);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkUsername(userId, userUsername) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userUsername`, userUsername)
      .where("userId", "!=", userId);

    console.log("getUser: ", userId);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }

    // if(sql[0]['userUsername'] == userUsername ){
    //   if (sql[0]['userEmail'] == userEmail){
    //     if(sql[0]['userPhoneNo'] == userPhoneNo){
    //     result = false;
    //     }
    //   }

    // }else{
    //   if (sql[0]['userEmail'] == userEmail){
    //     if (sql[0]['userPhoneNo'] == userPhoneNo){

    //     }

    //   }
    // }

    // if(sql[0]['userUsername'] == userUsername && sql[0]['userEmail'] == userEmail && sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = false;
    // }else if(sql[0]['userUsername'] == userUsername || sql[0]['userEmail'] == userEmail || sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = true;
    // }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkEmail(userId, userEmail) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userEmail`, userEmail)
      .where("userId", "!=", userId);

    console.log("getUser: ", userId);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }

    // if(sql[0]['userUsername'] == userUsername ){
    //   if (sql[0]['userEmail'] == userEmail){
    //     if(sql[0]['userPhoneNo'] == userPhoneNo){
    //     result = false;
    //     }
    //   }

    // }else{
    //   if (sql[0]['userEmail'] == userEmail){
    //     if (sql[0]['userPhoneNo'] == userPhoneNo){

    //     }

    //   }
    // }

    // if(sql[0]['userUsername'] == userUsername && sql[0]['userEmail'] == userEmail && sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = false;
    // }else if(sql[0]['userUsername'] == userUsername || sql[0]['userEmail'] == userEmail || sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = true;
    // }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPhone(userId, userPhoneNo) {
  let result = null;

  try {
    let sql = await knex
      .connect(`user`)
      .where(`userPhoneNo`, userPhoneNo)
      .where(`userId`, "!=", userId);

    console.log("getUser: ", userId);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }

    // if(sql[0]['userUsername'] == userUsername ){
    //   if (sql[0]['userEmail'] == userEmail){
    //     if(sql[0]['userPhoneNo'] == userPhoneNo){
    //     result = false;
    //     }
    //   }

    // }else{
    //   if (sql[0]['userEmail'] == userEmail){
    //     if (sql[0]['userPhoneNo'] == userPhoneNo){

    //     }

    //   }
    // }

    // if(sql[0]['userUsername'] == userUsername && sql[0]['userEmail'] == userEmail && sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = false;
    // }else if(sql[0]['userUsername'] == userUsername || sql[0]['userEmail'] == userEmail || sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = true;
    // }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUser(
  userId,
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {

    let sql2 = await knex
      .connect(`user`)
      .select(`userId`, `userPassword`)
      .where(`userId`, userId);

    if (sql2[0].userPassword == userPassword) {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: userPassword,
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "30",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("updateUser: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    } else {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: sha(userPassword),
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "30",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("updateUser: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUserWakil(userId) {
  let result = null;

  try {

    let sql = await knex
      .connect(`userWakil`)
      .update({
        userId: userId,
        userWakilStatus: "Active",
        userWakilStatusCode: "20",
        setupKorbanId,
      })
      .where(`userId`, userId);

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUserAdmin(
  userId,
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql2 = await knex
      .connect(`user`)
      .select(`userId`, `userPassword`)
      .where(`userId`, userId);

    if (sql2[0].userPassword == userPassword) {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: userPassword,
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "26",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userRegistrationDate: datetime,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("insertEjen: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    } else {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: sha(userPassword),
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "26",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userRegistrationDate: datetime,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("insertEjen: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUserStaff(
  userId,
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql2 = await knex
      .connect(`user`)
      .select(`userId`, `userPassword`)
      .where(`userId`, userId);

    if (sql2[0].userPassword == userPassword) {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: userPassword,
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "27",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userRegistrationDate: datetime,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("insertEjen: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    } else {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: sha(userPassword),
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "27",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userRegistrationDate: datetime,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("insertEjen: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUserEjen(
  userId,
  userFullname,
  userUsername,
  userEmail,
  userPassword,
  userPhoneNo,
  userIc,
  userType,
  userAlamat,
  userAlamatPostCode,
  userAlamatBandar,
  userAlamatNegeri,
  userAlamatNegara,
  userBankAccNo,
  userBankHolderName,
  userBank,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql2 = await knex
      .connect(`user`)
      .select(`userId`, `userPassword`)
      .where(`userId`, userId);

    if (sql2[0].userPassword == userPassword) {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: userPassword,
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "29",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userRegistrationDate: datetime,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("insertEjen: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    } else {
      let sql = await knex
        .connect(`user`)
        .update({
          userId: userId,
          userFullname: userFullname,
          userUsername: userUsername,
          userEmail: userEmail,
          userPassword: sha(userPassword),
          userPhoneNo: userPhoneNo,
          userIc: userIc,
          userType: userType,
          userTypeCode: "29",
          userAlamat: userAlamat,
          userAlamatPostCode: userAlamatPostCode,
          userAlamatBandar: userAlamatBandar,
          userAlamatNegeri: userAlamatNegeri,
          userAlamatNegara: userAlamatNegara,
          userBankAccNo: userBankAccNo,
          userBankHolderName: userBankHolderName,
          userBank: userBank,
          userRegistrationDate: datetime,
          userStatus: "Active",
          userStatusCode: "20",
          setupKorbanId,
        })
        .where(`userId`, userId);

      console.log("insertEjen: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateUserEjen2(
  userId,
  userEjenPermalink,
  userEjenCategory,
  userEjenCommissionCategory,
  userEjenCommissionType,
  userEjenCommissionValue,
  userEjenIdReferal,
  setupKorbanId
) {
  let result = null;

  try {
    let sql = await knex
      .connect(`userEjen`)
      .update({
        userId: userId,
        userEjenPermalink: userEjenPermalink,
        userEjenIdReferal: userEjenIdReferal,
        userEjenCategory: userEjenCategory,
        userEjenCommissionCategory: userEjenCommissionCategory,
        userEjenCommissionType: userEjenCommissionType,
        userEjenCommissionValue: userEjenCommissionValue,
        userEjenStatus: "Active",
        userEjenStatusCode: "20",
        setupKorbanId,
      })
      .where(`userId`, userId);

    console.log("insertUserEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getUser(year) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex
    .connect(`user`)
    .select(`*`, `userRegistrationDate as masa`)
    .where(`setupKorbanId`, year);
  // .where(`userStatusCode`, userStatusCode)

  count = await knex.connect(`user`).count().where(`setupKorbanId`, year);
  // .where(`userStatusCode`, userStatusCode)

  for (let i = 0; i < result.length; i++) {
    result[i].userRegistrationDate = moment(
      result[i].userRegistrationDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }

  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getPenggunaSortByDate(year) {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex
    .connect(`user`)
    .select(`*`, `userRegistrationDate as masa`)
    .orderBy(`userRegistrationDate`, `desc`)
    .where(`setupKorbanId`, year);

  count = await knex.connect(`user`).count().where(`setupKorbanId`, year);

  for (let i = 0; i < result.length; i++) {
    result[i].userRegistrationDate = moment(
      result[i].userRegistrationDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }
  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getPenggunaSortByDateAsc(year) {
  let result = null;
  let count = null;

  result = await knex
    .connect(`user`)
    .select(`*`, `userRegistrationDate as masa`)
    .orderBy(`userRegistrationDate`, `asc`)
    .where(`setupKorbanId`, year);

  count = await knex.connect(`user`).count().where(`setupKorbanId`, year);

  for (let i = 0; i < result.length; i++) {
    result[i].userRegistrationDate = moment(
      result[i].userRegistrationDate
    ).format("YYYY-MM-DD HH:mm:ss");
  }
  // console.log(count[0]["count(*)"]);

  return [result, count[0]["count(*)"]];
}

async function getUserButiran(userId) {
  let result = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex
    .connect(`user AS u`)
    .leftJoin("userEjen AS ue", "ue.userId", "u.userId")
    .leftJoin("userWakil AS uw", "uw.userId", "u.userId")
    .where("u.userId", userId)
  // .where(`userStatusCode`, userStatusCode)

  // console.log(count[0]["count(*)"]);

  return result;
}

let deletePengguna = async (userId) => {
  console.log("ID", userId);

  return await knex
    .connect("user")
    .update({
      userStatus: "Delete",
    })
    .where("userId", userId);
};

module.exports = {
  deletePengguna,
  insertUser,
  insertUserWakil,
  insertUserAdmin,
  insertUserStaff,
  insertUserEjen,
  insertUserEjen2,
  getUser,
  getPenggunaSortByDate,
  getPenggunaSortByDateAsc,
  getUserButiran,
  updateUser,
  updateUserWakil,
  updateUserAdmin,
  updateUserStaff,
  updateUserEjen,
  updateUserEjen2,
  checkUsernameEmail,
  checkUsername,
  checkEmail,
  checkPhone,
  getCurrentSetupKorbanId,
};
