const express = require("express");
const router = express.Router();
const {
  getKomisyenEjen,
} = require("../../function/configuration/configuration.js");

router.get("/", async (req, res) => {
  try {
    const result = await getKomisyenEjen();

    if (result.komisyenejen.length > 0)
      return res.status(200).json({
        status: 200,
        message: "Maklumat komisyen ejen dijumpai!",
        data: result,
      });

    res.status(404).json({
      status: 404,
      message: "Maklumat komisyen ejen tidak dijumpai!",
      data: "",
    });
  } catch (error) {
    console.error(error); // LOG ERROR

    return res.status(500).json({
      status: 500,
      message: "Masalah Ralat! Sila Hubungi Admin.",
      data: "",
    });
  }
});

module.exports = router;
