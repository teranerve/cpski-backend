const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/ejen/ejen.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let userFullname = null;
  let userUsername = null;
  let userEmail = null;
  let userPassword = null;
  let userPhoneNo = null;
  let userIc = null;
  let userEjenPermalink = null;
  let userAlamat = null;
  let userAlamatPostCode = null;
  let userAlamatBandar = null;
  let userAlamatNegeri = null;
  let userAlamatNegara = null;
  let userBankAccNo = null;
  let userBankHolderName = null;
  let userBank = null;
  let userEjenCategory = null;
  let userEjenCommissionCategory = null;
  let userEjenCommissionType = null;
  let userEjenCommissionValue = null;
  let userEjenIdReferal = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;

    userFullname = param.userFullname;
    userUsername = param.userUsername;
    userEmail = param.userEmail;
    userPassword = param.userPassword;
    userPhoneNo = param.userPhoneNo;
    userIc = param.userIc;
    userAlamat = param.userAlamat;
    userAlamatPostCode = param.userAlamatPostCode;
    userAlamatBandar = param.userAlamatBandar;
    userAlamatNegeri = param.userAlamatNegeri;
    userAlamatNegara = param.userAlamatNegara;
    userBankAccNo = param.userBankAccNo;
    userBankHolderName = param.userBankHolderName;
    userBank = param.userBank;
    userEjenPermalink = param.userEjenPermalink;
    userEjenCategory = param.userEjenCategory;
    userEjenCommissionCategory = param.userEjenCommissionCategory;
    userEjenCommissionType = param.userEjenCommissionType;
    userEjenCommissionValue = param.userEjenCommissionValue;
    userEjenIdReferal = param.userEjenIdReferal;

    //VALIDATE UNIQUE USERNAME, EMAIL, PHONE, NRIC

    let checkUsername = await model.checkUsername(userUsername);
    let checkEmail = await model.checkEmail(userEmail);
    let checkPhoneNo = await model.checkPhoneNo(userPhoneNo);
    let checkIc = await model.checkIc(userIc);
    let checkPermalink = await model.checkPermalink(userEjenPermalink);
    let setupKorbanId = await model.getCurrentSetupKorbanId();

    if (!setupKorbanId) throw "SetupKorbanId Not Found!";

    if (checkEmail == false) {
      result = {
        status: "gagal",
        jenis: "email",

        message: "Alamat email telah wujud",
      };
    } else if (checkPhoneNo == false) {
      result = {
        status: "gagal",
        jenis: "phone",

        message: "No telefon telah wujud",
      };
    } else if (checkIc == false) {
      result = {
        status: "gagal",
        jenis: "ic",

        message: "Nombor kad pengenalan telah wujud",
      };
    } else if (checkUsername == false) {
      result = {
        status: "gagal",
        jenis: "username",
        message: "Nama pengguna telah wujud",
      };
    } else if (checkPermalink == false) {
      result = {
        status: "gagal",
        jenis: "permalink",
        message: "Permalink telah wujud",
      };
    } else {
      // INSERT USER EJEN

      let insertUser = await model.insertUser(
        userFullname,
        userUsername,
        userEmail,
        userPassword,
        userPhoneNo,
        userIc,
        userAlamat,
        userAlamatPostCode,
        userAlamatBandar,
        userAlamatNegeri,
        userAlamatNegara,
        userBankAccNo,
        userBankHolderName,
        userBank,
        setupKorbanId
      );
      //INSERT USER EJEN DETAIL
      let insertEjen = await model.insertUserEjen(
        insertUser,
        userEjenPermalink,
        userEjenCategory,
        userEjenCommissionCategory,
        userEjenCommissionType,
        userEjenCommissionValue,
        userEjenIdReferal,
        setupKorbanId
      );

      if (insertUser != false || insertEjen != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya daftar Ejen.",
        };
      } else {
        result = {
          status: "gagal",
          message: "Anda gagal daftar Ejen",
        };
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
