const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/haiwanKorban/haiwanKorban.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        year = param.Id;

        let getHaiwanKorban = await model.getHaiwanKorban(year);

        if (getHaiwanKorban[0] != false && getHaiwanKorban[1] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai jumlah haiwan korban.",
                jumlahHaiwanKorban: getHaiwanKorban[0],
                jumlahBaki: getHaiwanKorban[1],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai jumlah haiwan korban.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});


module.exports = router;