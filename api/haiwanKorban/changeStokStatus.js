const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/haiwanKorban/changeStokStatus.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.post("/", async (req, res) => {
  try {
    const param = model.check_param(req.body);
    if (!param.status)
      return res.status(400).json({ status: "gagal", message: param.message });

    const result = await model.make_stok_inactive(req.body);
    if (!result)
      return res.status(400).json({
        status: "gagal",
        message: "Senarai stok tidak sah. Proses terhenti.",
      });

    return res.status(200).json({
      status: "berjaya",
      message: "Berjaya menukar status stok",
      data: result,
    });
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }
});

module.exports = router;
