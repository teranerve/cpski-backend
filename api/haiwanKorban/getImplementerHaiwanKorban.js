const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/haiwanKorban/haiwanKorban.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
    let param = null;
    let result = null;
    let jenisHaiwan = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        jenisHaiwan = param.jenisHaiwan;
        year = param.Id;

        let getImplementerHaiwanKorban = await model.getImplementerHaiwanKorban(jenisHaiwan, year);

        if (getImplementerHaiwanKorban[0] != false || getImplementerHaiwanKorban[1] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai jumlah haiwan korban oleh implementer.",
                jumlahHaiwanKorban: getImplementerHaiwanKorban[0],
                jumlahBaki: getImplementerHaiwanKorban[1],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai jumlah haiwan korban oleh implementer.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});


module.exports = router;