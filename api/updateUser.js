const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../function/user.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.put("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let username = null;
  let fullname = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    username = param.username;
    fullname = param.fullname;

    // GET USER FUNCTION
    const updateUser = await model.updateUser(username,fullname);

    if (updateUser != false) {
      result = {
        message: "anda berjaya kemaskini pengguna",
        environment: process.env.ENVIRONMENT,
        userdata: updateUser,
        test: updateUser,
      };
    } else {
      result = {
          message: 'User Update fail'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
