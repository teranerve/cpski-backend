const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/pengguna/pengguna.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;

    let userStatusCode = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        userId = param.userId;
        // userStatusCode = param.userStatusCode;
    
        // GET USER FUNCTION
        let getUserButiran = await model.getUserButiran(userId);
    
        if (getUserButiran != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai Pengguna Butiran",
            environment: process.env.ENVIRONMENT,
            userdata: getUserButiran,
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan Pengguna Butiran'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;