const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/pengguna/pengguna.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let userFullname = null;
  let userUsername = null;
  let userEmail = null;
  let userPassword = null;
  let userPhoneNo = null;
  let userIc = null;
  let userType = null;
  let userAlamat = null;
  let userAlamatPostCode = null;
  let userAlamatBandar = null;
  let userAlamatNegeri = null;
  let userAlamatNegara = null;
  let userBankAccNo = null;
  let userBankHolderName = null;
  let userBank = null;
  let userEjenPermalink = null;
  let userEjenCategory = null;
  let userEjenCommissionCategory = null;
  let userEjenCommissionType = null;
  let userEjenCommissionValue = null;
  let userEjenIdReferal = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;
    // username = param.username;
    // password = param.password;
    // fullname = param.fullname;
    // type = param.type;

    userFullname = param.userFullname;
    userUsername = param.userUsername;
    userEmail = param.userEmail;
    userPassword = param.userPassword;
    userPhoneNo = param.userPhoneNo;
    userIc = param.userIc;
    userType = param.userType;
    userAlamat = param.userAlamat;
    userAlamatPostCode = param.userAlamatPostCode;
    userAlamatBandar = param.userAlamatBandar;
    userAlamatNegeri = param.userAlamatNegeri;
    userAlamatNegara = param.userAlamatNegara;
    userBankAccNo = param.userBankAccNo;
    userBankHolderName = param.userBankHolderName;
    userBank = param.userBank;
    userEjenPermalink = param.userEjenPermalink;
    userEjenCategory = param.userEjenCategory;
    userEjenCommissionCategory = param.userEjenCommissionCategory;
    userEjenCommissionType = param.userEjenCommissionType;
    userEjenCommissionValue = param.userEjenCommissionValue;
    userEjenIdReferal = param.userEjenIdReferal;

    //VALIDATE UNIQUE USERNAME AND EMAIL

    let checkUsernameEmail = await model.checkUsernameEmail(
      userUsername,
      userEmail,
      userPhoneNo
    );

    let setupKorbanId = await model.getCurrentSetupKorbanId();

    if (!setupKorbanId) throw "SetupKorbanId Not Found!";

    if (checkUsernameEmail == false) {
      result = {
        status: "gagal",
        message: "Username, Email atau Nombor Telefon telah wujud.",
      };
    } else {
      if (userType == "Wakil") {
        let insertUser = await model.insertUser(
          userFullname,
          userUsername,
          userEmail,
          userPassword,
          userPhoneNo,
          userIc,
          userType,
          userAlamat,
          userAlamatPostCode,
          userAlamatBandar,
          userAlamatNegeri,
          userAlamatNegara,
          userBankAccNo,
          userBankHolderName,
          userBank,
          setupKorbanId
        );
        //INSERT USER WAKIL DETAIL
        let insertUserWakil = await model.insertUserWakil(insertUser);

        if (insertUser != false || insertUserWakil != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya daftar Wakil.",
          };
        } else {
          result = {
            status: "gagal",
            message: "Anda gagal daftar Wakil",
          };
        }
      }
      if (userType == "Admin") {
        let insertUserAdmin = await model.insertUserAdmin(
          userFullname,
          userUsername,
          userEmail,
          userPassword,
          userPhoneNo,
          userIc,
          userType,
          userAlamat,
          userAlamatPostCode,
          userAlamatBandar,
          userAlamatNegeri,
          userAlamatNegara,
          userBankAccNo,
          userBankHolderName,
          userBank,
          setupKorbanId
        );
        //INSERT USER EJEN DETAIL

        if (insertUserAdmin != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya daftar Admin.",
          };
        } else {
          result = {
            status: "gagal",
            message: "Anda gagal daftar Admin",
          };
        }
      }
      if (userType == "Staff") {
        let insertUserStaff = await model.insertUserStaff(
          userFullname,
          userUsername,
          userEmail,
          userPassword,
          userPhoneNo,
          userIc,
          userType,
          userAlamat,
          userAlamatPostCode,
          userAlamatBandar,
          userAlamatNegeri,
          userAlamatNegara,
          userBankAccNo,
          userBankHolderName,
          userBank,
          setupKorbanId
        );
        //INSERT USER STAFF DETAIL

        if (insertUserStaff != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya daftar Staff.",
          };
        } else {
          result = {
            status: "gagal",
            message: "Anda gagal daftar Staff  ",
          };
        }
      }
      if (userType == "Ejen") {
        let insertUserEjen = await model.insertUserEjen(
          userFullname,
          userUsername,
          userEmail,
          userPassword,
          userPhoneNo,
          userIc,
          userType,
          userAlamat,
          userAlamatPostCode,
          userAlamatBandar,
          userAlamatNegeri,
          userAlamatNegara,
          userBankAccNo,
          userBankHolderName,
          userBank,
          setupKorbanId
        );
        //INSERT USER EJEN DETAIL
        let insertUserEjen2 = await model.insertUserEjen2(
          insertUserEjen,
          userEjenPermalink,
          userEjenCategory,
          userEjenCommissionCategory,
          userEjenCommissionType,
          userEjenCommissionValue,
          userEjenIdReferal,
          setupKorbanId
        );

        if (insertUserEjen != false || insertUserEjen2 != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya daftar Ejen.",
          };
        } else {
          result = {
            status: "gagal",
            message: "Anda gagal daftar Ejen",
          };
        }
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
