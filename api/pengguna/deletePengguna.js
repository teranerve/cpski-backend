const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/pengguna/pengguna.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.post("/", async (req, res) => {
  try {
    let param = req.body;
    let userId = param.userId;

    let process = await model.deletePengguna(userId);
    console.log("ID", userId);

    if (process) {
        // true / 1 / " " / 
      return res
        .status(200)
        .json({ status: 200, message: "Pengguna Berjaya Dibuang!", data: "" });
    }

    return res
      .status(500)
      .json({
        status: 500,
        message: "Masalah Ralat! Sila Hubungi Admin.",
        data: "",
      });
  } catch (err) {
    console.error(err);

    return res
      .status(500)
      .json({
        status: 500,
        message: "Masalah Ralat! Sila Hubungi Admin.",
        data: "",
      });
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;