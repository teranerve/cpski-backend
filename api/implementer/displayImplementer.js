const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/implementer/implementer.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");
// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// GET USER
router.get("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let implementerStatusCode = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    // implementerStatusCode = param.implementerStatusCode;

    // GET USER FUNCTION
    const displayImplementer = await model.displayImplementer();

    if (displayImplementer[0] != false && displayImplementer[1] != false) {
      result = {
        status: "berjaya",
        message: "anda berjaya dapatkan senarai Implementer",
        environment: process.env.ENVIRONMENT,
        userdata: displayImplementer[0],
        count: displayImplementer[1],
      };
    } else {
      result = {
          status: "gagal",
          message: 'User Type does not exist'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
