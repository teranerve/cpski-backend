const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let result = null;
  let param = null;
  let userId = null;


  try {
    param = req.query
    userId = param.userId
    let getOverviewEjen = await model.getOverviewEjen(userId);

    if (getOverviewEjen[0] != false && getOverviewEjen[1] != false && getOverviewEjen[2] != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan data Overview",
        environment: process.env.ENVIRONMENT,
        overviewData1: getOverviewEjen[0][0],
        overviewData2: getOverviewEjen[1][0],
        overviewData3: getOverviewEjen[2][0]
      };
    } else {
      result = {
        status: "gagal",
        message: 'Anda gagal dapatkan data Overview'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;