const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let result = null;
    let param = null;
    let jenisHaiwan = null;
    let year = null;


    try {
        param = req.query;
        jenisHaiwan = param.Haiwan;
        year = param.Id;

        // console.log("HAHAHA", year)
        let getDonutChartAdmin = await model.getDonutChartAdmin(jenisHaiwan, year);

        if (getDonutChartAdmin != null) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan data Donut Chart",
                environment: process.env.ENVIRONMENT,
                donutChartData: getDonutChartAdmin,
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan data Donut Chart'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;