const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let result = null;
    let param = null;
  let userId = null;


    try {
        param = req.query
    userId = param.userId

        let getLatestTempahanEjen = await model.getLatestTempahanEjen(userId);

        if (getLatestTempahanEjen != null) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan data Tempahan Terkini",
                environment: process.env.ENVIRONMENT,
                latestTempahanData: getLatestTempahanEjen,
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan data Tempahan Terkini'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;