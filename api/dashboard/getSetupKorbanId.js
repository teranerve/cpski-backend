const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let result = null;
  let param = req.query;
  let year = param.Tahun;

  try {
    let getSetupKorbanId = await model.getSetupKorbanId(year);

    if (getSetupKorbanId != 0) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan ID setup tahun korban",
        environment: process.env.ENVIRONMENT,
        data: getSetupKorbanId[0].setupKorbanId,
      };
    } else {
      result = {
        status: "gagal",
        message: 'Anda gagal dapatkan ID setup tahun korban'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;