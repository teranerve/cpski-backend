const express = require("express");
const router = express.Router();
const model = require("../../function/sijil/sijil");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null, tempahanNo = null, tempahanId = null, noHaiwan = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    tempahanNo = param.tempahanNo;
    tempahanId = param.tempahanId;
    implementerId = param.implementerId;
    noHaiwan = param.noHaiwan;

      let getPesertaInOneAnimal = await model.getPesertaInOneAnimal(tempahanNo, tempahanId, implementerId, noHaiwan);

      if (getPesertaInOneAnimal != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan butiran getPesertaInOneAnimal",
          data: getPesertaInOneAnimal,
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan butiran getPesertaInOneAnimal'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;