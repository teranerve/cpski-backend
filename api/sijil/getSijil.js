const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/sijil/sijil"); // INCLUDE FUNCTION FILE

router.get("/getImplementerSijil", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    implementerId = param.implementerId;

      let getImplementerCiptaSijil = await model.getImplementerCiptaSijil(implementerId);

      if (getImplementerCiptaSijil[0] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan butiran implementer",
          data: getImplementerCiptaSijil[0],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan butiran implementer'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;