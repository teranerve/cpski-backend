const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/implementerLokasiHaiwan/implementerLokasiHaiwan.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let jenis = null;
  let hargaekor = null;
  let hargabhg = null;
  let year = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.body;

    jenis = param.jenis;
    hargaekor = param.hargaekor;
    hargabhg = param.hargabhg;
    // year = param.year;

    // console.log("POST YEAR", year)

    // GET USER FUNCTION
    const insertImplementerLokasiHaiwan = await model.insertImplementerLokasiHaiwan(jenis, hargaekor,hargabhg);

    if (insertImplementerLokasiHaiwan != false) {
      result = {
        message: "anda berjaya masukkan Implementer Lokasi Haiwan",
        environment: process.env.ENVIRONMENT,
        userdata: insertImplementerLokasiHaiwan,
        test: insertImplementerLokasiHaiwan,
      };
    } else {
      result = {
          message: 'User Insert fail'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
