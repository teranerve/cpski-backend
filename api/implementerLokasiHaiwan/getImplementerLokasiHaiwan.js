const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/implementerLokasiHaiwan/implementerLokasiHaiwan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;

  let implementerId = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;
    implementerId = param.implementerId;
    tahunId = param.TahunId;

    // console.log("TAHUN INI", tahunId)

    // GET USER FUNCTION
    const getImplementerLokasiHaiwan = await model.getImplementerLokasiHaiwan(
      implementerId,
      tahunId
    );

    if (
      getImplementerLokasiHaiwan[0] != false &&
      getImplementerLokasiHaiwan[1] != false
    ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai haiwan.",
        data: getImplementerLokasiHaiwan[0],
        name: getImplementerLokasiHaiwan[1],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai haiwan",
        name: getImplementerLokasiHaiwan[1] || [],
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
