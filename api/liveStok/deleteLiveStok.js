const express = require("express"); // MUST HAVE
const { insertImplementer } = require("../../function/implementer/implementer.js");
const router = express.Router(); // MUST HAVE
const moment = require("moment");
const model = require("../../function/liveStok/liveStok.js"); // INCLUDE FUNCTION FILE
const conf = require("../../function/configuration/configuration.js");
// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
    let param = null;
    let result = null;

    // VARIABLE SETUP
    let liveStokId = null;
    let  liveStokStatus  =  null;
    let selectedData =  null;
    

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.body;
        console.log(param.length);
        selectedData = param;
        for(let i=0; i<selectedData.length; i++ ){
            console.log(selectedData[i].liveStokId)
            console.log('--- deleteLiveStok PROCESS START ---')
        
            let deleteLiveStok = await model.deleteLiveStok(
            selectedData[i].liveStokId,
            "Deleted")



            if (deleteLiveStok != false) {
                result = {
                    status: "berjaya",
                    message: "Anda berjaya kemaskini LiveStoks.",
                    data:  deleteLiveStok
                };
            } else {
                result = {
                    status: "gagal",
                    message: 'Anda gagal kemaskini LiveStok',
                    
                }
            }

        }

        
        

    
    
} catch (error) {
    console.log(error); // LOG ERROR
    result = {
        message: `API Error`,
    };
}

// RETURN
res.status(200).json(result);
});

module.exports = router;