const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/liveStok/liveStok.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    let liveStokId = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        liveStokId = param.liveStokId;
    
        // GET USER FUNCTION
        let getLiveStokBahagian = await model.getLiveStokBahagian(liveStokId);
    
        if (getLiveStokBahagian != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai bahagian haiwan.",
            data: getLiveStokBahagian,
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan senarai bahagian haiwan.'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;