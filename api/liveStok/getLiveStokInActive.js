const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/liveStok/liveStok.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    let implementerLokasiHaiwanId = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerLokasiHaiwanId = param.implementerLokasiHaiwanId;
    
        // GET USER FUNCTION
        const getLiveStokInActive = await model.getLiveStokInActive(implementerLokasiHaiwanId);
    
        if (getLiveStokInActive[0] != false || getLiveStokInActive[1] != false || getLiveStokInActive[2] != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai stok haiwan.",
            data: getLiveStokInActive[0],
            implementerLokasiHaiwan: getLiveStokInActive[1],
            implementer: getLiveStokInActive[2],
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan senarai stok haiwan.'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;