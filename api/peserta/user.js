const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/peserta/peserta"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// GET USER
router.get("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let userFullname = null;
  let userPhoneNo = null;
  let userEmail = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    console.log("param: ", param);
    userFullname = param.userFullname;
    userPhoneNo = param.userPhoneNo;
    userEmail = param.userEmail;
     // LOG TO CHECK 

    // GET USER FUNCTION
    const getUserWakilId = await model.getUserWakilId(userFullname,userPhoneNo,userEmail);

    if (!getUserWakilId) return;

    result = {
      message: "anda berjaya dapatkan senarai pengguna",
      environment: process.env.ENVIRONMENT,
      userdata: getUserWakilId,
      test: getUserWakilId,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

// INSERT USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  let username = null;
  let fullname = null;

  let status = null;
  let message = null;
  let data = null;

  try {
    param = req.body;
    username = param.username ? param.username : "";
    fullname = param.fullname ? param.fullname : "";

    console.log("param: ", param);

    if (username == null || username == "") {
      status = "unsuccess";
      message = "Username cannot be empty";
    } else if (fullname == null || fullname == "") {
      status = "unsuccess";
      message = "Fullname cannot be empty";
    } else {
      const insertUser = await model.insertUser(username, fullname);

      if (!insertUser) return;

      status = "success";
      message = "User success insert";
      data = insertUser;
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  result = {
    environment: process.env.ENVIRONMENT,
    status: status,
    message: message,
    data: data
  };

  res.status(200).json(result);
});

// UPDATE USER
router.put("/", async (req, res) => {
  let param = null;
  let result = null;

  let username = null;
  let fullname = null;
  try {
    param = req.body;
    username = param.username ? param.username : "";
    fullname = param.fullname ? param.fullname : "";

    console.log("param: ", param);

    const updateUser = await model.updateUser(username, fullname);

    if (!updateUser) return;

    result = {
      message: "anda berjaya kemaskini pengguna",
      environment: process.env.ENVIRONMENT,
      test: updateUser,
    };
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  res.status(200).json(result);
});

// NOT RECOMMEND DELETING USER
router.delete("/", async (req, res) => {
  let param = null;
  let result = null;

  let username = null;
  try {
    param = req.query;
    username = param.username;

    console.log("param: ", param);

    const deleteUser = await model.deleteUser(username);

    if (!deleteUser) return;

    result = {
      message: "anda berjaya buang pengguna",
      environment: process.env.ENVIRONMENT,
      userdata: deleteUser,
      test: deleteUser,
    };
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  res.status(200).json(result);
});

module.exports = router;
