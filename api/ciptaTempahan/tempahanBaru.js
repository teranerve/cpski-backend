const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/ciptaTempahan/tempahanBaru"); // INCLUDE FUNCTION FILE
const moment = require("moment");
const CryptoJS = require("crypto-js");

router.get("/senaraiKawasan", async (req, res) => {
  // VARIABLE SETUP
  let result = null;

  try {
    // GET TEMPAHAN NUMBER FUNCTION
    const getSenaraiNegara = await model.getSenaraiNegara();

    if (!getSenaraiNegara) return;

    result = {
      message: "Get Kawasan Korban",
      status: "Success",
      environment: process.env.ENVIRONMENT,
      data: getSenaraiNegara,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

router.get("/senaraiHaiwan", async (req, res) => {
  // VARIABLE SETUP
  let result = null;

  try {
    // GET TEMPAHAN NUMBER FUNCTION
    const getSenaraiHaiwan = await model.getSenaraiHaiwan();

    if (!getSenaraiHaiwan) return;

    result = {
      message: "Get Senarai Haiwan",
      status: "Success",
      environment: process.env.ENVIRONMENT,
      data: getSenaraiHaiwan,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// GET TEMPAHAN NUMBER
router.get("/nomborTempahan", async (req, res) => {
  // VARIABLE SETUP
  let result = null;

  try {
    // GET TEMPAHAN NUMBER FUNCTION
    const generateTempahanNo = await model.generateTempahanNo();

    if (!generateTempahanNo) return;

    result = {
      message: "Get Tempahan's Number",
      environment: process.env.ENVIRONMENT,
      data: generateTempahanNo,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// GET ORGANIZATION'S ID
router.get("/organizationId", async (req, res) => {
  // VARIABLE SETUP
  let param = null;
  let result = null;

  let organizationDomain = null;
  let organizationStatus = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;

    console.log("param: ", param);

    organizationDomain = param.organizationDomain;
    organizationStatus = param.organizationStatus;

    // GET ORGANIZATION'S ID FUNCTION
    const getOrganizationId = await model.getOrganizationId(
      organizationDomain,
      organizationStatus
    );

    if (!getOrganizationId) return;

    //  console.log('getUserWakilId => ')
    // console.log(getUserWakilId)

    result = {
      message: "Get Organization's ID",
      environment: process.env.ENVIRONMENT,
      data: getOrganizationId,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// GET SETUP KORBAN'S ID
router.get("/korbanId", async (req, res) => {
  // VARIABLE SETUP
  let param = null;
  let result = null;

  let setupKorbanTahun = null;
  let setupKorbanStatus = null;
  let organizationId = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;

    console.log("param: ", param);

    setupKorbanTahun = param.setupKorbanTahun;
    setupKorbanStatus = param.setupKorbanStatus;
    organizationId = param.organizationId;

    // GET ORGANIZATION'S ID FUNCTION
    const getSetupKorbanId = await model.getSetupKorbanId(
      setupKorbanTahun,
      setupKorbanStatus,
      organizationId
    );

    if (!getSetupKorbanId) return;

    result = {
      message: "Get Setup Korban's ID",
      environment: process.env.ENVIRONMENT,
      data: getSetupKorbanId,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// GET EJEN DETAILS
router.get("/ejenDetails", async (req, res) => {
  // VARIABLE SETUP
  let result = null;
  let message = null;
  let userEjenPermalink = req.query.userEjenPermalink;

  try {
    if (userEjenPermalink == null || userEjenPermalink == "")
      message = "userEjenPermalink cannot be empty";

    // GET RESIT NUMBER FUNCTION
    const getEjenDetails = await model.getEjenDetails(userEjenPermalink);

    if (!getEjenDetails) return;

    result = {
      message: "Get Ejen Detail",
      environment: process.env.ENVIRONMENT,
      data: getEjenDetails,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// GET USER WAKIL'S ID
router.get("/semakWakil", async (req, res) => {
  // VARIABLE SETUP
  let param = null;
  let result = null;
  let message = null;
  let message2 = null;
  let data = null;
  let data2 = null;
  let data3 = null;

  let userFullname = null;
  let userPhoneNo = null;
  let userEmail = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;

    // console.log("param: ", param);

    userFullname = param.userFullname;
    userPhoneNo = param.userPhoneNo;
    userEmail = param.userEmail;

    if (userFullname == null || userFullname == "")
      message = "userFullname cannot be empty";
    else if (userPhoneNo == null || userPhoneNo == "")
      message = "userPhoneNo cannot be empty";
    else if (userEmail == null || userEmail == "")
      message = "userEmail cannot be empty";
    else {
      const checkEmail = await model.checkEmail(userEmail);
      if (!checkEmail) return;
      else data2 = checkEmail;
      const checkPhone = await model.checkPhone(userPhoneNo);
      if (!checkPhone) return;
      else data3 = checkPhone;

      console.log("checkEmail => ");
      console.log(checkEmail);
      console.log("checkPhone => ");
      console.log(checkPhone);

      if (checkEmail.length > 0 && checkPhone.length > 0)
        message = "Email & Phone number already exist";
      else if (checkEmail.length > 0) message = "Email already exist";
      else if (checkPhone.length > 0) message = "Phone number already exist";

      // GET USER WAKIL ID FUNCTION
      const getUserWakilId = await model.getUserWakilId(
        userFullname,
        userPhoneNo,
        userEmail
      );

      if (!getUserWakilId) return;
      else {
        message2 = "Get user Wakil Id";
        data = getUserWakilId;
      }
      console.log("getUserWakilId => ");
      console.log(getUserWakilId);
    }

    result = {
      message: message,
      message2: message2,
      environment: process.env.ENVIRONMENT,
      data: data,
      data2: data2,
      data3: data3,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// INSERT NEW USER INTO USER AND USER WAKIL TABLE
router.post("/daftarPenggunaBaru", async (req, res) => {
  let param = null;
  let result = null;

  let userFullname = null;
  let userPhoneNo = null;
  let userEmail = null;

  let status = null;
  let message = null;
  let data = null;
  let data2 = null;

  try {
    // BIND PARAMETER TO VARIABLES
    userFullname = req.body.params.userFullname;
    userPhoneNo = req.body.params.userPhoneNo;
    userEmail = req.body.params.userEmail;

    if (userFullname == null || userFullname == "") {
      status = "unsuccess";
      message = "userFullname cannot be empty";
    } else if (userPhoneNo == null || userPhoneNo == "") {
      status = "unsuccess";
      message = "userPhoneNo cannot be empty";
    } else if (userEmail == null || userEmail == "") {
      status = "unsuccess";
      message = "userEmail cannot be empty";
    } else {
      // INSERT NEW USER INTO USER TABLE
      const insertUser = await model.insertUser(
        userFullname,
        userPhoneNo,
        userEmail
      );
      if (!insertUser) return;
      status = "success";
      message = "New Wakil has been inserted into database";
      data = insertUser;

      // INSERT NEW USER INTO USER WAKIL TABLE
      const insertUserWakil = await model.insertUserWakil(insertUser);
      if (!insertUserWakil) return;
      data2 = insertUserWakil;
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  result = {
    environment: process.env.ENVIRONMENT,
    status: status,
    message: message,
    data: data,
    data2: data2,
  };

  res.status(200).json(result);
});

router.post("/testsms", async (req, res) => {
  let param = null;
  let result = null;

  let status = null;
  let message = null;
  let data = null;

  try {
    const sendSMS = await model.sendSMS("HAHAHA", "01120624714");
    if (!sendSMS) return;
    else {
      data = sendSMS;
      status = "Berjaya";
      message = "Anda berjaya menghantar SMS kepada pengguna";
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  result = {
    environment: process.env.ENVIRONMENT,
    status: status,
    message: message,
    data: data,
  };

  res.status(200).json(result);
});

// GET STOCK SELECTION
router.get("/stockSelection", async (req, res) => {
  let result = null;
  let status = null;
  let message = null;
  let data = null;

  let implementerNegara = null;
  let implementerIbadah = null;
  let tempahanQuantity = null;
  let tempahanIbadahType = null;
  let jenisHaiwan = null;
  let limit = null;

  console.log(req.query);

  try {
    implementerNegara = req.query.implementerNegara;
    implementerIbadah = req.query.implementerIbadah;
    tempahanQuantity = req.query.tempahanQuantity;
    tempahanIbadahType = req.query.tempahanIbadahType;
    jenisHaiwan = req.query.jenisHaiwan;
    limit = req.query.limit;

    if (implementerNegara == null || implementerNegara == "") {
      status = "Failed";
      message = "implementerNegara cannot be empty";
    } else if (implementerIbadah == null || implementerIbadah == "") {
      status = "Failed";
      message = "implementerIbadah cannot be empty";
    } else if (tempahanQuantity == null || tempahanQuantity == "") {
      status = "Failed";
      message = "tempahanQuantity cannot be empty";
    } else if (tempahanIbadahType == null || tempahanIbadahType == "") {
      status = "Failed";
      message = "tempahanIbadahType cannot be empty";
    } else if (jenisHaiwan == null || jenisHaiwan == "") {
      status = "Failed";
      message = "jenisHaiwan cannot be empty";
    } else if (limit == null || limit == "") {
      status = "Failed";
      message = "limit cannot be empty";
    } else {
      const getStockSelection = await model.getStockSelection(
        implementerNegara,
        implementerIbadah,
        tempahanQuantity,
        tempahanIbadahType,
        jenisHaiwan,
        limit
      );
      // .getStockSelection('Semenanjung', 32, 1, 'Korban', 'Lembu', 1);
      // getStockSelection(implementerNegara,implementerIbadah,tempahanQuantity,tempahanIbadahType,jenisHaiwan,limit)

      if (!getStockSelection) return;
      else {
        status = "Success";
        message = "Get Stok Selection";
        data = getStockSelection;
      }
    }

    result = {
      status: status,
      message: message,
      environment: process.env.ENVIRONMENT,
      data: data,
    };
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: "API Error",
    };
  }

  // RETURN
  res.status(200).json(result);
});

// INSERT NEW USER INTO USER AND USER WAKIL TABLE
router.post("/tempahanBaru", async (req, res) => {
  let result = null;

  let tempahanNo = null;
  let setupKorbanId = null;
  let userWakilId = null;

  let organizationId = null;
  let tempahanIbadahTypeCode = null;
  let tempahanIbadahType = null;

  let tempahanTotalPrice = null;
  let tempahanQuantity = null;

  let butiranTempahan = null;

  let status = null;
  let message = null;
  let data = null;
  let data2 = null;
  let data3 = null;
  let data4 = null;
  let data5 = null;

  console.log("PARAMETER TEMPAHAN BARU: ", req.body);
  // console.log(req.body)

  try {
    // BIND PARAMETER TO VARIABLES
    tempahanNo = req.body.tempahanNo;
    setupKorbanId = req.body.setupKorbanId;
    userWakilId = req.body.userWakilId;
    userEjenId = req.body.userEjenId;
    organizationId = req.body.organizationId;
    tempahanIbadahTypeCode = req.body.tempahanIbadahTypeCode;
    tempahanIbadahType = req.body.tempahanIbadahType;
    tempahanTotalPrice = req.body.tempahanTotalPrice;
    tempahanQuantity = req.body.tempahanQuantity;
    butiranTempahan = req.body.butiranTempahan;

    if (tempahanNo == null || tempahanNo == "") {
      status = "Failed";
      message = "tempahanNo cannot be empty";
    } else if (setupKorbanId == null || setupKorbanId == "") {
      status = "Failed";
      message = "setupKorbanId cannot be empty";
    } else if (userWakilId == null || userWakilId == "") {
      status = "Failed";
      message = "userWakilId cannot be empty";
    } else if (organizationId == null || organizationId == "") {
      status = "Failed";
      message = "organizationId cannot be empty";
    } else if (tempahanIbadahTypeCode == null || tempahanIbadahTypeCode == "") {
      status = "Failed";
      message = "tempahanIbadahTypeCode cannot be empty";
    } else if (tempahanIbadahType == null || tempahanIbadahType == "") {
      status = "Failed";
      message = "tempahanIbadahType cannot be empty";
    } else if (tempahanTotalPrice == null || tempahanTotalPrice == "") {
      status = "Failed";
      message = "tempahanTotalPrice cannot be empty";
    } else if (tempahanQuantity == null || tempahanQuantity == "") {
      status = "Failed";
      message = "tempahanQuantity cannot be empty";
    } else {
      const checkTempahanId = await model.checkTempahanId(tempahanNo);

      console.log("checkTempahanId => ", checkTempahanId);
      if (!checkTempahanId) return;
      else {
        console.log("checkTempahanId => ");
        console.log(checkTempahanId);
        console.log(checkTempahanId.length);
      }

      if (checkTempahanId.length > 0) {
        console.log("update existing tempahan");
      } else {
        console.log("insert new tempahan");
      }

      // if tempahan id already exist
      if (checkTempahanId.length > 0) {
        // UPDATE EXISTING TEMPAHAN
        const updatePendingTempahan = await model.updatePendingTempahan(
          tempahanNo,
          setupKorbanId,
          userWakilId,
          userEjenId,
          organizationId,
          tempahanIbadahTypeCode,
          tempahanIbadahType,
          tempahanTotalPrice,
          tempahanQuantity,
          "Pending Order",
          23
        );

        if (!updatePendingTempahan) return;
        else {
          status = "Success";
          message = "New Tempahan has been inserted into database";
          data = checkTempahanId[0].tempahanId;

          let implementerNegara = [];
          let jenisHaiwan = [];
          let kuantitiHaiwan = [];
          let nama = [];
          let jantina = [];
          let harga = [];

          butiranTempahan.forEach((value) => {
            for (let key in value) {
              console.log(value[key]);
              if (key === "negara") {
                implementerNegara.push(value[key]);
              }
              if (key === "haiwan") {
                jenisHaiwan.push(value[key]);
              }
              if (key === "kuantiti") {
                kuantitiHaiwan.push(value[key]);
              }
              if (key === "nama") {
                nama.push(value[key]);
              }
              if (key === "jantina") {
                jantina.push(value[key]);
              }
              if (key === "price") {
                harga.push(value[key]);
              }
            }
          });

          let BilBakiBaru = 0;
          let liveStokId = null;
          let liveStokBahagianId = null;
          let implementerId = null;
          let implementerLokasiId = null;
          let liveStokType = null;
          let tempahanDetailsHaiwanTypeCode = null;
          let wakilPesertaId = null;
          let jumlahBakiBhgn = null;
          let jumlahBaki = null;

          // traverse through tempahans array
          for (let i = 0; i < tempahanQuantity; i++) {
            // check peserta name if exist
            const getWakilPeserta = await model.getWakilPeserta(
              nama[i],
              jantina[i]
            );
            if (!getWakilPeserta) return;
            else {
              console.log("getWakilPeserta => ");
              console.log(getWakilPeserta);
              data5 = getWakilPeserta;

              // peserta name does not exist
              if (getWakilPeserta.length == 0) {
                // insert new peserta name
                const insertWakilPeserta = await model.insertWakilPeserta(
                  userWakilId,
                  nama[i],
                  jantina[i],
                  "Active",
                  20
                );
                if (!insertWakilPeserta) return;
                console.log("insertWakilPeserta => ");
                console.log(insertWakilPeserta[0]);
                // assign new inserted peserta id to variable => wakilPesertaId
                wakilPesertaId = insertWakilPeserta[0];
              }
              // peserta name already exist
              if (getWakilPeserta.length > 0) {
                console.log("user dah ada");
                console.log(getWakilPeserta);
                wakilPesertaId = getWakilPeserta[0].wakilPesertaId;
              }
            }

            console.log("=============== i => " + i + " ===============");

            let liveStokBilBahagianBaki = null;

            // if the haiwan is in Ekor
            if (kuantitiHaiwan[i] == "Ekor") {
              // if the haiwan is Ekor & Kambing
              if (jenisHaiwan[i] == "Kambing") {
                console.log("SAYA LALU EKOR KAMBING");
                const getStockSelection = await model.getStockSelection(
                  implementerNegara[i],
                  tempahanIbadahTypeCode,
                  1,
                  tempahanIbadahType,
                  jenisHaiwan[i],
                  1
                );

                if (!getStockSelection) return;
                else {
                  console.log("get Stock Selection => ");
                  console.log(getStockSelection);

                  liveStokId = getStockSelection[0].liveStokId;
                  liveStokBilBahagianBaki =
                    getStockSelection[0].liveStokBilBahagianBaki;
                  liveStokBahagianId = getStockSelection[0].liveStokBahagianId;
                  implementerId = getStockSelection[0].implementerId;
                  implementerLokasiId =
                    getStockSelection[0].implementerLokasiId;
                  liveStokType = getStockSelection[0].liveStokType;

                  console.log("liveStokId => " + liveStokId);
                  console.log(
                    "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
                  );

                  if (liveStokType == "Lembu")
                    tempahanDetailsHaiwanTypeCode = 15;
                  if (liveStokType == "Kambing")
                    tempahanDetailsHaiwanTypeCode = 16;
                  if (liveStokType == "Unta")
                    tempahanDetailsHaiwanTypeCode = 17;

                  if (i + 1 > checkTempahanId.length) {
                    const insertTempahanDetails =
                      await model.insertTempahanDetails(
                        checkTempahanId[0].tempahanId,
                        implementerId,
                        implementerLokasiId,
                        wakilPesertaId,
                        liveStokType,
                        tempahanDetailsHaiwanTypeCode,
                        1,
                        kuantitiHaiwan[i],
                        harga[i],
                        "Pending Order",
                        20
                      );
                    if (!insertTempahanDetails) return;
                    else data4 = insertTempahanDetails;
                    console.log(
                      "insert new tempahan from continue pending order"
                    );
                  } else {
                    const updatePendingTempahanDetails =
                      await model.updatePendingTempahanDetails(
                        checkTempahanId[i].tempahanId,
                        implementerId,
                        implementerLokasiId,
                        wakilPesertaId,
                        liveStokType,
                        tempahanDetailsHaiwanTypeCode,
                        1,
                        kuantitiHaiwan[i],
                        harga[i],
                        "Pending Order",
                        20,
                        checkTempahanId[i].tempahanDetailsId
                      );
                    if (!updatePendingTempahanDetails) return;
                    else data4 = updatePendingTempahanDetails;
                    console.log("update old tempahan");
                  }
                }
              }
              // if the haiwan is Ekor & Lembu or Unta
              else {
                console.log("SAYA LALU EKOR BUKAN KAMBING");
                const getStockSelection = await model.getStockSelection(
                  implementerNegara[i],
                  tempahanIbadahTypeCode,
                  7,
                  tempahanIbadahType,
                  jenisHaiwan[i],
                  7
                );

                if (!getStockSelection) return;
                else {
                  console.log("get Stock Selection => ");
                  console.log(getStockSelection);

                  implementerId = getStockSelection[0].implementerId;
                  implementerLokasiId =
                    getStockSelection[0].implementerLokasiId;
                  liveStokType = getStockSelection[0].liveStokType;

                  if (liveStokType == "Lembu")
                    tempahanDetailsHaiwanTypeCode = 15;
                  if (liveStokType == "Kambing")
                    tempahanDetailsHaiwanTypeCode = 16;
                  if (liveStokType == "Unta")
                    tempahanDetailsHaiwanTypeCode = 17;

                  if (i + 1 > checkTempahanId.length) {
                    const insertTempahanDetails =
                      await model.insertTempahanDetails(
                        checkTempahanId[0].tempahanId,
                        implementerId,
                        implementerLokasiId,
                        wakilPesertaId,
                        liveStokType,
                        tempahanDetailsHaiwanTypeCode,
                        1,
                        kuantitiHaiwan[i],
                        harga[i],
                        "Pending Order",
                        20
                      );
                    if (!insertTempahanDetails) return;
                    else data4 = insertTempahanDetails;
                    console.log(
                      "insert new tempahan from continue pending order"
                    );
                  } else {
                    const updatePendingTempahanDetails =
                      await model.updatePendingTempahanDetails(
                        checkTempahanId[i].tempahanId,
                        implementerId,
                        implementerLokasiId,
                        wakilPesertaId,
                        liveStokType,
                        tempahanDetailsHaiwanTypeCode,
                        1,
                        kuantitiHaiwan[i],
                        harga[i],
                        "Pending Order",
                        20,
                        checkTempahanId[i].tempahanDetailsId
                      );
                    if (!updatePendingTempahanDetails) return;
                    else data4 = updatePendingTempahanDetails;
                    console.log("update old tempahan");
                  }

                  // const updatePendingTempahanDetails = await model.updatePendingTempahanDetails(checkTempahanId[i].tempahanId,implementerId,implementerLokasiId,wakilPesertaId,getStockSelection[i].liveStokType,tempahanDetailsHaiwanTypeCode,1,kuantitiHaiwan[i],harga[i],'Pending Order',20,checkTempahanId[i].tempahanDetailsId);
                  // if (!updatePendingTempahanDetails) return;
                  // else data4 = updatePendingTempahanDetails;
                }
              }
            }
            // if the haiwan is in Bahagian (Lembu & Unta)
            else {
              console.log("SAYA LALU BAHAGIAN");

              const getStockSelection = await model.getStockSelection(
                implementerNegara[i],
                tempahanIbadahTypeCode,
                1,
                tempahanIbadahType,
                jenisHaiwan[i],
                1
              );
              if (!getStockSelection) return;
              else {
                console.log("get Stock Selection => ");
                console.log(getStockSelection);

                liveStokId = getStockSelection[0].liveStokId;
                liveStokBilBahagianBaki =
                  getStockSelection[0].liveStokBilBahagianBaki;
                liveStokBahagianId = getStockSelection[0].liveStokBahagianId;
                implementerId = getStockSelection[0].implementerId;
                implementerLokasiId = getStockSelection[0].implementerLokasiId;
                liveStokType = getStockSelection[0].liveStokType;

                console.log("liveStokId => " + liveStokId);
                console.log(
                  "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
                );

                if (liveStokType == "Lembu") tempahanDetailsHaiwanTypeCode = 15;
                if (liveStokType == "Kambing")
                  tempahanDetailsHaiwanTypeCode = 16;
                if (liveStokType == "Unta") tempahanDetailsHaiwanTypeCode = 17;

                if (i + 1 > checkTempahanId.length) {
                  const insertTempahanDetails =
                    await model.insertTempahanDetails(
                      checkTempahanId[0].tempahanId,
                      implementerId,
                      implementerLokasiId,
                      wakilPesertaId,
                      liveStokType,
                      tempahanDetailsHaiwanTypeCode,
                      1,
                      kuantitiHaiwan[i],
                      harga[i],
                      "Pending Order",
                      20
                    );
                  if (!insertTempahanDetails) return;
                  else data4 = insertTempahanDetails;
                  console.log(
                    "insert new tempahan from continue pending order"
                  );
                } else {
                  const updatePendingTempahanDetails =
                    await model.updatePendingTempahanDetails(
                      checkTempahanId[i].tempahanId,
                      implementerId,
                      implementerLokasiId,
                      wakilPesertaId,
                      liveStokType,
                      tempahanDetailsHaiwanTypeCode,
                      1,
                      kuantitiHaiwan[i],
                      harga[i],
                      "Pending Order",
                      20,
                      checkTempahanId[i].tempahanDetailsId
                    );
                  if (!updatePendingTempahanDetails) return;
                  else data4 = updatePendingTempahanDetails;
                  console.log("update old tempahan");
                }
              }
            }
          }
        }
      }
      // insert new tempahan, tempahanDetails
      else {
        // INSERT NEW USER INTO USER TABLE
        const insertTempahan = await model.insertTempahan(
          tempahanNo,
          setupKorbanId,
          userWakilId,
          userEjenId,
          organizationId,
          tempahanIbadahTypeCode,
          tempahanIbadahType,
          tempahanTotalPrice,
          tempahanQuantity,
          "Pending Order",
          23
        );

        if (!insertTempahan) return;
        else {
          console.log("insertTempahan => ");
          console.log(insertTempahan);

          status = "Success";
          message = "New Tempahan has been inserted into database";
          data = insertTempahan;

          let implementerNegara = [];
          let jenisHaiwan = [];
          let kuantitiHaiwan = [];
          let nama = [];
          let jantina = [];
          let harga = [];

          butiranTempahan.forEach((value) => {
            // console.log(value)
            for (let key in value) {
              //   console.log(value[key])
              if (key === "negara") {
                implementerNegara.push(value[key]);
              }
              if (key === "haiwan") {
                jenisHaiwan.push(value[key]);
              }
              if (key === "kuantiti") {
                kuantitiHaiwan.push(value[key]);
              }
              if (key === "nama") {
                nama.push(value[key]);
              }
              if (key === "jantina") {
                jantina.push(value[key]);
              }
              if (key === "price") {
                harga.push(value[key]);
              }
            }
          });

          let BilBakiBaru = 0;
          let liveStokId = null;
          let liveStokBahagianId = null;
          let implementerId = null;
          let implementerLokasiId = null;
          let liveStokType = null;
          let tempahanDetailsHaiwanTypeCode = null;
          let wakilPesertaId = null;
          let jumlahBakiBhgn = null;
          let jumlahBaki = null;

          // traverse through tempahans array
          for (let i = 0; i < tempahanQuantity; i++) {
            // check peserta name if exist
            const getWakilPeserta = await model.getWakilPeserta(
              nama[i],
              jantina[i]
            );
            if (!getWakilPeserta) return;
            else {
              console.log("getWakilPeserta => ");
              console.log(getWakilPeserta);
              data5 = getWakilPeserta;

              // peserta name does not exist
              if (getWakilPeserta.length == 0) {
                // insert new peserta name
                const insertWakilPeserta = await model.insertWakilPeserta(
                  userWakilId,
                  nama[i],
                  jantina[i],
                  "Active",
                  20
                );
                if (!insertWakilPeserta) return;
                console.log("insertWakilPeserta => ");
                console.log(insertWakilPeserta[0]);
                // assign new inserted peserta id to variable => wakilPesertaId
                wakilPesertaId = insertWakilPeserta[0];
              }
              // peserta name already exist
              if (getWakilPeserta.length > 0) {
                console.log("user dah ada");
                console.log(getWakilPeserta);
                wakilPesertaId = getWakilPeserta[0].wakilPesertaId;
              }
            }

            console.log("=============== i => " + i + " ===============");

            let liveStokBilBahagianBaki = null;

            // if the haiwan is in Ekor
            if (kuantitiHaiwan[i] == "Ekor") {
              // if the haiwan is Ekor & Kambing
              if (jenisHaiwan[i] == "Kambing") {
                console.log("SAYA LALU EKOR KAMBING");
                const getStockSelection = await model.getStockSelection(
                  implementerNegara[i],
                  tempahanIbadahTypeCode,
                  1,
                  tempahanIbadahType,
                  jenisHaiwan[i],
                  1
                );

                if (!getStockSelection) return;
                else {
                  console.log("get Stock Selection => ");
                  console.log(getStockSelection);

                  liveStokId = getStockSelection[0].liveStokId;
                  liveStokBilBahagianBaki =
                    getStockSelection[0].liveStokBilBahagianBaki;
                  liveStokBahagianId = getStockSelection[0].liveStokBahagianId;
                  implementerId = getStockSelection[0].implementerId;
                  implementerLokasiId =
                    getStockSelection[0].implementerLokasiId;
                  liveStokType = getStockSelection[0].liveStokType;

                  console.log("liveStokId => " + liveStokId);
                  console.log(
                    "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
                  );

                  if (liveStokType == "Lembu")
                    tempahanDetailsHaiwanTypeCode = 15;
                  if (liveStokType == "Kambing")
                    tempahanDetailsHaiwanTypeCode = 16;
                  if (liveStokType == "Unta")
                    tempahanDetailsHaiwanTypeCode = 17;

                  const insertTempahanDetails =
                    await model.insertTempahanDetails(
                      insertTempahan,
                      implementerId,
                      implementerLokasiId,
                      wakilPesertaId,
                      liveStokType,
                      tempahanDetailsHaiwanTypeCode,
                      1,
                      kuantitiHaiwan[i],
                      harga[i],
                      "Pending Order",
                      20
                    );
                  if (!insertTempahanDetails) return;
                  else data4 = insertTempahanDetails;
                }
              }
              // if the haiwan is Ekor & Lembu or Unta
              else {
                console.log("SAYA LALU EKOR BUKAN KAMBING");
                const getStockSelection = await model.getStockSelection(
                  implementerNegara[i],
                  tempahanIbadahTypeCode,
                  7,
                  tempahanIbadahType,
                  jenisHaiwan[i],
                  7
                );

                if (!getStockSelection) return;
                else {
                  console.log("get Stock Selection => ");
                  console.log(getStockSelection);

                  implementerId = getStockSelection[0].implementerId;
                  implementerLokasiId =
                    getStockSelection[0].implementerLokasiId;
                  liveStokType = getStockSelection[0].liveStokType;

                  if (liveStokType == "Lembu")
                    tempahanDetailsHaiwanTypeCode = 15;
                  if (liveStokType == "Kambing")
                    tempahanDetailsHaiwanTypeCode = 16;
                  if (liveStokType == "Unta")
                    tempahanDetailsHaiwanTypeCode = 17;

                  const insertTempahanDetails =
                    await model.insertTempahanDetails(
                      insertTempahan,
                      implementerId,
                      implementerLokasiId,
                      wakilPesertaId,
                      liveStokType,
                      tempahanDetailsHaiwanTypeCode,
                      1,
                      kuantitiHaiwan[i],
                      harga[i],
                      "Pending Order",
                      20
                    );
                  if (!insertTempahanDetails) return;
                  else data4 = insertTempahanDetails;
                }
              }
            }
            // if the haiwan is in Bahagian (Lembu & Unta)
            else {
              console.log("SAYA LALU BAHAGIAN");

              const getStockSelection = await model.getStockSelection(
                implementerNegara[i],
                tempahanIbadahTypeCode,
                1,
                tempahanIbadahType,
                jenisHaiwan[i],
                1
              );
              if (!getStockSelection) return;
              else {
                console.log("get Stock Selection => ");
                console.log(getStockSelection);

                liveStokId = getStockSelection[0].liveStokId;
                liveStokBilBahagianBaki =
                  getStockSelection[0].liveStokBilBahagianBaki;
                liveStokBahagianId = getStockSelection[0].liveStokBahagianId;
                implementerId = getStockSelection[0].implementerId;
                implementerLokasiId = getStockSelection[0].implementerLokasiId;
                liveStokType = getStockSelection[0].liveStokType;

                console.log("liveStokId => " + liveStokId);
                console.log(
                  "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
                );

                if (liveStokType == "Lembu") tempahanDetailsHaiwanTypeCode = 15;
                if (liveStokType == "Kambing")
                  tempahanDetailsHaiwanTypeCode = 16;
                if (liveStokType == "Unta") tempahanDetailsHaiwanTypeCode = 17;

                const insertTempahanDetails = await model.insertTempahanDetails(
                  insertTempahan,
                  implementerId,
                  implementerLokasiId,
                  wakilPesertaId,
                  liveStokType,
                  tempahanDetailsHaiwanTypeCode,
                  1,
                  kuantitiHaiwan[i],
                  harga[i],
                  "Pending Order",
                  20
                );
                if (!insertTempahanDetails) return;
                else data4 = insertTempahanDetails;
              }
            }
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  result = {
    environment: process.env.ENVIRONMENT,
    status: status,
    message: message,
    data: data,
    data2: data2,
    data3: data3,
    data4: data4,
    data5: data5,
  };

  console.log("result => ", result);

  res.status(200).json(result);
});

const multer = require("multer");
const today = moment(new Date()).format("YYYY-MM-DD HH-mm-ss");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "images/");
  },
  filename: function (req, file, cb) {
    cb(null, today + "-" + file.originalname);
    console.log("diskStorage");
    console.log(file.filename);
  },
});

var upload = multer({ storage: storage });

router.post("/muatNaikResit", upload.single("avatar"), async (req, res) => {
  let param = null;
  let result = null;
  let status = null;
  let message = null;
  let data = null;
  let data2 = null;

  let tempahanId = null;
  let userWakilId = null;
  let resitAmountPaid = null;
  let resitPaymentMethod = null;
  let tempahanNumber = null;
  let namaWakil = null;
  let emelWakil = null;
  let noTelefonWakil = null;
  let paymentMethod = null;
  let selectedBank = null;
  let tempahanInfo = null;
  let tempahanDetailsInfo = null;
  let maklumatTempahan = null;

  let userEjenId = null;
  let ejenCommissionType = null;
  let ejenCommissionValue = null;
  let ejenCommissionCategory = null;
  let tempahanTotalPrice = null;

  let rowError = [];
  let komisyenPerBahagian = 0;
  let komisyenPerClosing = 0;
  let komisyenCurrency = null;
  let chargeFee = 0;

  let modalNoTempahan = null;
  let modalNamaWakil = null;
  let modalKaedahPembayaran = null;
  let modalTelefonWakil = null;
  let modalNoTransaksi = null;
  let modalEmailWakil = null;
  let modalStatusPembayaran = null;
  let modalTarikhTempahan = null;
  let modalStatusTempahan = null;
  let modalTarikhMuatNaikResit = null;
  let modalTarikhBayaran = null;

  let liveStokBilBahagianBaki = null;
  let liveStokBilBahagian = null;
  let BilBakiBaru = null;
  let row = 0;
  let jumlahKeseluruhan = 0;
  let jumlahBaki = 0;
  let jumlahKeseluruhanBhgn = 0;
  let jumlahBakiBhgn = 0;

  console.log("req.file");
  console.log(req.file);

  if (req.file) data = "success";

  console.log("req.body");
  console.log(req.body);

  if (req.body) data2 = "success";

  try {
    param = req.body;

    tempahanId = param.tempahanId;
    modalNoTempahan = param.modalNoTempahan;
    modalNamaWakil = param.modalNamaWakil;
    modalKaedahPembayaran = param.modalKaedahPembayaran;
    modalTelefonWakil = param.modalTelefonWakil;
    modalNoTransaksi = param.modalNoTransaksi;
    modalEmailWakil = param.modalEmailWakil;

    modalTarikhMuatNaikResit = moment(param.modalTarikhMuatNaikResit).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    modalStatusPembayaran = param.modalStatusPembayaran;
    modalTarikhTempahan = moment(param.modalTarikhTempahan).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    modalStatusTempahan = param.modalStatusTempahan;
    modalTarikhBayaran = moment(param.modalTarikhBayaran).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    resitPaymentMethod = param.modalKaedahPembayaran;

    // console.log(
    //   modalTarikhMuatNaikResit,
    //   modalStatusPembayaran,
    //   modalTarikhTempahan,
    //   modalStatusTempahan,
    //   modalTarikhBayaran,
    //   resitPaymentMethod,
    // );

    // BIND PARAMETER TO VARIABLES
    // tempahanId = req.body.tempahanId;
    // userWakilId = req.body.userWakilId;
    // resitAmountPaid = req.body.resitAmountPaid;
    // resitPaymentMethod = req.body.resitPaymentMethod;
    // tempahanNumber = req.body.tempahanNumber;
    // namaWakil = req.body.namaWakil;
    // emelWakil = req.body.emelWakil;
    // noTelefonWakil = req.body.noTelefonWakil;
    // selectedBank = req.body.selectedBank;
    // tempahanInfo = req.body.tempahanInfo;
    // tempahanDetailsInfo = req.body.tempahanDetailsInfo;
    // maklumatTempahan = req.body.maklumatTempahan

    if (tempahanId == null || tempahanId == "") {
      status = "Failed";
      message = "tempahanId cannot be empty";
    } else {
      //  CHECK IF ALREADY INSERT INTO TEMPAHAN DETAILS LIVE STOCK
      const checkIfAlreadyInsertIntoTempahanDetailsLiveStock =
        await model.checkIfAlreadyInsertIntoTempahanDetailsLiveStock(
          tempahanId
        );
      if (!checkIfAlreadyInsertIntoTempahanDetailsLiveStock) return;
      else {
        console.log("checkIfAlreadyInsertIntoTempahanDetailsLiveStock => ");
        console.log(checkIfAlreadyInsertIntoTempahanDetailsLiveStock);
      }

      // GET TEMPAHAN DATA
      const getTempahan = await model.getTempahan(tempahanId);
      if (!getTempahan) return;
      else {
        console.log("getTempahan => ");
        console.log(getTempahan);
        resitAmountPaid = getTempahan[0].tempahanTotalPrice;
      }

      // GET TEMPAHAN DETAILS DATA
      const getTempahanDetails = await model.getTempahanDetails(tempahanId);
      if (!getTempahanDetails) return;
      else {
        console.log("getTempahanDetails => ");
        console.log(getTempahanDetails);
      }

      // GET TEMPAHAN DETAILS LIVESTOK DATA
      const getTempahanDetailsLiveStok = await model.getTempahanDetailsLiveStok(
        tempahanId
      );
      if (!getTempahanDetailsLiveStok) return;
      else {
        console.log("getTempahanDetailsLiveStok => ");
        console.log(getTempahanDetailsLiveStok);
      }

      // GET TEMPAHAN DETAILS LIVESTOK DATA
      const getResitDetails = await model.getResitDetails(tempahanId);
      if (!getResitDetails) return;
      else {
        console.log("getResitDetails => ");
        console.log(getResitDetails);
      }

      // GET TEMPAHAN DETAILS LIVESTOK DATA
      const getRingkasanTempahan = await model.getRingkasanTempahan(tempahanId);
      if (!getRingkasanTempahan) return;
      else {
        console.log("getRingkasanTempahan => ");
        console.log(getRingkasanTempahan);
      }

      // check If Already Insert Into TempahanDetailsLiveStock
      if (checkIfAlreadyInsertIntoTempahanDetailsLiveStock[0].count == 0) {
        // UPDATE TEMPAHAN TABLE
        const updateTempahan = await model.updateTempahan(
          "Pending Payment",
          20,
          tempahanId,
          modalTarikhBayaran,
          modalTarikhMuatNaikResit
        );
        if (!updateTempahan) return;
        else {
          console.log("updateTempahan => ");
          console.log(updateTempahan);
        }

        let row = 0;
        for (let i = 0; i < getTempahanDetails.length; i++) {
          row += 1;
          console.log("=============== i => " + i + " ===============");
          console.log("=============== row => " + row + " ===============");
          if (getTempahanDetails[i].tempahanDetailsBhgnType == "Ekor") {
            if (getTempahanDetails[i].tempahanDetailsHaiwanType == "Kambing") {
              console.log("Saya lalu ekor + kambing");

              const getStockSelection = await model.getStockSelection(
                getTempahanDetails[i].implementerNegara,
                getTempahanDetails[i].tempahanIbadahTypeCode,
                1,
                getTempahanDetails[i].tempahanIbadahType,
                getTempahanDetails[i].tempahanDetailsHaiwanType,
                1
              );
              if (!getStockSelection) return;
              else {
                console.log("get Stock Selection => Resit Baru");
                console.log(getStockSelection);

                let liveStokBahagianIdArray = [];

                getStockSelection.forEach((value) => {
                  for (let key in value) {
                    // console.log('key => ' + key + ' value => ' + value[key])
                    if (key === "liveStokBahagianId") {
                      liveStokBahagianIdArray.push(value[key]);
                    }
                  }
                });

                console.log("liveStokBahagianIdArray => ");
                console.log(liveStokBahagianIdArray);

                // check if stock is not found
                if (liveStokBahagianIdArray.length == 0) {
                  rowError.push({
                    nama: getTempahanDetails[i].wakilPesertaName,
                    kawasan: getTempahanDetails[i].implementerKawasan,
                    negara: getTempahanDetails[i].implementerNegara,
                    jenis: getTempahanDetails[i].tempahanDetailsHaiwanType,
                    baris: row,
                  });

                  console.log("Maaf tiada stok untuk tempahan baris ke " + row);

                  resitAmountPaid =
                    resitAmountPaid -
                    getTempahanDetails[i].tempahanDetailsPrice;

                  // UPDATE TEMPAHAN DETAILS TABLE
                  const updateTempahanDetailsBasedOnTempahanDetailsId =
                    await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                      "Failed",
                      22,
                      getTempahanDetails[i].tempahanDetailsId
                    );
                  if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                  else {
                    console.log(
                      "updateTempahanDetailsBasedOnTempahanDetailsId => "
                    );
                    console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                  }
                } else {
                  // UPDATE TEMPAHAN DETAILS TABLE
                  const updateTempahanDetailsBasedOnTempahanDetailsId =
                    await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                      "Pending Payment",
                      20,
                      getTempahanDetails[i].tempahanDetailsId
                    );
                  if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                  else {
                    console.log(
                      "updateTempahanDetailsBasedOnTempahanDetailsId => "
                    );
                    console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                  }

                  komisyenPerClosing += 1;
                  console.log("komisyenPerClosing => " + komisyenPerClosing);

                  const getCommissionByParts = await model.getCommissionByParts(
                    getTempahanDetails[i].implementerNegara,
                    getTempahanDetails[i].tempahanDetailsHaiwanType
                  );
                  if (!getCommissionByParts) return;
                  else {
                    console.log("getCommissionByParts() => ");
                    console.log(getCommissionByParts);
                    komisyenCurrency = getCommissionByParts[0].CURRENCY;
                    komisyenPerBahagian += parseFloat(
                      getCommissionByParts[0].KOMISYEN
                    );
                    //   console.log('komisyenPerBahagian => ' + komisyenPerBahagian)
                  }
                }

                for (let v = 0; v < liveStokBahagianIdArray.length; v++) {
                  const insertTempahanDetailsLiveStok =
                    await model.insertTempahanDetailsLiveStok(
                      getTempahanDetails[i].tempahanDetailsId,
                      getTempahan[0].tempahanId,
                      getStockSelection[v].implementerId,
                      getStockSelection[v].implementerLokasiId,
                      getTempahanDetails[i].wakilPesertaId,
                      getStockSelection[v].liveStokId,
                      liveStokBahagianIdArray[v]
                    );
                  if (!insertTempahanDetailsLiveStok) return;
                  // else data4 = insertTempahanDetailsLiveStok;
                  console.log("insertTempahanDetailsLiveStok => ");
                  console.log(insertTempahanDetailsLiveStok);

                  const updateLiveStockBahagianStatus =
                    await model.updateLiveStockBahagianStatus(
                      "Pending Reserved",
                      null,
                      getStockSelection[v].liveStokId,
                      liveStokBahagianIdArray[v]
                    );
                  if (!updateLiveStockBahagianStatus) return;
                  // else data3 = updateLiveStockBahagianStatus;
                  console.log("updateLiveStockBahagianStatus => ");
                  console.log(updateLiveStockBahagianStatus);

                  const updateLiveStockStatus =
                    await model.updateLiveStockStatus(
                      "Pending Reserved",
                      null,
                      getStockSelection[v].liveStokId
                    );
                  if (!updateLiveStockStatus) return;
                  // data2 = updateLiveStockStatus;
                  console.log("updateLiveStockStatus => ");
                  console.log(updateLiveStockStatus);
                }
              }
            } else {
              console.log("Saya lalu ekor + bukan kambing");

              const getStockSelection = await model.getStockSelection(
                getTempahanDetails[i].implementerNegara,
                getTempahanDetails[i].tempahanIbadahTypeCode,
                7,
                getTempahanDetails[i].tempahanIbadahType,
                getTempahanDetails[i].tempahanDetailsHaiwanType,
                7
              );
              if (!getStockSelection) return;
              else {
                console.log("get Stock Selection => Resit Baru ");
                console.log(getStockSelection);

                let liveStokBahagianIdArray = [];

                getStockSelection.forEach((value) => {
                  for (let key in value) {
                    // console.log('key => ' + key + ' value => ' + value[key])
                    if (key === "liveStokBahagianId") {
                      liveStokBahagianIdArray.push(value[key]);
                    }
                  }
                });

                console.log("liveStokBahagianIdArray => ");
                console.log(liveStokBahagianIdArray);

                // check if stock is not found
                if (liveStokBahagianIdArray.length == 0) {
                  rowError.push({
                    nama: getTempahanDetails[i].wakilPesertaName,
                    kawasan: getTempahanDetails[i].implementerKawasan,
                    negara: getTempahanDetails[i].implementerNegara,
                    jenis: getTempahanDetails[i].tempahanDetailsHaiwanType,
                    baris: row,
                  });

                  console.log("Maaf tiada stok untuk tempahan baris ke " + row);

                  resitAmountPaid =
                    resitAmountPaid -
                    getTempahanDetails[i].tempahanDetailsPrice;

                  // UPDATE TEMPAHAN DETAILS TABLE
                  const updateTempahanDetailsBasedOnTempahanDetailsId =
                    await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                      "Failed",
                      22,
                      getTempahanDetails[i].tempahanDetailsId
                    );
                  if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                  else {
                    console.log(
                      "updateTempahanDetailsBasedOnTempahanDetailsId => "
                    );
                    console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                  }
                } else {
                  // UPDATE TEMPAHAN DETAILS TABLE
                  const updateTempahanDetailsBasedOnTempahanDetailsId =
                    await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                      "Pending Payment",
                      20,
                      getTempahanDetails[i].tempahanDetailsId
                    );
                  if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                  else {
                    console.log(
                      "updateTempahanDetailsBasedOnTempahanDetailsId => "
                    );
                    console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                  }

                  komisyenPerClosing += 7;
                  console.log("komisyenPerClosing => " + komisyenPerClosing);

                  const getCommissionByParts = await model.getCommissionByParts(
                    getTempahanDetails[i].implementerNegara,
                    getTempahanDetails[i].tempahanDetailsHaiwanType
                  );
                  if (!getCommissionByParts) return;
                  else {
                    console.log("getCommissionByParts() => ");
                    console.log(getCommissionByParts);
                    komisyenCurrency = getCommissionByParts[0].CURRENCY;
                    komisyenPerBahagian +=
                      parseFloat(getCommissionByParts[0].KOMISYEN) * 7;
                    console.log(
                      "komisyenPerBahagian => " + komisyenPerBahagian
                    );
                  }
                }

                for (let v = 0; v < liveStokBahagianIdArray.length; v++) {
                  const insertTempahanDetailsLiveStok =
                    await model.insertTempahanDetailsLiveStok(
                      getTempahanDetails[i].tempahanDetailsId,
                      getTempahan[0].tempahanId,
                      getStockSelection[v].implementerId,
                      getStockSelection[v].implementerLokasiId,
                      getTempahanDetails[i].wakilPesertaId,
                      getStockSelection[v].liveStokId,
                      liveStokBahagianIdArray[v]
                    );
                  if (!insertTempahanDetailsLiveStok) return;
                  console.log("insertTempahanDetailsLiveStok => ");
                  console.log(insertTempahanDetailsLiveStok);

                  const updateLiveStockBahagianStatus =
                    await model.updateLiveStockBahagianStatus(
                      "Pending Reserved",
                      null,
                      getStockSelection[v].liveStokId,
                      liveStokBahagianIdArray[v]
                    );
                  if (!updateLiveStockBahagianStatus) return;
                  console.log("updateLiveStockBahagianStatus => ");
                  console.log(updateLiveStockBahagianStatus);

                  const updateLiveStockStatus =
                    await model.updateLiveStockStatus(
                      "Pending Reserved",
                      null,
                      getStockSelection[v].liveStokId
                    );
                  if (!updateLiveStockStatus) return;
                  console.log("updateLiveStockStatus => ");
                  console.log(updateLiveStockStatus);
                }
              }
            }
          } else {
            console.log("saya lalu bahagian lembu & unta");

            const getStockSelection = await model.getStockSelection(
              getTempahanDetails[i].implementerNegara,
              getTempahanDetails[i].tempahanIbadahTypeCode,
              1,
              getTempahanDetails[i].tempahanIbadahType,
              getTempahanDetails[i].tempahanDetailsHaiwanType,
              1
            );
            if (!getStockSelection) return;
            else {
              console.log("get Stock Selection => Resit Baru");
              console.log(getStockSelection);

              let liveStokBahagianIdArray = [];

              getStockSelection.forEach((value) => {
                for (let key in value) {
                  // console.log('key => ' + key + ' value => ' + value[key])
                  if (key === "liveStokBahagianId") {
                    liveStokBahagianIdArray.push(value[key]);
                  }
                }
              });

              console.log("liveStokBahagianIdArray => ");
              console.log(liveStokBahagianIdArray);

              // changes
              if (liveStokBahagianIdArray.length == 0) {
                rowError.push({
                  nama: getTempahanDetails[i].wakilPesertaName,
                  kawasan: getTempahanDetails[i].implementerKawasan,
                  negara: getTempahanDetails[i].implementerNegara,
                  jenis: getTempahanDetails[i].tempahanDetailsHaiwanType,
                  baris: row,
                });
                console.log("Maaf tiada stok untuk tempahan baris ke " + row);

                resitAmountPaid =
                  resitAmountPaid - getTempahanDetails[i].tempahanDetailsPrice;

                // console.log('rowError = ' + rowError)
                // console.log('resitAmountPaid = ' + resitAmountPaid)

                // UPDATE TEMPAHAN DETAILS TABLE
                const updateTempahanDetailsBasedOnTempahanDetailsId =
                  await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                    "Failed",
                    22,
                    getTempahanDetails[i].tempahanDetailsId
                  );
                if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                else {
                  console.log(
                    "updateTempahanDetailsBasedOnTempahanDetailsId => "
                  );
                  console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                }
              } else {
                // UPDATE TEMPAHAN DETAILS TABLE
                const updateTempahanDetailsBasedOnTempahanDetailsId =
                  await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                    "Pending Payment",
                    20,
                    getTempahanDetails[i].tempahanDetailsId
                  );
                if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                else {
                  console.log(
                    "updateTempahanDetailsBasedOnTempahanDetailsId => "
                  );
                  console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                }

                komisyenPerClosing += 1;
                console.log("komisyenPerClosing => " + komisyenPerClosing);

                const getCommissionByParts = await model.getCommissionByParts(
                  getTempahanDetails[i].implementerNegara,
                  getTempahanDetails[i].tempahanDetailsHaiwanType
                );
                if (!getCommissionByParts) return;
                else {
                  console.log("getCommissionByParts() => ");
                  console.log(getCommissionByParts);
                  komisyenCurrency = getCommissionByParts[0].CURRENCY;
                  komisyenPerBahagian += parseFloat(
                    getCommissionByParts[0].KOMISYEN
                  );
                  console.log("komisyenPerBahagian => " + komisyenPerBahagian);
                }
              }

              for (let v = 0; v < liveStokBahagianIdArray.length; v++) {
                const insertTempahanDetailsLiveStok =
                  await model.insertTempahanDetailsLiveStok(
                    getTempahanDetails[i].tempahanDetailsId,
                    getTempahan[0].tempahanId,
                    getStockSelection[v].implementerId,
                    getStockSelection[v].implementerLokasiId,
                    getTempahanDetails[i].wakilPesertaId,
                    getStockSelection[v].liveStokId,
                    liveStokBahagianIdArray[v]
                  );
                if (!insertTempahanDetailsLiveStok) return;
                console.log("insertTempahanDetailsLiveStok => ");
                console.log(insertTempahanDetailsLiveStok);

                const updateLiveStockBahagianStatus =
                  await model.updateLiveStockBahagianStatus(
                    "Pending Reserved",
                    null,
                    getStockSelection[v].liveStokId,
                    liveStokBahagianIdArray[v]
                  );
                if (!updateLiveStockBahagianStatus) return;
                console.log("updateLiveStockBahagianStatus => ");
                console.log(updateLiveStockBahagianStatus);

                const updateLiveStockStatus = await model.updateLiveStockStatus(
                  "Pending Reserved",
                  null,
                  getStockSelection[v].liveStokId
                );
                if (!updateLiveStockStatus) return;
                console.log("updateLiveStockStatus => ");
                console.log(updateLiveStockStatus);
              }
            }
          }
        }

        console.log("resitAmountPaid " + resitAmountPaid);

        if (resitAmountPaid == 0) {
          console.log("resitAmountPaid " + resitAmountPaid);

          // UPDATE TEMPAHAN TABLE
          const updateTempahan = await model.updateTempahan(
            "Failed",
            22,
            tempahanId,
            modalTarikhBayaran,
            modalTarikhMuatNaikResit
          );
          if (!updateTempahan) return;
          else {
            console.log("updateTempahan => ");
            console.log(updateTempahan);
          }

          //UPDATE TEMPAHAN TOTAL PRICE
          const updateTempahanTotalPrice = await model.updateTempahanTotalPrice(
            tempahanId,
            resitAmountPaid
          );
          if (!updateTempahanTotalPrice) return;
          else {
            console.log("updateTempahanTotalPrice => ");
            console.log(updateTempahanTotalPrice);
          }
        } else {
          //UPDATE TEMPAHAN TOTAL PRICE
          const updateTempahanTotalPrice = await model.updateTempahanTotalPrice(
            tempahanId,
            resitAmountPaid
          );
          if (!updateTempahanTotalPrice) return;
          else {
            console.log("updateTempahanTotalPrice => ");
            console.log(updateTempahanTotalPrice);
          }

          // INSERT NEW RESIT INTO RESIT TABLE
          const insertResit = await model.insertResit(
            tempahanId,
            getTempahan[0].userWakilId,
            resitAmountPaid + chargeFee,
            resitAmountPaid,
            resitPaymentMethod,
            modalNoTransaksi,
            20,
            req.file.filename
          );
          if (!insertResit) return;
          else {
            status = "Success";
            message = "New Resit has been inserted into database";
            data = insertResit;

            // GET EJEN ID AND COMMISSION
            const getEjenDetails2 = await model.getEjenDetails2(tempahanId);

            if (!getEjenDetails2) return;
            else {
              console.log("getEjenDetails2 => ");
              console.log(getEjenDetails2);

              userEjenId = getEjenDetails2[0].userEjenId;
              tempahanTotalPrice = getEjenDetails2[0].tempahanTotalPrice;
              ejenCommissionType = getEjenDetails2[0].userEjenCommissionType;
              ejenCommissionValue = getEjenDetails2[0].userEjenCommissionValue;
              ejenCommissionCategory =
                getEjenDetails2[0].userEjenCommissionCategory;

              console.log(
                "komisyenPerClosing FINALLL => " + komisyenPerClosing
              );

              ejenCommissionValue *= komisyenPerClosing;

              if (ejenCommissionCategory == "Per Closing") {
                // if ejen commission in ringgit
                if (ejenCommissionType == "RM") {
                  const insertEjenCommission = await model.insertEjenCommission(
                    userEjenId,
                    tempahanId,
                    ejenCommissionType,
                    ejenCommissionValue,
                    "Pending",
                    23
                  );

                  if (!insertEjenCommission) return;
                  else {
                    console.log("insertEjenCommission => ");
                    console.log(insertEjenCommission);
                  }
                }
                // if ejen commission in percentage
                else {
                  console.log(
                    "ejenCommissionValue = ejenCommissionValue * ejenCommissionValue = "
                  );
                  ejenCommissionValue =
                    ejenCommissionValue * tempahanTotalPrice;
                  console.log(ejenCommissionValue);

                  const insertEjenCommission = await model.insertEjenCommission(
                    userEjenId,
                    tempahanId,
                    ejenCommissionType,
                    ejenCommissionValue,
                    "Pending",
                    23
                  );

                  if (!insertEjenCommission) return;
                  else {
                    console.log("insertEjenCommission => ");
                    console.log(insertEjenCommission);
                  }
                }
              } else {
                const insertEjenCommission = await model.insertEjenCommission(
                  userEjenId,
                  tempahanId,
                  komisyenCurrency,
                  komisyenPerBahagian,
                  "Pending",
                  23
                );

                if (!insertEjenCommission) return;
                else {
                  console.log("insertEjenCommission => ");
                  console.log(insertEjenCommission);
                }
              }
            }
          }
        }
      } else {
        console.log("tempahanDetailsLiveStock already inserted");

        console.log("modalStatusPembayaran = " + modalStatusPembayaran);

        if (modalStatusPembayaran == "Success") {
          console.log(
            "update tempahan, tempahan details, livestok, livestok bahagian, resit, ejenCommission"
          );

          // UPDATE TEMPAHAN TABLE
          const updateTempahan = await model.updateTempahan(
            "Success",
            20,
            tempahanId,
            modalTarikhBayaran,
            modalTarikhMuatNaikResit,
            moment().format("YYYY-MM-DD HH:mm:ss")
          );
          if (!updateTempahan) return;
          else {
            console.log("updateTempahan => ");
            console.log(updateTempahan);
          }

          // UPDATE TEMPAHAN DETAILS TABLE
          for (let i = 0; i < getTempahanDetails.length; i++) {
            const updateTempahanDetailsBasedOnTempahanDetailsId =
              await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                "Success",
                20,
                getTempahanDetails[i].tempahanDetailsId
              );
            if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
            else {
              console.log("updateTempahanDetailsBasedOnTempahanDetailsId => ");
              console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
            }
          }

          for (let v = 0; v < getTempahanDetailsLiveStok.length; v++) {
            const updateLiveStockBahagianStatusPendingReserved =
              await model.updateLiveStockBahagianStatusPendingReserved(
                "Reserved",
                21,
                getTempahanDetailsLiveStok[v].liveStokId,
                getTempahanDetailsLiveStok[v].liveStokBahagianId
              );

            if (!updateLiveStockBahagianStatusPendingReserved) return;
            else {
              console.log("updateLiveStockBahagianStatusPendingReserved => ");
              console.log(updateLiveStockBahagianStatusPendingReserved);
            }

            const getLiveStokBahagianBaki = await model.getLiveStokBahagianBaki(
              getTempahanDetailsLiveStok[v].liveStokId
            );
            if (!getLiveStokBahagianBaki) return;
            else {
              console.log("getLiveStokBahagianBaki =>");
              console.log(getLiveStokBahagianBaki);
            }

            liveStokBilBahagian =
              getLiveStokBahagianBaki[0].liveStokBilBahagian;
            liveStokBilBahagianBaki =
              getLiveStokBahagianBaki[0].liveStokBilBahagianBaki;
            console.log("liveStokBilBahagian => " + liveStokBilBahagian);
            console.log(
              "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
            );

            BilBakiBaru = liveStokBilBahagianBaki - 1;
            console.log("BilBakiBaru => " + BilBakiBaru);

            if (BilBakiBaru > 0) {
              const updateLiveStock = await model.updateLiveStock(
                BilBakiBaru,
                "Partial",
                null,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            } else if (BilBakiBaru == 0) {
              const updateLiveStock = await model.updateLiveStock(
                BilBakiBaru,
                "Full",
                null,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            }

            // GET IMPLEMENTER LOKASI HAIWAN
            const getImplementerLokasiHaiwan =
              await model.getImplementerLokasiHaiwan(
                getTempahanDetailsLiveStok[v].implementerId,
                getTempahanDetailsLiveStok[v].liveStokType
              );
            if (!getImplementerLokasiHaiwan) return;
            else {
              console.log("getImplementerLokasiHaiwan() => ");
              console.log(getImplementerLokasiHaiwan);

              // 'implementerId','jumlahKeseluruhan','jumlahBaki','jumlahKeseluruhanBhgn','jumlahBakiBhgn'
              jumlahKeseluruhan =
                getImplementerLokasiHaiwan[0].jumlahKeseluruhan;
              jumlahBaki = getImplementerLokasiHaiwan[0].jumlahBaki;
              jumlahKeseluruhanBhgn =
                getImplementerLokasiHaiwan[0].jumlahKeseluruhanBhgn;
              jumlahBakiBhgn = getImplementerLokasiHaiwan[0].jumlahBakiBhgn;

              console.log("jumlahKeseluruhan => " + jumlahKeseluruhan);
              console.log("jumlahBaki => " + jumlahBaki);
              console.log("jumlahKeseluruhanBhgn => " + jumlahKeseluruhanBhgn);
              console.log("jumlahBakiBhgn => " + jumlahBakiBhgn);

              if (getTempahanDetailsLiveStok[v].liveStokType == "Kambing") {
                jumlahBaki = jumlahBaki - 1;
                jumlahBakiBhgn = jumlahBakiBhgn - 1;
                console.log("jumlahBaki selepas tolak = " + jumlahBaki);
                console.log("jumlahBakiBhgn selepas tolak = " + jumlahBakiBhgn);
              } else {
                jumlahBakiBhgn = jumlahBakiBhgn - 1;
                console.log("jumlahBakiBhgn selepas tolak = " + jumlahBakiBhgn);
                difference = parseInt(
                  (jumlahKeseluruhanBhgn - jumlahBakiBhgn) / 7
                );
                jumlahBaki = jumlahKeseluruhan - difference;
                console.log("difference = " + difference);
                console.log("jumlahBaki selepas tolak = " + jumlahBaki);
              }

              const updateImplementerLokasiHaiwan =
                await model.updateImplementerLokasiHaiwan(
                  jumlahBaki,
                  jumlahBakiBhgn,
                  getTempahanDetailsLiveStok[v].implementerId,
                  getTempahanDetailsLiveStok[v].liveStokType
                );
              if (!updateImplementerLokasiHaiwan) return;
              else {
                console.log("updateImplementerLokasiHaiwan() => ");
                console.log(updateImplementerLokasiHaiwan);
              }
            }
          }

          // UPDATE RESIT TABLE
          const updateResit = await model.updateResit(tempahanId, "Success");
          if (!updateResit) return;
          else {
            console.log("updateResit => ");
            console.log(updateResit);
          }

          // UPDATE EJEN COMMISSION TABLE
          const updateEjenCommission = await model.updateEjenCommission(
            "Success",
            20,
            tempahanId
          );
          if (!updateEjenCommission) return;
          else {
            console.log("updateEjenCommission => ");
            console.log(updateEjenCommission);
          }
        } else if (modalStatusPembayaran == "Failed") {
          console.log(
            "update tempahan, tempahan details, livestok activekan, livestok bahagian activekan, resit failed, ejenCommission failed"
          );

          // UPDATE TEMPAHAN TABLE
          const updateTempahan = await model.updateTempahan(
            "Failed",
            20,
            tempahanId,
            modalTarikhBayaran,
            modalTarikhMuatNaikResit,
            moment().format("YYYY-MM-DD HH:mm:ss")
          );
          if (!updateTempahan) return;
          else {
            console.log("updateTempahan => ");
            console.log(updateTempahan);
          }

          // UPDATE TEMPAHAN DETAILS TABLE
          for (let i = 0; i < getTempahanDetails.length; i++) {
            row += 1;
            console.log("=============== i => " + i + " ===============");
            console.log("=============== row => " + row + " ===============");
            if (getTempahanDetails[i].tempahanDetailsBhgnType == "Ekor") {
              if (
                getTempahanDetails[i].tempahanDetailsHaiwanType == "Kambing"
              ) {
                //CUBA TEST FAIL KAMBING
                console.log("Saya lalu ekor + kambing");

                // UPDATE TEMPAHAN DETAILS TABLE
                const updateTempahanDetailsBasedOnTempahanDetailsId =
                  await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                    "Failed",
                    20,
                    getTempahanDetails[i].tempahanDetailsId
                  );
                if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                else {
                  console.log(
                    "updateTempahanDetailsBasedOnTempahanDetailsId => "
                  );
                  console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                }
              } else {
                console.log("Saya lalu ekor + bukan kambing");

                // UPDATE TEMPAHAN DETAILS TABLE
                const updateTempahanDetailsBasedOnTempahanDetailsId =
                  await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                    "Failed",
                    21,
                    getTempahanDetails[i].tempahanDetailsId
                  );
                if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
                else {
                  console.log(
                    "updateTempahanDetailsBasedOnTempahanDetailsId => "
                  );
                  console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
                }
              }
            } else {
              console.log("saya lalu bahagian lembu & unta");

              // UPDATE TEMPAHAN DETAILS TABLE
              const updateTempahanDetailsBasedOnTempahanDetailsId =
                await model.updateTempahanDetailsBasedOnTempahanDetailsId(
                  "Failed",
                  21,
                  getTempahanDetails[i].tempahanDetailsId
                );
              if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
              else {
                console.log(
                  "updateTempahanDetailsBasedOnTempahanDetailsId => "
                );
                console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
              }
            }
          }

          for (let v = 0; v < getTempahanDetailsLiveStok.length; v++) {
            const updateLiveStockBahagianStatusPendingReserved =
              await model.updateLiveStockBahagianStatusPendingReserved(
                "Active",
                20,
                getTempahanDetailsLiveStok[v].liveStokId,
                getTempahanDetailsLiveStok[v].liveStokBahagianId
              );
            if (!updateLiveStockBahagianStatusPendingReserved) return;
            console.log("updateLiveStockBahagianStatusPendingReserved => ");
            console.log(updateLiveStockBahagianStatusPendingReserved);

            const getLiveStokBahagianBaki = await model.getLiveStokBahagianBaki(
              getTempahanDetailsLiveStok[v].liveStokId
            );
            if (!getLiveStokBahagianBaki) return;
            else {
              console.log("getLiveStokBahagianBaki =>");
              console.log(getLiveStokBahagianBaki);
            }

            liveStokBilBahagian =
              getLiveStokBahagianBaki[0].liveStokBilBahagian;
            liveStokBilBahagianBaki =
              getLiveStokBahagianBaki[0].liveStokBilBahagianBaki;
            console.log("liveStokBilBahagian => " + liveStokBilBahagian);
            console.log(
              "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
            );

            BilBakiBaru = liveStokBilBahagianBaki;
            console.log("BilBakiBaru => " + BilBakiBaru);

            if (liveStokBilBahagian == liveStokBilBahagianBaki) {
              const updateLiveStock = await model.updateLiveStock(
                BilBakiBaru,
                "Active",
                20,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            } else if (liveStokBilBahagianBaki < liveStokBilBahagian) {
              const updateLiveStock = await model.updateLiveStock(
                BilBakiBaru,
                "Partial",
                20,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            }
          }

          // UPDATE RESIT TABLE
          const updateResit = await model.updateResit(tempahanId, "Failed");
          if (!updateResit) return;
          else {
            console.log("updateResit => ");
            console.log(updateResit);
          }

          // UPDATE EJEN COMMISSION TABLE
          const updateEjenCommission = await model.updateEjenCommission(
            "Failed",
            21,
            tempahanId
          );
          if (!updateEjenCommission) return;
          else {
            data2 = updateEjenCommission;
            console.log("updateEjenCommission => ");
            console.log(updateEjenCommission);
          }
        }

        // SEND SMS & WHATSAPP
        const sendWhatsapp = await model.sendWhatsapp(
          modalNamaWakil,
          modalTelefonWakil,
          modalStatusPembayaran,
          modalNoTempahan,
          getResitDetails,
          getRingkasanTempahan
        );
        if (!sendWhatsapp) return;
        else {
          console.log("sendWhatsapp => ");
          console.log(sendWhatsapp);
        }
      }
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  result = {
    environment: process.env.ENVIRONMENT,
    status: status,
    message: message,
    data: data,
    data2: data2,
    data3: rowError,
  };

  res.status(200).json(result);
});

router.post("/batalTempahan", async (req, res) => {
  let param = null;
  let result = null;
  let status = null;
  let message = null;
  let data = null;
  let data2 = null;
  let data3 = null;
  let data4 = null;

  let tempahanId = null;
  let tempahanStatus = null;
  let userWakilId = null;
  let resitAmountPaid = null;
  let resitPaymentMethod = null;
  let tempahanNumber = null;
  let namaWakil = null;
  let emelWakil = null;
  let noTelefonWakil = null;
  let paymentMethod = null;
  let selectedBank = null;
  let tempahanInfo = null;
  let tempahanDetailsInfo = null;
  let maklumatTempahan = null;

  let userEjenId = null;
  let ejenCommissionType = null;
  let ejenCommissionValue = null;
  let ejenCommissionCategory = null;
  let tempahanTotalPrice = null;

  let rowError = [];
  let komisyenPerBahagian = 0;
  let komisyenPerClosing = 0;
  let komisyenCurrency = null;
  let chargeFee = 0;

  let modalNoTempahan = null;
  let modalNamaWakil = null;
  let modalKaedahPembayaran = null;
  let modalTelefonWakil = null;
  let modalNoTransaksi = null;
  let modalEmailWakil = null;
  let modalStatusPembayaran = null;
  let modalTarikhTempahan = null;
  let modalStatusTempahan = null;
  let modalTarikhMuatNaikResit = null;
  let modalTarikhBayaran = null;

  let liveStokBilBahagianBaki = null;
  let liveStokBilBahagian = null;
  let BilBakiBaru = null;
  let row = 0;
  let jumlahBaki = 0;

  let tempahanRefundValue = null;
  let refundId = null;

  console.log("req.body");
  console.log(req.body);

  try {
    param = req.body;

    tempahanId = param.tempahanId;

    if (tempahanId == null || tempahanId == "") {
      status = "Failed";
      message = "tempahanId cannot be empty";
    } else {
      //  Decrypt
      var reb64 = CryptoJS.enc.Hex.parse(tempahanId.toString());
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, "R@iden28");
      var tempahanIdDecrypted = decrypt.toString(CryptoJS.enc.Utf8);

      tempahanId = tempahanIdDecrypted;

      //  CHECK IF ALREADY INSERT INTO TEMPAHAN DETAILS LIVE STOCK
      const checkIfAlreadyInsertIntoTempahanDetailsLiveStock =
        await model.checkIfAlreadyInsertIntoTempahanDetailsLiveStock(
          tempahanId
        );
      if (!checkIfAlreadyInsertIntoTempahanDetailsLiveStock) return;
      else {
        console.log("checkIfAlreadyInsertIntoTempahanDetailsLiveStock => ");
        console.log(checkIfAlreadyInsertIntoTempahanDetailsLiveStock);
      }

      // GET TEMPAHAN DATA
      const getTempahan = await model.getTempahan(tempahanId);
      if (!getTempahan) return;
      else {
        console.log("getTempahan => ");
        console.log(getTempahan);
        tempahanTotalPrice = getTempahan[0].tempahanTotalPrice;
        tempahanStatus = getTempahan[0].tempahanStatus;
      }

      // GET TEMPAHAN DETAILS DATA
      const getTempahanDetails = await model.getTempahanDetails(tempahanId);
      if (!getTempahanDetails) return;
      else {
        console.log("getTempahanDetails => ");
        console.log(getTempahanDetails);
      }

      // GET TEMPAHAN DETAILS LIVESTOK DATA
      const getTempahanDetailsLiveStok = await model.getTempahanDetailsLiveStok(
        tempahanId
      );
      if (!getTempahanDetailsLiveStok) return;
      else {
        console.log("getTempahanDetailsLiveStok => ");
        console.log(getTempahanDetailsLiveStok);
      }

      // GET TEMPAHAN DETAILS LIVESTOK DATA
      const getResitDetails = await model.getResitDetails(tempahanId);
      if (!getResitDetails) return;
      else {
        console.log("getResitDetails => ");
        console.log(getResitDetails);
        if (getResitDetails.length > 0)
          resitAmountPaid = getResitDetails[0].resitAmountPaid;
      }

      // GET TEMPAHAN DETAILS LIVESTOK DATA
      const getRingkasanTempahan = await model.getRingkasanTempahan(tempahanId);
      if (!getRingkasanTempahan) return;
      else {
        console.log("getRingkasanTempahan => ");
        console.log(getRingkasanTempahan);
      }

      // check If Already Insert Into TempahanDetailsLiveStock
      if (checkIfAlreadyInsertIntoTempahanDetailsLiveStock[0].count == 0) {
        console.log("tempahanDetailsLiveStok tiada data");

        // UPDATE TEMPAHAN TABLE
        const updateCancelTempahan = await model.updateCancelTempahan(
          "Cancel Order",
          21,
          tempahanId,
          null
        );
        if (!updateCancelTempahan) return;
        else {
          console.log("updateCancelTempahan => ");
          console.log(updateCancelTempahan);
          data = updateCancelTempahan;
        }

        // UPDATE TEMPAHAN DETAILS TABLE
        for (let i = 0; i < getTempahanDetails.length; i++) {
          const updateTempahanDetailsBasedOnTempahanDetailsId =
            await model.updateTempahanDetailsBasedOnTempahanDetailsId(
              "Cancel Order",
              21,
              getTempahanDetails[i].tempahanDetailsId
            );
          if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
          else {
            console.log("updateTempahanDetailsBasedOnTempahanDetailsId => ");
            console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
            data2 = updateTempahanDetailsBasedOnTempahanDetailsId;
          }
        }
      } else {
        console.log("tempahanDetailsLiveStock already inserted");

        console.log(
          "update tempahan, tempahan details, livestok activekan, livestok bahagian activekan, resit failed, ejenCommission failed"
        );

        if (tempahanStatus == "Success") {
          // UPDATE TEMPAHAN TABLE
          const updateCancelTempahan = await model.updateCancelTempahan(
            "Cancel Order",
            21,
            tempahanId,
            resitAmountPaid
          );
          if (!updateCancelTempahan) return;
          else {
            console.log("updateCancelTempahan => ");
            console.log(updateCancelTempahan);
            data = updateCancelTempahan;
          }

          // INSERT REFUND TABLE
          // const insertRefund = await model.insertRefund(tempahanId,resitAmountPaid,null,null,'Success');
          // if (!insertRefund) return;
          // else {
          //   console.log("insertRefund => ")
          //   console.log(insertRefund)

          //   refundId = insertRefund
          // }
        } else {
          // UPDATE TEMPAHAN TABLE
          const updateCancelTempahan = await model.updateCancelTempahan(
            "Cancel Order",
            21,
            tempahanId,
            null
          );
          if (!updateCancelTempahan) return;
          else {
            console.log("updateCancelTempahan => ");
            console.log(updateCancelTempahan);
            data = updateCancelTempahan;
          }
        }

        // UPDATE TEMPAHAN DETAILS TABLE
        for (let i = 0; i < getTempahanDetails.length; i++) {
          const updateTempahanDetailsBasedOnTempahanDetailsId =
            await model.updateTempahanDetailsBasedOnTempahanDetailsId(
              "Cancel Order",
              21,
              getTempahanDetails[i].tempahanDetailsId
            );
          if (!updateTempahanDetailsBasedOnTempahanDetailsId) return;
          else {
            console.log("updateTempahanDetailsBasedOnTempahanDetailsId => ");
            console.log(updateTempahanDetailsBasedOnTempahanDetailsId);
            data2 = updateTempahanDetailsBasedOnTempahanDetailsId;
          }

          // INSERT REFUND DETAILS TABLE
          // const insertRefundDetails = await model.insertRefundDetails(refundId,getTempahanDetails[i].tempahanDetailsId,getTempahanDetails[i].tempahanDetailsPrice,'Success');
          // if (!insertRefundDetails) return;
          // else {
          //   console.log("insertRefundDetails => ")
          //   console.log(insertRefundDetails)
          // }
        }

        // UPDATE TEMPAHAN DETAILS LIVESTOK TABLE
        if (tempahanStatus == "Success") {
          console.log("yes the tempahan status is success ");
          for (let v = 0; v < getTempahanDetailsLiveStok.length; v++) {
            // ACTIVE RESERVED STOCK TO ACTIVE
            const updateLiveStockBahagianStatusReserved =
              await model.updateLiveStockBahagianStatusReserved(
                "Active",
                20,
                getTempahanDetailsLiveStok[v].liveStokId,
                getTempahanDetailsLiveStok[v].liveStokBahagianId
              );
            if (!updateLiveStockBahagianStatusReserved) return;
            console.log("updateLiveStockBahagianStatusReserved => ");
            console.log(updateLiveStockBahagianStatusReserved);

            // GET LIVE STOK BAHAGIAN CURRENT BAKI
            const getLiveStokBahagianBaki = await model.getLiveStokBahagianBaki(
              getTempahanDetailsLiveStok[v].liveStokId
            );
            if (!getLiveStokBahagianBaki) return;
            else {
              console.log("getLiveStokBahagianBaki =>");
              console.log(getLiveStokBahagianBaki);
            }

            liveStokBilBahagian =
              getLiveStokBahagianBaki[0].liveStokBilBahagian;
            liveStokBilBahagianBaki =
              getLiveStokBahagianBaki[0].liveStokBilBahagianBaki;

            console.log("liveStokBilBahagian => " + liveStokBilBahagian);
            console.log(
              "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
            );
            console.log(
              "liveStokId => " + getTempahanDetailsLiveStok[v].liveStokId
            );

            liveStokBilBahagianBaki += 1;

            if (liveStokBilBahagian == liveStokBilBahagianBaki) {
              const updateLiveStock = await model.updateLiveStock(
                liveStokBilBahagianBaki,
                "Active",
                20,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            } else if (liveStokBilBahagianBaki < liveStokBilBahagian) {
              const updateLiveStock = await model.updateLiveStock(
                liveStokBilBahagianBaki,
                "Partial",
                20,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            }

            // GET IMPLEMENTER LOKASI HAIWAN
            const getImplementerLokasiHaiwan =
              await model.getImplementerLokasiHaiwan(
                getTempahanDetailsLiveStok[v].implementerId,
                getTempahanDetailsLiveStok[v].liveStokType
              );
            if (!getImplementerLokasiHaiwan) return;
            else {
              console.log("getImplementerLokasiHaiwan() => ");
              console.log(getImplementerLokasiHaiwan);

              // 'implementerId','jumlahKeseluruhan','jumlahBaki','jumlahKeseluruhanBhgn','jumlahBakiBhgn'
              jumlahKeseluruhan =
                getImplementerLokasiHaiwan[0].jumlahKeseluruhan;
              jumlahBaki = getImplementerLokasiHaiwan[0].jumlahBaki;
              jumlahKeseluruhanBhgn =
                getImplementerLokasiHaiwan[0].jumlahKeseluruhanBhgn;
              jumlahBakiBhgn = getImplementerLokasiHaiwan[0].jumlahBakiBhgn;

              console.log("jumlahKeseluruhan => " + jumlahKeseluruhan);
              console.log("jumlahBaki => " + jumlahBaki);
              console.log("jumlahKeseluruhanBhgn => " + jumlahKeseluruhanBhgn);
              console.log("jumlahBakiBhgn => " + jumlahBakiBhgn);

              if (getTempahanDetailsLiveStok[v].liveStokType == "Kambing") {
                jumlahBaki = jumlahBaki + 1;
                jumlahBakiBhgn = jumlahBakiBhgn + 1;
                console.log("jumlahBaki selepas tolak = " + jumlahBaki);
                console.log("jumlahBakiBhgn selepas tolak = " + jumlahBakiBhgn);
              } else {
                jumlahBakiBhgn = jumlahBakiBhgn + 1;
                console.log("jumlahBakiBhgn selepas tolak = " + jumlahBakiBhgn);
                difference = parseInt(
                  (jumlahKeseluruhanBhgn - jumlahBakiBhgn) / 7
                );
                jumlahBaki = jumlahKeseluruhan - difference;
                console.log("difference = " + difference);
                console.log("jumlahBaki selepas tolak = " + jumlahBaki);
              }

              const updateImplementerLokasiHaiwan =
                await model.updateImplementerLokasiHaiwan(
                  jumlahBaki,
                  jumlahBakiBhgn,
                  getTempahanDetailsLiveStok[v].implementerId,
                  getTempahanDetailsLiveStok[v].liveStokType
                );
              if (!updateImplementerLokasiHaiwan) return;
              else {
                console.log("updateImplementerLokasiHaiwan() => ");
                console.log(updateImplementerLokasiHaiwan);
              }
            }
          }
        } else {
          for (let v = 0; v < getTempahanDetailsLiveStok.length; v++) {
            const updateLiveStockBahagianStatusPendingReserved =
              await model.updateLiveStockBahagianStatusPendingReserved(
                "Active",
                20,
                getTempahanDetailsLiveStok[v].liveStokId,
                getTempahanDetailsLiveStok[v].liveStokBahagianId
              );
            if (!updateLiveStockBahagianStatusPendingReserved) return;
            console.log("updateLiveStockBahagianStatusPendingReserved => ");
            console.log(updateLiveStockBahagianStatusPendingReserved);

            const getLiveStokBahagianBaki = await model.getLiveStokBahagianBaki(
              getTempahanDetailsLiveStok[v].liveStokId
            );
            if (!getLiveStokBahagianBaki) return;
            else {
              console.log("getLiveStokBahagianBaki =>");
              console.log(getLiveStokBahagianBaki);
            }

            liveStokBilBahagian =
              getLiveStokBahagianBaki[0].liveStokBilBahagian;
            liveStokBilBahagianBaki =
              getLiveStokBahagianBaki[0].liveStokBilBahagianBaki;

            console.log("liveStokBilBahagian => " + liveStokBilBahagian);
            console.log(
              "liveStokBilBahagianBaki => " + liveStokBilBahagianBaki
            );
            console.log(
              "liveStokId => " + getTempahanDetailsLiveStok[v].liveStokId
            );

            if (liveStokBilBahagian == liveStokBilBahagianBaki) {
              const updateLiveStock = await model.updateLiveStock(
                liveStokBilBahagianBaki,
                "Active",
                20,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            } else if (liveStokBilBahagianBaki < liveStokBilBahagian) {
              const updateLiveStock = await model.updateLiveStock(
                liveStokBilBahagianBaki,
                "Partial",
                20,
                getTempahanDetailsLiveStok[v].liveStokId
              );
              if (!updateLiveStock) return;
              data2 = updateLiveStock;
              console.log("updateLiveStock => ");
              console.log(updateLiveStock);
            }
          }
        }

        // UPDATE RESIT TABLE
        const updateResit = await model.updateResit(tempahanId, "Failed");
        if (!updateResit) return;
        else {
          console.log("updateResit => ");
          console.log(updateResit);
          data3 = updateResit;
        }

        // UPDATE EJEN COMMISSION TABLE
        const updateEjenCommission = await model.updateEjenCommission(
          "Failed",
          21,
          tempahanId
        );
        if (!updateEjenCommission) return;
        else {
          data2 = updateEjenCommission;
          console.log("updateEjenCommission => ");
          console.log(updateEjenCommission);
          data4 = updateEjenCommission;
          status = "Berjaya";
          message = "Anda berjaya batalkan tempahan";
        }
      }
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  result = {
    environment: process.env.ENVIRONMENT,
    status: status,
    message: message,
    data: data,
    data2: data2,
    data3: data3,
    data4: data4,
  };

  res.status(200).json(result);
});

router.post("/update-penama", async (req, res) => {
  try {
    if (!req.body.penamaOld || !req.body.penamaNew)
      return res.status(400).json({
        status: false,
        message: "penama is empty",
      });

    const hasNullEmptyValue = req.body.penamaNew.some((data) => {
      return data.WAKIL === null || data.WAKIL.trim() === "";
    });

    if (hasNullEmptyValue)
      return res.status(400).json({
        status: false,
        message: "Some penama is empty",
      });

    // update penama
    let updatePenama = await model.updatePenamaList(
      req.body.penamaOld,
      req.body.penamaNew
    );

    if (!updatePenama.status)
      return res.status(400).json({
        status: false,
        message: updatePenama,
      });

    return res.status(200).json({
      status: true,
      message: "Penama list updated.",
    });
  } catch (error) {
    return res.status(500).json({
      status: false,
      message: "API error: " + error,
    });
  }
});

module.exports = router;
