const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/test/training.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
 

  let user_id = null;
  let user_name = null;
  let user_password = null;
  let user_email = null;
  let user_tel = null;
  let user_age = null;
  let user_dob = null;
  let user_status = null;
  let user_address = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.body;

    user_id=param.user_id;
    user_name=param.user_name;
    user_password= param.user_password;
    user_email= param.user_email;
    user_tel= param.user_tel;
    user_age= param.user_age;
    user_dob=param.user_dob;
    user_status=param.user_status;
    user_address=param.user_address;
    



     
      // INSERT USER EJEN

      let insertTesting = await model.insertUser(
        user_id,
        user_name,
        user_password,
        user_email,
        user_tel,
        user_age,
        user_dob,
        user_status,
        user_address
      )

      if (insertTesting != false ) {
        result = {
          status: "berjaya",
          message: "Anda berjaya masukkan User Testing.",
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal masukkan User Testing'
        }
      }
      
    
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
