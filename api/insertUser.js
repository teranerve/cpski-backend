const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../function/user.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let username = null;
  let password = null;
  let fullname = null;
  let type = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    username = param.username;
    password = param.password;
    fullname = param.fullname;
    type = param.type;

    // GET USER FUNCTION
    const insertUser = await model.insertUser(username,fullname,password,type);

    if (insertUser != false) {
      result = {
        message: "anda berjaya masukkan pengguna",
        environment: process.env.ENVIRONMENT,
        userdata: insertUser,
        test: insertUser,
      };
    } else {
      result = {
          message: 'User Insert fail'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
