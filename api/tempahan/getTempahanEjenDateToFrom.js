const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    userId = param.userId;
    let from_date = param.from_date
    let to_date = param.to_date

    console.log('+++MASUK SINI+++')
    console.log(req.query)
    console.log(param.from_date)
    console.log(param.to_date)

      let getTempahanEjenDateToFrom = await model.getTempahanEjenDateToFrom(userId,from_date,to_date);

      if (getTempahanEjenDateToFrom[0] != false || getTempahanEjenDateToFrom[1] != false || getTempahanEjenDateToFrom[2] != false || getTempahanEjenDateToFrom[3] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai tempahan oleh ejen.",
          data: getTempahanEjenDateToFrom[0],
          data1: getTempahanEjenDateToFrom[1],
          data2: getTempahanEjenDateToFrom[2],
          data3: getTempahanEjenDateToFrom[3],
          data4: param.from_date
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai tempahan oleh ejen.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;