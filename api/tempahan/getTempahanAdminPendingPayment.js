const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let year = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;
    year = param.Id;

    let getTempahanAdminPendingPayment = await model.getTempahanAdminPendingPayment(year);

    if (
      getTempahanAdminPendingPayment[0] != false ||
      getTempahanAdminPendingPayment[1] != false ||
      getTempahanAdminPendingPayment[2] != false
    ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai tempahan oleh admin.",
        data: getTempahanAdminPendingPayment[0],
        data2: getTempahanAdminPendingPayment[1],
        data3: getTempahanAdminPendingPayment[2],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai tempahan oleh admin.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
