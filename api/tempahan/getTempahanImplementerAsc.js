const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let implementerId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerId = param.implementerId;

        let getTempahanImplementerAsc = await model.getTempahanImplementerAsc(implementerId);

        if (getTempahanImplementerAsc[0] != false || getTempahanImplementerAsc[1] != false || getTempahanImplementerAsc[2] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh implementer.",
                data: getTempahanImplementerAsc[0],
                data1: getTempahanImplementerAsc[1],
                data2: getTempahanImplementerAsc[2],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh implementer.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;