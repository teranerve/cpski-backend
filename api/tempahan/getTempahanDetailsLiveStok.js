const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
  let result = null;
  let tempahanDetailsId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    tempahanDetailsId = param.tempahanDetailsId;

      let getTempahanDetailsLiveStok = await model.getTempahanDetailsLiveStok(tempahanDetailsId);

      if (getTempahanDetailsLiveStok != false ) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai inventori bahagian oleh admin.",
          data: getTempahanDetailsLiveStok,
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai inventori bahagian oleh admin.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;