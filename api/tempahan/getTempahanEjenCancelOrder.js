const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    userId = param.userId;

      let getTempahanEjenCancelOrder = await model.getTempahanEjenCancelOrder(userId);

      if (getTempahanEjenCancelOrder[0] != false || getTempahanEjenCancelOrder[1] != false || getTempahanEjenCancelOrder[2] != false || getTempahanEjenCancelOrder[3] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai tempahan oleh ejen.",
          data: getTempahanEjenCancelOrder[0],
          data1: getTempahanEjenCancelOrder[1],
          data2: getTempahanEjenCancelOrder[2],
          data3: getTempahanEjenCancelOrder[3],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai tempahan oleh ejen.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;