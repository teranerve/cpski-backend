const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let implementerId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerId = param.implementerId;

        let getTempahanImplementerCancelOrder = await model.getTempahanImplementerCancelOrder(implementerId);

        if (getTempahanImplementerCancelOrder[0] != false || getTempahanImplementerCancelOrder[1] != false || getTempahanImplementerCancelOrder[2] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh implementer.",
                data: getTempahanImplementerCancelOrder[0],
                data1: getTempahanImplementerCancelOrder[1],
                data2: getTempahanImplementerCancelOrder[2],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh implementer.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;