const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let tempahanDetailsId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        tempahanDetailsId = param.tempahanDetailsId;

        let getButiranTempahanImplementer = await model.getButiranTempahanImplementer(tempahanDetailsId);

        if (getButiranTempahanImplementer != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan butiran tempahan oleh implementer.",
                tempahanDetails: getButiranTempahanImplementer[0],
                tempahan: getButiranTempahanImplementer[1],
                resit: getButiranTempahanImplementer[2],
                userWakil: getButiranTempahanImplementer[3],
                wakilPeserta: getButiranTempahanImplementer[4],
                liveStok: getButiranTempahanImplementer[5],
                liveStokBahagian: getButiranTempahanImplementer[6],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan butiran tempahan oleh implementer.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;