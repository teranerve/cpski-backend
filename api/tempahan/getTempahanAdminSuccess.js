const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;

    let getTempahanAdminSuccess = await model.getTempahanAdminSuccess(
      req.query.year
    );

    if (
      getTempahanAdminSuccess[0] != false ||
      getTempahanAdminSuccess[1] != false ||
      getTempahanAdminSuccess[2] != false
    ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai tempahan oleh admin.",
        data: getTempahanAdminSuccess[0],
        data2: getTempahanAdminSuccess[1][0],
        data3: getTempahanAdminSuccess[2],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai tempahan oleh admin.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
