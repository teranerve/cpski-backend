const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
  let result = null;
  

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    let from_date = param.from_date
    let to_date = param.to_date
    // param = req.body;
    console.log('+++MASUK SINI+++')
    console.log(req)
    console.log(param.from_date)
    console.log(param.to_date)

      let getTempahanAdminDateToFrom = await model.getTempahanAdminDateToFrom(from_date, to_date);

      if (getTempahanAdminDateToFrom[0] != false || getTempahanAdminDateToFrom[1] != false|| getTempahanAdminDateToFrom[2] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai tempahan oleh admin.",
          data: getTempahanAdminDateToFrom[0][0],
          data2: getTempahanAdminDateToFrom[1],
          data3: getTempahanAdminDateToFrom[2],
          data4: param.from_date 
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai tempahan oleh admin.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;