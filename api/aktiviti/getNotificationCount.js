const express = require("express");
const router = express.Router();
const model = require("../../function/aktiviti/aktiviti.js");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let aktivitiDateLivestokId = null;

  console.log(req.query)

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    aktivitiDateLivestokId = param.aktivitiDateLivestokId;

      let getData = await model.getNotificationCount(aktivitiDateLivestokId);

      console.log('--- getNotificationCount ---')
      console.log(getData[0])

      if (getData != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan data",
          data: getData[0]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan data',
          data: null,
          data2: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

router.get("/success-notification", async (req, res) => {
  let param = null;
  let result = null;

  console.log(req.query)

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;

      let getData = await model.getSuccessNotification();

      console.log('--- getSuccessNotification ---')
      console.log(getData[0])

      if (getData != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan data notifikasi - OK",
          data: getData[0]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan data notifikasi - OK',
          data: null
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

router.get("/failed-notification", async (req, res) => {
  let param = null;
  let result = null;

  console.log(req.query)

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;

      let getData = await model.getFailedNotification();

      console.log('--- getFailedNotification ---')
      console.log(getData[0])

      if (getData != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan data notifikasi - TIADA PILIHAN",
          data: getData[0]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan data notifikasi - TIADA PILIHAN',
          data: null
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

router.post("/resend-notification", async (req, res) => {
  let param = null;
  let result = null;
  let notificationType = null, 
  notificationReceiver = null, 
  notificationContent = null,
  notificationId = null

  console.log(req.body)

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.body;

    notificationId = param.notificationId
    notificationType = param.notificationType
    notificationReceiver = param.notificationReceiver
    notificationContent = param.notificationContent

      let resendData = await model.resendNotification(notificationType, notificationReceiver, notificationContent);

      console.log('--- resendNotification ---')
      console.log(resendData.resultWhatsapp)

      if (resendData.resultWhatsapp === 'OK' || resendData.resultSMS === 'OK') {
        let updateData = await model.updateNotificationStatus(notificationId);
      } else {
        console.log('--- ERROR ---')
        console.log('--- NOTIFICATION STATUS IS NOT UPDATED ---')
      }

      if (resendData != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya hantar notifikasi",
          data: resendData
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal hantar notifikasi',
          data: null
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;