const express = require("express");
const router = express.Router();
const model = require("../../function/aktiviti/aktiviti.js");

router.post("/", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null,
    aktivitiDateId = null,
    liveStokId = null,
    tempahanDetailsId = null,
    liveStokBahagianId = null,
    implementerLokasiId = null,
    aktivitiDateLivestokId = null,
    SN = null,
    insertData = null,
    getBulkWakil = null,
    nomborHaiwanArr = [],
    ekorPeserta = [];
  (wakilArr = []),
    (tempahanDetailsIdArr = []),
    (currentAktivitiStatus = null),
    (tarikhPelaksanaan = null);

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;
    implementerId = param.implementerId;
    implementerLokasiId = param.implementerLokasiId;
    aktivitiDateId = param.aktivitiDateId;
    liveStokId = param.liveStokId;
    tempahanDetailsId = param.tempahanDetailsId;
    aktivitiDateLivestokId = param.aktivitiDateLivestokId;
    SN = param.HaiwanSN;
    currentAktivitiStatus = param.currentAktiviti;
    tarikhPelaksanaan = param.tarikhPelaksanaan;

    console.log("--- GET AKTIVITI-DATE PROCESS START ---");
    let getAktivitiDate = await model.getAktivitiDate(aktivitiDateId);

    console.log("--- getBulkStock PROCESS START ---");
    let getBulkStock = await model.getBulkStock(liveStokId);

    console.log("--- BULK STOCK PROCESS START ---");

    if (getBulkStock[0].tempahanDetailsBhgnType === "Ekor") {
      console.log(`MASUK EKOR SIDE`);
      console.log("--- UPDATE AKTIVITI-DATE PROCESS START ---");
      let updateStatus = await model.updateAktivitiDateLivestokStatus(
        tempahanDetailsId
      );

      console.log("--- insertAktivitiDateLivestok PROCESS START ---");
      insertData = await model.insertAktivitiDateLivestok(
        aktivitiDateId,
        liveStokId,
        tempahanDetailsId,
        getAktivitiDate[0][0].setupKorbanAktivitiId,
        implementerId,
        implementerLokasiId,
        "ACTIVE"
      );

      let getDataTempahan = await model.getAktivitiDateLivestok(
        insertData,
        "get-tempahanDetailsId"
      );

      for (let i = 0; i < getBulkStock.length; i++) {
        console.log("--- updateTempahanDetailsLiveStok PROCESS START ---");
        let updateTempahanDetailsLiveStok =
          await model.updateTempahanDetailsLiveStok(
            getBulkStock[i].tempahanId,
            getBulkStock[i].liveStokId,
            liveStokBahagianId,
            insertData,
            getAktivitiDate[0][0].setupKorbanAktivitiId,
            aktivitiDateId,
            tempahanDetailsId
          );
      }

      let getBulkStockEach = await model.getBulkStockEach(liveStokId);
      for (let i = 0; i < getBulkStockEach.length; i++) {
        console.log("--- getBulkWakil PROCESS START ---");
        getBulkWakil = await model.getBulkWakil(tempahanDetailsId);

        nomborHaiwanArr.push(
          getBulkWakil[i].wakilPesertaName +
            ", " +
            getBulkWakil[i].liveStokBahagianNo.replace(/K22-|A22-/g, "") +
            ", " +
            getBulkWakil[i].liveStokIbadah +
            ", " +
            getBulkWakil[i].tempahanDetailsHaiwanType +
            ", " +
            getBulkWakil[i].implementerNegara +
            ` (Cadangan Pelaksanaan Korban: ` +
            tarikhPelaksanaan +
            `)`
        );

        ekorPeserta.push(getBulkWakil[i].wakilPesertaName);
      }

      //   console.log('--- getBulkWakil PROCESS START ---')
      //   let getBulkWakil = await model.getBulkWakil(
      //     tempahanDetailsId,
      //   )
      //   console.log(getBulkWakil[0].liveStokBahagianNo)

      console.log("--- sendNotificationPindahStok PROCESS START ---");
      let sendNotifikasi = await model.sendNotificationPindahStok(
        "Ekor",
        getBulkWakil[0].userEmail,
        getBulkWakil[0].userFullname,
        getBulkWakil[0].userPhoneNo,
        nomborHaiwanArr,
        getBulkWakil[0].tempahanNo,
        getBulkWakil[0].tempahanDetailsBhgnType,
        getBulkWakil[0].tempahanDetailsHaiwanType,
        currentAktivitiStatus,
        tarikhPelaksanaan,
        ekorPeserta
      );

      console.log("--- insertNotificationAktivitiWS PROCESS START ---");
      
      console.log('userWhatsappText', sendNotifikasi.userWhatsappText);
      
      let insertNotificationAktivitiWS = await model.insertNotificationAktiviti(
        1,
        getBulkWakil[0].userPhoneNo,
        null,
        sendNotifikasi.userWhatsappText,
        "whatsApp",
        insertData,
        sendNotifikasi.resultWhatsapp
      );

      console.log("--- insertNotificationAktivitiSMS PROCESS START ---");
      let insertNotificationAktivitiSMS =
        await model.insertNotificationAktiviti(
          1,
          getBulkWakil[0].userPhoneNo,
          null,
          sendNotifikasi.userMessageText,
          "SMS",
          insertData,
          sendNotifikasi.resultSMS
        );
    } else {
      console.log(`MASUK BAHAGIAN SIDE`);
      for (let i = 0; i < getBulkStock.length; i++) {
        console.log("--- UPDATE AKTIVITI-DATE PROCESS START ---");
        let updateStatus = await model.updateAktivitiDateLivestokStatus(
          getBulkStock[i].tempahanDetailsId
        );

        console.log("--- insertAktivitiDateLivestok PROCESS START ---");
        insertData = await model.insertAktivitiDateLivestok(
          aktivitiDateId,
          liveStokId,
          getBulkStock[i].tempahanDetailsId,
          getAktivitiDate[0][0].setupKorbanAktivitiId,
          implementerId,
          implementerLokasiId,
          "ACTIVE"
        );

        let getDataTempahan = await model.getAktivitiDateLivestok(
          insertData,
          "get-tempahanDetailsId"
        );

        console.log("--- updateTempahanDetailsLiveStok PROCESS START ---");
        let updateTempahanDetailsLiveStok =
          await model.updateTempahanDetailsLiveStok(
            getBulkStock[i].tempahanId,
            getBulkStock[i].liveStokId,
            liveStokBahagianId,
            insertData,
            getAktivitiDate[0][0].setupKorbanAktivitiId,
            aktivitiDateId,
            getBulkStock[i].tempahanDetailsId
          );

        console.log("--- getBulkWakil PROCESS START ---");
        getBulkWakil = await model.getBulkWakil(
          getBulkStock[i].tempahanDetailsId
        );

        tempahanDetailsIdArr.push(
          Object.assign(
            { email: getBulkWakil[0].userEmail },
            { name: getBulkWakil[0].userFullname },
            { phone: getBulkWakil[0].userPhoneNo },
            {
              liveStokBahagianNo: getBulkWakil[0].liveStokBahagianNo.replace(
                /K22-|A22-/g,
                ""
              ),
            },
            { tempahanNo: getBulkWakil[0].tempahanNo },
            {
              tempahanDetailsBhgnType: getBulkWakil[0].tempahanDetailsBhgnType,
            },
            {
              tempahanDetailsHaiwanType:
                getBulkWakil[0].tempahanDetailsHaiwanType,
            },
            { ibadah: getBulkWakil[0].liveStokIbadah },
            { lokasi: getBulkWakil[0].implementerNegara },
            { peserta: getBulkWakil[0].wakilPesertaName },
            { aktivitiDateLivestokId: insertData },
            { tarikh: tarikhPelaksanaan }
          )
        );

        wakilArr.push(getBulkWakil[0].userFullname);

        // console.log('--- sendNotificationPindahStok PROCESS START ---')
        // let sendNotifikasi = await model.sendNotificationPindahStok(
        //   'Bahagian',
        //   getBulkWakil[0].userEmail,
        //   getBulkWakil[0].userFullname,
        //   getBulkWakil[0].userPhoneNo,
        //   getBulkWakil[0].liveStokBahagianNo,
        //   getBulkWakil[0].tempahanNo,
        //   getBulkWakil[0].tempahanDetailsBhgnType,
        //   getBulkWakil[0].tempahanDetailsHaiwanType,
        // )
      }

      const wakilName = {};
      for (const num of wakilArr) {
        wakilName[num] = wakilName[num] ? wakilName[num] + 1 : 1;
      }

      let currentName = null,
        currentNameElement = null;

      for (let i = 0; i < Object.keys(wakilName).length; i++) {
        currentName = tempahanDetailsIdArr.filter(
          (obj) => obj.name === Object.keys(wakilName)[i]
        ).length;
        currentNameElement = tempahanDetailsIdArr.filter(
          (obj) => obj.name === Object.keys(wakilName)[i]
        );

        console.log("--- sendNotificationPindahStok v2 PROCESS START ---");
        let sendNotifikasi = await model.sendNotificationPindahStok(
          "Bahagian",
          currentNameElement[0].email,
          currentNameElement[0].name,
          currentNameElement[0].phone,
          currentNameElement,
          currentNameElement[0].tempahanNo,
          currentNameElement[0].tempahanDetailsBhgnType,
          currentNameElement[0].tempahanDetailsHaiwanType,
          currentAktivitiStatus,
          tarikhPelaksanaan,
          null
        );

        console.log("--- insertNotificationAktivitiWS PROCESS START ---");
        let insertNotificationAktivitiWS =
          await model.insertNotificationAktiviti(
            1,
            currentNameElement[0].phone,
            null,
            sendNotifikasi.userWhatsappText,
            "whatsApp",
            null,
            sendNotifikasi.resultWhatsapp
          );

        console.log("--- insertNotificationAktivitiSMS PROCESS START ---");
        let insertNotificationAktivitiSMS =
          await model.insertNotificationAktiviti(
            1,
            currentNameElement[0].phone,
            null,
            sendNotifikasi.userMessageText,
            "SMS",
            null,
            sendNotifikasi.resultSMS
          );
      }
    }

    if (insertData != false) {
      result = {
        status: "berjaya",
        message: "ANDA BERJAYA INSERT DATA!",
        data: insertData[0],
      };
    } else {
      result = {
        status: "gagal",
        message: "ANDA TIDAK BERJAYA INSERT DATA!",
        data: null,
        data2: null,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
