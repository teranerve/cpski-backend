const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");
// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// GET USER
router.get("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let implementerId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    implementerId = param.implementerId;

    // GET USER FUNCTION
    const getImplementerById = await model.getImplementerById(implementerId);

    if (getImplementerById != false ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan details implementer",
        data: getImplementerById
      };
    } else {
      result = {
          status: "gagal",
          message: 'Anda gagal dapatkan details implementer'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
