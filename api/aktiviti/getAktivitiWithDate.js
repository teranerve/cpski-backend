const express = require("express");
const router = express.Router();
const model = require("../../function/aktiviti/aktiviti.js");


router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null;

  try {
    param = req.query;
    implementerId = param.implementerId;

      let getAktivitiWithDate = await model.getAktivitiWithDate(implementerId);

      if (getAktivitiWithDate != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan getAktivitiWithDate",
          data: getAktivitiWithDate[0]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan getAktivitiWithDate'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

router.get("/getAktivitiStokHaiwan", async (req, res) => {
  let param = null;
  let result = null;
  let aktivitiDateId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    aktivitiDateId = param.aktivitiDateId;

      let getAktivitiStokHaiwan = await model.getAktivitiStokHaiwan(aktivitiDateId);

      if (getAktivitiStokHaiwan != false) {
        result = {
          status: "Berjaya",
          message: "Anda berjaya dapatkan aktiviti stok haiwan",
          data: getAktivitiStokHaiwan,
        };
      } else {
        result = {
          status: "Gagal",
          message: 'Anda gagal dapatkan aktiviti stok haiwan',
          data: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});
module.exports = router;