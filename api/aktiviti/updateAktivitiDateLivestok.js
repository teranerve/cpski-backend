const express = require("express");
const router = express.Router();
const model = require("../../function/aktiviti/aktiviti.js");


router.post("/", async (req, res) => {
  let param = null;
  let result = null;
  let aktivitiDateLivestokId = null, aktivitiDateLivestokStatus = null

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body
    aktivitiDateLivestokId = param.aktivitiDateLivestokId,
    aktivitiDateLivestokStatus = param.status

      console.log('--- updateAktivitiDateLivestok PROCESS START ---')

      let updateAktivitiDateLivestok = await model.updateAktivitiDateLivestok(
        aktivitiDateLivestokId,
        aktivitiDateLivestokStatus
      )

      if (updateAktivitiDateLivestok != false) {
        result = {
          status: "berjaya",
          message: "ANDA BERJAYA INSERT DATA!",
          data: updateAktivitiDateLivestok
        };
      } else {
        result = {
          status: "gagal",
          message: 'ANDA TIDAK BERJAYA INSERT DATA!',
          data: null,
          data2: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;