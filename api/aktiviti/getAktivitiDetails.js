const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let setupKorbanAktivitiId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    setupKorbanAktivitiId = param.setupKorbanAktivitiId;

      let getAktivitiDetails = await model.getAktivitiDetails(setupKorbanAktivitiId);

      if (getAktivitiDetails[0] != false || getAktivitiDetails[1] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan detail aktiviti",
          data: getAktivitiDetails[0],
          data2: getAktivitiDetails[1],
          data3: getAktivitiDetails[2]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan detail aktiviti'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

router.get("/getAktivitiStokHaiwan", async (req, res) => {
  let param = null;
  let result = null;
  let aktivitiDateId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    aktivitiDateId = param.aktivitiDateId;

      let getAktivitiStokHaiwan = await model.getAktivitiStokHaiwan(aktivitiDateId);

      if (getAktivitiStokHaiwan != false) {
        result = {
          status: "Berjaya",
          message: "Anda berjaya dapatkan aktiviti stok haiwan",
          data: getAktivitiStokHaiwan,
        };
      } else {
        result = {
          status: "Gagal",
          message: 'Anda gagal dapatkan aktiviti stok haiwan',
          data: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});
module.exports = router;