const express = require("express");
const router = express.Router();
const model = require("../../function/aktiviti/aktiviti.js");


router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let tempahanDetailsId = null;

  console.log(req.query)

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    tempahanDetailsId = param.tempahanDetailsId;

      let getData = await model.getSelectedWakil(tempahanDetailsId);

      console.log('--- getSelectedWakil ---')
      console.log(getData[0])

      if (getData != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan data",
          data: getData[0]
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan data',
          data: null,
          data2: null,
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;