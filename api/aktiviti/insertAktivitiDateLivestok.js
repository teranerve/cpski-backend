const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE

router.post("/", async (req, res) => {
  let param = null;
  let result = null;
  let implementerId = null,
    aktivitiDateId = null,
    liveStokId = null,
    tempahanDetailsId = null,
    liveStokBahagianId = null,
    implementerLokasiId = null,
    insertData = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;
    implementerId = param.implementerId;
    implementerLokasiId = param.implementerLokasiId;
    aktivitiDateId = param.aktivitiDateId;
    liveStokId = param.liveStokId;
    tempahanDetailsId = param.tempahanDetailsId;

    console.log("--- GET AKTIVITI PROCESS START ---");
    let getAktivitiDate = await model.getAktivitiDate(aktivitiDateId);
    console.log("getAktivitiDate: ", getAktivitiDate);

    console.log("--- GET getBulktempahanDetailsLiveStok PROCESS START ---");
    let getBulktempahanDetailsLiveStok =
      await model.getBulktempahanDetailsLiveStok(implementerId, liveStokId);
    console.log(
      "getBulktempahanDetailsLiveStok: ",
      getBulktempahanDetailsLiveStok
    );

    for (let i = 0; i < getBulktempahanDetailsLiveStok[0].length; i++) {
      console.log("--- insertAktivitiDateLivestok PROCESS START ---");
      insertData = await model.insertAktivitiDateLivestok(
        aktivitiDateId,
        liveStokId,
        getBulktempahanDetailsLiveStok[0][i].tempahanDetailsId,
        getAktivitiDate[0][0].setupKorbanAktivitiId,
        implementerId,
        implementerLokasiId,
        "ACTIVE"
      );

      console.log("--- updateTempahanDetailsLiveStok PROCESS START ---");
      let updateTempahanDetailsLiveStok =
        await model.updateTempahanDetailsLiveStok(
          getBulktempahanDetailsLiveStok[0][i].tempahanId,
          getBulktempahanDetailsLiveStok[0][i].liveStokId,
          liveStokBahagianId,
          insertData,
          getAktivitiDate[0][0].setupKorbanAktivitiId,
          aktivitiDateId,
          getBulktempahanDetailsLiveStok[0][i].tempahanDetailsId
        );
    }

    if (insertData != false) {
      result = {
        status: "berjaya",
        message: "ANDA BERJAYA INSERT DATA!",
        data: insertData[0],
      };
    } else {
      result = {
        status: "gagal",
        message: "ANDA TIDAK BERJAYA INSERT DATA!",
        data: null,
        data2: null,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
