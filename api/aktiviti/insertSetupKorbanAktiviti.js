const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let aktivitiLookupId = null;
  let jenisIbadahAktivitiBaru = null;
  let aktivitiDescription = null;
  let implementerId = null;
  let implementerLokasiId = null;
  let statusAktiviti = null;
  let aktivitiDateTarikh = null;

  let setupKorbanId = null;

  let setupKorbanAktivitiId = null;

  let setupKorbanAktivitiStatusCode = null;

  let currentYear = null;

  console.log("bi ha param: ", req.body);
  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;

    aktivitiLookupId = param.aktivitiLookupId;
    jenisIbadahAktivitiBaru = param.jenisIbadahAktivitiBaru;
    aktivitiDescription = param.aktivitiDescription;
    implementerId = param.implementerId;
    implementerLokasiId = param.implementerLokasiId;
    statusAktiviti = param.statusAktiviti;
    aktivitiDateTarikh = param.aktivitiDateTarikh;
    setupKorbanAktivitiId = param.setupKorbanAktivitiId;
    currentYear = moment().format("YYYY");

    if (statusAktiviti == "Active") setupKorbanAktivitiStatusCode = 20;
    else setupKorbanAktivitiStatusCode = 21;

    // console.log('currentYear => ' + currentYear)

    // GET SETUP KORBAN'S ID BY CURRENT YEAR
    let getSetupKorbanId = await model.getSetupKorbanId(currentYear);
    if (!getSetupKorbanId) return;
    else {
      // console.log('getSetupKorbanId => ' + getSetupKorbanId[0].setupKorbanId)
      setupKorbanId = getSetupKorbanId[0].setupKorbanId;
    }

    if (setupKorbanAktivitiId == null) {
      // INSERT SETUP KORBAN AKTIVITI
      let insertSetupKorbanAktiviti = await model.insertSetupKorbanAktiviti(
        aktivitiLookupId,
        implementerId,
        implementerLokasiId,
        jenisIbadahAktivitiBaru,
        aktivitiDescription,
        setupKorbanId,
        statusAktiviti,
        setupKorbanAktivitiStatusCode
      );
      console.log("insertSetupKorbanAktiviti: ", insertSetupKorbanAktiviti);
      //  INSERT AKTIVITI DATE
      let insertAktivitDate = await model.insertAktivitDate(
        insertSetupKorbanAktiviti,
        aktivitiLookupId,
        implementerId,
        aktivitiDateTarikh,
        statusAktiviti,
        setupKorbanAktivitiStatusCode
      );
      console.log("insertAktivitDate: ", insertAktivitDate);

      if (insertSetupKorbanAktiviti != false || insertAktivitDate != false) {
        result = {
          status: "Berjaya",
          message:
            "Anda berjaya masukkan Setup Korban Aktiviti & Aktiviti Date.",
        };
      } else {
        result = {
          status: "Gagal",
          message: "Anda gagal masukkan Setup Korban Aktiviti & Aktiviti Date.",
        };
      }
    } else {
      //  INSERT AKTIVITI DATE
      let insertAktivitDate = await model.insertAktivitDate(
        setupKorbanAktivitiId,
        aktivitiLookupId,
        implementerId,
        aktivitiDateTarikh,
        statusAktiviti,
        setupKorbanAktivitiStatusCode
      );

      if (insertAktivitDate != false) {
        result = {
          status: "Berjaya",
          message: "Anda berjaya masukkan Aktiviti Date.",
        };
      } else {
        result = {
          status: "Gagal",
          message: "Anda gagal masukkan Aktiviti Date.",
        };
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

// POST USER
router.post("/updateAktivitiDate", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let aktivitiDateId = null;
  let aktivitiDateTarikhPelaksanaan = null;
  let aktivitiDateStatus = null;
  let aktivitiDateStatusCode = null;

  console.log(req.body);
  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;

    aktivitiDateId = param.aktivitiDateId;
    aktivitiDateTarikhPelaksanaan = param.aktivitiDateTarikhPelaksanaan;
    aktivitiDateStatus = param.aktivitiDateStatus;

    if (aktivitiDateStatus == "Active") aktivitiDateStatusCode = 20;
    else aktivitiDateStatusCode = 21;

    //  INSERT AKTIVITI DATE
    let updateAktivitDate = await model.updateAktivitDate(
      aktivitiDateId,
      aktivitiDateTarikhPelaksanaan,
      aktivitiDateStatus,
      aktivitiDateStatusCode
    );

    if (updateAktivitDate != false) {
      result = {
        status: "Berjaya",
        message: "Anda berjaya kemaskini Aktiviti Date.",
      };
    } else {
      result = {
        status: "Gagal",
        message: "Anda gagal kemaskini Aktiviti Date.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
