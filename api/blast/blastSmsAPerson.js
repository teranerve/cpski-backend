const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/blast/blast.js"); // INCLUDE FUNCTION FILE\
const sgMail = require('@sendgrid/mail')
// const client = require('@sendgrid/mail');
// const nodemailer = require("nodemailer"); 

router.post("/", async (req, res) => {
    let param = null;
    let result = null;
    let phoneNo = null, 
     SMSText = null
  
    try {
      // BIND PARAMETER TO VARIABLES
      param = req.body
      console.log('param')
      console.log(param)
      phoneNo = param.TelefonWakil
      SMSText = param.SMSText
  
        console.log('--- blastSmsAPerson PROCESS START ---')
  
        let blastSmsAPerson = await model.blastSmsAPerson(
          phoneNo,
          SMSText
        )
  
        // let insertNotificationAktivitiWS = await model.insertNotificationAktiviti(
        //   1,
        //   modalNamaWakil,
        //   WhatsAppText,
        //   'whatsApp',
        //   aktivitiDateLivestokId,
        //   blastSmsAPerson.resultWhatsapp
        // )
        if (blastSmsAPerson.resultSMS == 'OK') {
        let insertNotificationBlastSMS = await model.insertNotificationBlastSMS(
          1,
          phoneNo,
          SMSText,
          'SMS',
          blastSmsAPerson.resultSMS
        )
        }
        if (blastSmsAPerson != false) {
          result = {
            status: "berjaya",
            message: "ANDA BERJAYA SEND NOTI!",
            data: blastSmsAPerson
          };
        } else {
          result = {
            status: "gagal",
            message: 'ANDA TIDAK BERJAYA SEND NOTI!',
            data: null,
            data2: null,
          }
        }
    } catch (error) {
      console.log(error); // LOG ERROR
      result = {
        message: `API Error`,
      };
    }
  
    // RETURN
    res.status(200).json(result);
  
  });

module.exports = router;